-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: 68.178.216.14
-- Generation Time: Mar 22, 2014 at 01:27 PM
-- Server version: 5.0.96
-- PHP Version: 5.1.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `qochi`
--

-- --------------------------------------------------------

--
-- Table structure for table `Analytics`
--

CREATE TABLE `Analytics` (
  `id` int(11) NOT NULL auto_increment,
  `feeditemid` int(11) default NULL,
  `userid` int(11) default NULL,
  `unique_user_id` varchar(256) default NULL,
  `packid` varchar(500) default NULL,
  `imageid` varchar(500) default NULL,
  `timestamp` datetime default NULL,
  `share` tinyint(1) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_Analytics_1_idx` (`feeditemid`),
  KEY `fk_Analytics_2_idx` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `Analytics`
--

INSERT INTO `Analytics` VALUES(1, 1, 0, 'adminqa', 'pack523b33592cb1f-jpg', 'image523b33867b951.jpg', '2013-09-19 20:47:52', 0);
INSERT INTO `Analytics` VALUES(2, 2, 0, 'salman_paracha', 'pack523cc5cc9900a-jpg', 'mage523cc7c463624.jpg', '2014-01-11 15:16:36', 0);
INSERT INTO `Analytics` VALUES(3, 3, 0, 'salman_paracha', 'pack523cc5cc9900a-jpg', 'image523cc7c463624.jpg', '2014-01-13 20:16:17', 0);
INSERT INTO `Analytics` VALUES(4, 4, 0, 'salman_paracha', 'pack523cc5cc9900a-jpg', 'mage523cc7c463624.jpg', '2014-01-13 23:30:57', 0);
INSERT INTO `Analytics` VALUES(5, 5, 0, 'amrit', 'pack52cb9497857a4-png', 'image52cb955ac9e89.png', '2014-01-15 02:07:26', 0);
INSERT INTO `Analytics` VALUES(6, 6, 0, 'amrit', 'pack52cb9497857a4-png', 'image52cb955ac9e89.png', '2014-01-17 10:16:24', 0);
INSERT INTO `Analytics` VALUES(7, 7, 0, 'amrit', 'pack52cb9497857a4-png', 'image52cb955ac9e89.png', '2014-01-24 03:18:48', 0);
INSERT INTO `Analytics` VALUES(8, 8, 0, '1', 'pack52cb9497857a4-png', 'image52cb955deecc0.png', '2014-01-24 03:29:51', 0);
INSERT INTO `Analytics` VALUES(9, 9, 0, '1', 'pack52cb96cfedd60-png', 'image52cb9780ef91d.png', '2014-01-24 03:31:20', 0);
INSERT INTO `Analytics` VALUES(10, 10, 0, '1', 'pack52cb96cfedd60-png', 'image52cb97809f145.png', '2014-01-24 03:41:37', 0);
INSERT INTO `Analytics` VALUES(11, 11, 0, '1', 'pack52cb99325823b-png', 'image52cb99b00f178.png', '2014-01-24 03:51:46', 0);
INSERT INTO `Analytics` VALUES(12, 12, 0, '1', 'pack52cb9497857a4-png', 'image52cb955ca3388.png', '2014-01-24 04:35:17', 0);
INSERT INTO `Analytics` VALUES(13, 13, 0, '1', 'pack52cb99325823b-png', 'image52cb99af67a67.png', '2014-01-28 01:16:59', 0);
INSERT INTO `Analytics` VALUES(14, 14, 0, '1', 'pack52cb9497857a4-png', 'image52cb955bccb3e.png', '2014-01-28 23:27:01', 0);
INSERT INTO `Analytics` VALUES(15, 15, 0, '100006533104895', 'pack52cb9497857a4-png', 'image52cb955e32acc.png', '2014-01-28 23:40:26', 0);
INSERT INTO `Analytics` VALUES(16, 16, 0, '100006533104895', 'pack52cb96cfedd60-png', 'image52cb978324826.png', '2014-01-29 00:36:07', 0);
INSERT INTO `Analytics` VALUES(17, 17, 0, '100006533104895', 'pack52cb9a1cb6a02-png', 'image52cb9ac3770c6.png', '2014-01-29 00:54:22', 0);
INSERT INTO `Analytics` VALUES(18, 18, 0, '100006533104895', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-01-29 05:16:34', 0);
INSERT INTO `Analytics` VALUES(19, 19, 0, '100006533104895', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-01-29 05:16:46', 0);
INSERT INTO `Analytics` VALUES(20, 20, 0, '100006533104895', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-01-29 09:01:25', 0);
INSERT INTO `Analytics` VALUES(21, 21, 0, '100006533104895', 'pack52cb9b2609801-png', 'image52cb9b945d736.png', '2014-01-29 09:05:33', 0);
INSERT INTO `Analytics` VALUES(22, 22, 0, '100006533104895', 'pack52cb9b2609801-png', 'image52cb9b96e6763.png', '2014-01-29 09:07:33', 0);
INSERT INTO `Analytics` VALUES(23, 23, 0, '100006533104895', 'pack52cb9b2609801-png', 'image52cb9b94554d0.png', '2014-01-29 09:19:08', 0);
INSERT INTO `Analytics` VALUES(24, 24, 0, '100002353362874', 'pack52cb9497857a4-png', 'image52cb955b18f93.png', '2014-01-29 23:55:06', 0);
INSERT INTO `Analytics` VALUES(25, 25, 0, '100002353362874', 'pack52cb9497857a4-png', 'image52cb955b18f93.png', '2014-01-29 23:55:31', 0);
INSERT INTO `Analytics` VALUES(26, 26, 0, '710412425', 'pack52cb99325823b-png', 'image52cb99af7003c.png', '2014-01-30 01:05:40', 0);
INSERT INTO `Analytics` VALUES(27, 27, 0, '25318090', 'pack52cb99325823b-png', 'image52cb99af7003c.png', '2014-01-30 01:10:12', 0);
INSERT INTO `Analytics` VALUES(28, 28, 0, '710412425', 'pack52cb99325823b-png', 'image52cb99aed608f.png', '2014-01-30 01:10:12', 0);
INSERT INTO `Analytics` VALUES(29, 29, 0, '710412425', 'pack52cb99325823b-png', 'image52cb99af67a67.png', '2014-01-30 01:11:44', 0);
INSERT INTO `Analytics` VALUES(30, 30, 0, '710412425', 'pack52cb9b2609801-png', 'image52cb9b945d736.png', '2014-01-30 01:14:45', 0);
INSERT INTO `Analytics` VALUES(31, 31, 0, '710412425', 'pack52cb99325823b-png', 'image52cb99b0a591f.png', '2014-01-30 03:39:43', 0);
INSERT INTO `Analytics` VALUES(32, 32, 0, '710412425', 'pack52cb99325823b-png', 'image52cb99b0a591f.png', '2014-01-30 03:42:23', 0);
INSERT INTO `Analytics` VALUES(33, 33, 0, '710412425', 'pack52cb9b2609801-png', 'image52cb9b9614b82.png', '2014-01-30 07:27:13', 0);
INSERT INTO `Analytics` VALUES(34, 34, 0, '100002353362874', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-01-30 07:41:15', 0);
INSERT INTO `Analytics` VALUES(35, 35, 0, '100002353362874', 'pack52cb96cfedd60-png', 'image52cb97839cc91.png', '2014-01-30 07:43:30', 0);
INSERT INTO `Analytics` VALUES(36, 36, 0, '25318090', 'pack52cb99325823b-png', 'image52cb99b00f178.png', '2014-01-30 09:12:21', 0);
INSERT INTO `Analytics` VALUES(37, 37, 0, '25318090', 'pack52cb99325823b-png', 'image52cb99b00f178.png', '2014-01-30 09:13:44', 0);
INSERT INTO `Analytics` VALUES(38, 38, 0, '879185566', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-01-30 21:32:10', 0);
INSERT INTO `Analytics` VALUES(39, 39, 0, '879185566', 'pack52cb96cfedd60-png', 'image52cb9781c3db9.png', '2014-01-30 21:43:24', 0);
INSERT INTO `Analytics` VALUES(40, 40, 0, '879185566', 'pack52cb96cfedd60-png', 'image52cb9780d7fc2.png', '2014-01-30 21:43:55', 0);
INSERT INTO `Analytics` VALUES(41, 41, 0, '25318090', 'pack52cb96cfedd60-png', 'image52cb977fc6122.png', '2014-01-30 21:50:38', 0);
INSERT INTO `Analytics` VALUES(42, 42, 0, '879185566', 'pack52cb99325823b-png', 'image52cb99b025603.png', '2014-01-30 21:57:18', 0);
INSERT INTO `Analytics` VALUES(43, 43, 0, '25318090', 'pack52cb99325823b-png', 'image52cb99b0a591f.png', '2014-01-30 22:33:46', 0);
INSERT INTO `Analytics` VALUES(44, 44, 0, '100006533104895', 'pack52cb9f265647e-png', 'image52cb9fbda9659.png', '2014-01-31 02:40:56', 0);
INSERT INTO `Analytics` VALUES(45, 45, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb9782e9c2c.png', '2014-01-31 06:04:00', 0);
INSERT INTO `Analytics` VALUES(46, 46, 0, '2024225', 'pack52cb9c1a082e2-png', 'image52cb9c9570016.png', '2014-01-31 06:07:35', 0);
INSERT INTO `Analytics` VALUES(47, 47, 0, '25318090', 'pack52cba1aa94acf-png', 'image52cba282ad87c.png', '2014-01-31 09:47:23', 0);
INSERT INTO `Analytics` VALUES(48, 48, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb9781d3789.png', '2014-01-31 10:06:34', 0);
INSERT INTO `Analytics` VALUES(49, 49, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb977fed9ae.png', '2014-01-31 12:11:47', 0);
INSERT INTO `Analytics` VALUES(50, 50, 0, '25318090', 'pack52cb96cfedd60-png', 'image52cb9782e9c2c.png', '2014-01-31 14:59:31', 0);
INSERT INTO `Analytics` VALUES(51, 51, 0, '25318090', 'pack52cb99325823b-png', 'image52cb99afdc6a0.png', '2014-01-31 20:08:23', 0);
INSERT INTO `Analytics` VALUES(52, 52, 0, '100002353362874', 'pack52cb96cfedd60-png', 'image52cb97809f145.png', '2014-01-31 21:51:59', 0);
INSERT INTO `Analytics` VALUES(53, 53, 0, '25318090', 'pack52cb9497857a4-png', 'image52cb955cd8610.png', '2014-02-01 09:50:13', 0);
INSERT INTO `Analytics` VALUES(54, 54, 0, '100006533104895', 'pack52cb9497857a4-png', 'image52cb955b03dcc.png', '2014-02-04 02:01:54', 0);
INSERT INTO `Analytics` VALUES(55, 55, 0, '100001911886454', 'pack52cb9a1cb6a02-png', 'image52cb9ac1e760a.png', '2014-02-04 03:00:03', 0);
INSERT INTO `Analytics` VALUES(56, 56, 0, '100001911886454', 'pack52ee9c92dc225-png', 'image52ee9d069e2e8.png', '2014-02-04 05:24:16', 0);
INSERT INTO `Analytics` VALUES(57, 57, 0, '2024225', 'pack52cb9b2609801-png', 'image52cb9b952dee4.png', '2014-02-04 20:08:44', 0);
INSERT INTO `Analytics` VALUES(58, 58, 0, '100006533104895', 'pack52cb9497857a4-png', 'image52cb955d46abb.png', '2014-02-05 10:00:00', 0);
INSERT INTO `Analytics` VALUES(59, 59, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb9782e5b7a.png', '2014-02-05 15:47:50', 0);
INSERT INTO `Analytics` VALUES(60, 60, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb977fd0397.png', '2014-02-07 11:17:18', 0);
INSERT INTO `Analytics` VALUES(61, 61, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-12 20:13:14', 0);
INSERT INTO `Analytics` VALUES(62, 62, 0, '2024225', 'pack52cb9c1a082e2-png', 'image52cb9c957d55b.png', '2014-02-18 10:32:57', 0);
INSERT INTO `Analytics` VALUES(63, 63, 0, '2024225', 'pack52cba1aa94acf-png', 'image52cba28228e29.png', '2014-02-18 14:56:53', 0);
INSERT INTO `Analytics` VALUES(64, 64, 0, '100001911886454', 'pack52ee9c92dc225-png', 'image52ee9d066354e.png', '2014-02-19 05:15:08', 0);
INSERT INTO `Analytics` VALUES(65, 65, 0, '100001911886454', 'pack52ee9c92dc225-png', 'image52ee9d066354e.png', '2014-02-19 05:16:23', 0);
INSERT INTO `Analytics` VALUES(66, 66, 0, '100001911886454', 'pack52ee9c92dc225-png', 'image52ee9d0a67125.png', '2014-02-19 05:21:50', 0);
INSERT INTO `Analytics` VALUES(67, 67, 0, '100001911886454', 'pack52ee9c92dc225-png', 'image52ee9d069e2e8.png', '2014-02-19 05:28:03', 0);
INSERT INTO `Analytics` VALUES(68, 68, 0, '100001911886454', 'pack52ee9c92dc225-png', 'image52ee9d066a0a4.png', '2014-02-19 05:34:58', 0);
INSERT INTO `Analytics` VALUES(69, 69, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-19 06:19:52', 0);
INSERT INTO `Analytics` VALUES(70, 70, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fc6122.png', '2014-02-19 06:41:28', 0);
INSERT INTO `Analytics` VALUES(71, 71, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fed9ae.png', '2014-02-19 07:53:44', 0);
INSERT INTO `Analytics` VALUES(72, 72, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-19 07:55:52', 0);
INSERT INTO `Analytics` VALUES(73, 73, 0, '2024225', 'pack52cba1aa94acf-png', 'image52cba284133c0.png', '2014-02-19 09:59:09', 0);
INSERT INTO `Analytics` VALUES(74, 74, 0, '2024225', 'pack52cb9a1cb6a02-png', 'image52cb9ac2a0755.png', '2014-02-19 10:44:52', 0);
INSERT INTO `Analytics` VALUES(75, 75, 0, '2024225', 'pack52cba1aa94acf-png', 'image52cba282c6b83.png', '2014-02-19 10:49:14', 0);
INSERT INTO `Analytics` VALUES(76, 76, 0, '25318090', 'pack52ee9c92dc225-png', 'image52ee9d0809232.png', '2014-02-19 11:03:27', 0);
INSERT INTO `Analytics` VALUES(77, 77, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb978244f79.png', '2014-02-19 23:08:56', 0);
INSERT INTO `Analytics` VALUES(78, 78, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-20 03:30:20', 0);
INSERT INTO `Analytics` VALUES(79, 79, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-20 03:31:05', 0);
INSERT INTO `Analytics` VALUES(80, 80, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb978244f79.png', '2014-02-20 09:25:45', 0);
INSERT INTO `Analytics` VALUES(81, 81, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb977fc6122.png', '2014-02-20 13:37:21', 0);
INSERT INTO `Analytics` VALUES(82, 82, 0, '100001911886454', 'pack52cb9497857a4-png', 'image52cb955b03dcc.png', '2014-02-20 23:08:34', 0);
INSERT INTO `Analytics` VALUES(83, 83, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-21 00:06:47', 0);
INSERT INTO `Analytics` VALUES(84, 84, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-21 00:48:18', 0);
INSERT INTO `Analytics` VALUES(85, 85, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb97809f145.png', '2014-02-21 06:40:16', 0);
INSERT INTO `Analytics` VALUES(86, 86, 0, '25318090', 'pack52cb99325823b-png', 'image52cb99aed608f.png', '2014-02-21 07:24:41', 0);
INSERT INTO `Analytics` VALUES(87, 87, 0, '2024225', 'pack52cba1aa94acf-png', 'image52cba2839db5e.png', '2014-02-21 10:45:18', 0);
INSERT INTO `Analytics` VALUES(88, 88, 0, '2024225', 'pack52cb9b2609801-png', 'image52cb9b96cab2b.png', '2014-02-21 20:45:49', 0);
INSERT INTO `Analytics` VALUES(89, 89, 0, '100001911886454', 'pack52cb9497857a4-png', 'image52cb955b18f93.png', '2014-02-22 01:02:55', 0);
INSERT INTO `Analytics` VALUES(90, 90, 0, '100001911886454', 'pack52cb9497857a4-png', 'image52cb955b18f93.png', '2014-02-22 01:04:53', 0);
INSERT INTO `Analytics` VALUES(91, 91, 0, '25318090', 'pack52cb99325823b-png', 'image52cb99aed608f.png', '2014-02-24 09:45:56', 0);
INSERT INTO `Analytics` VALUES(92, 92, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fd36ed.png', '2014-02-26 01:30:02', 0);
INSERT INTO `Analytics` VALUES(93, 93, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-26 01:31:50', 0);
INSERT INTO `Analytics` VALUES(94, 94, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', '2014-02-26 03:39:24', 0);
INSERT INTO `Analytics` VALUES(95, 95, 0, '2024225', 'pack52ee9c92dc225-png', 'image52ee9d09eb322.png', '2014-02-26 09:00:03', 0);
INSERT INTO `Analytics` VALUES(96, 96, 0, '25318090', 'pack52ee9c92dc225-png', 'image52ee9d066354e.png', '2014-02-26 09:09:51', 0);
INSERT INTO `Analytics` VALUES(97, 97, 0, '25318090', 'pack52cb9b2609801-png', 'image52cb9b960e974.png', '2014-02-26 09:13:53', 0);
INSERT INTO `Analytics` VALUES(98, 98, 0, '2024225', 'pack52cb9a1cb6a02-png', 'image52cb9ac3770c6.png', '2014-02-26 13:26:19', 0);
INSERT INTO `Analytics` VALUES(99, 99, 0, '2021720', 'pack52cb9497857a4-png', 'image52cb955bccb3e.png', '2014-02-26 19:40:36', 0);
INSERT INTO `Analytics` VALUES(100, 100, 0, '100001911886454', 'pack52cb9497857a4-png', 'image52cb955ac9e89.png', '2014-02-27 00:04:57', 0);
INSERT INTO `Analytics` VALUES(101, 101, 0, '2024225', 'pack52ee9c92dc225-png', 'image52ee9d0863980.png', '2014-02-27 10:53:36', 0);
INSERT INTO `Analytics` VALUES(102, 102, 0, '100001911886454', 'pack52cb9c1a082e2-png', 'image52cb9c950c81b.png', '2014-02-28 00:31:43', 0);
INSERT INTO `Analytics` VALUES(103, 103, 0, '25318090', 'pack52ee9c92dc225-png', 'image52ee9d0684862.png', '2014-02-28 02:15:40', 0);
INSERT INTO `Analytics` VALUES(104, 104, 0, '2024225', 'pack52cb9c1a082e2-png', 'image52cb9c9673f5a.png', '2014-02-28 10:04:18', 0);
INSERT INTO `Analytics` VALUES(105, 105, 0, '2024225', 'pack52cba1aa94acf-png', 'image52cba28380040.png', '2014-02-28 19:57:57', 0);
INSERT INTO `Analytics` VALUES(106, 106, 0, '2024225', 'pack52cb9b2609801-png', 'image52cb9b9614b82.png', '2014-02-28 22:23:13', 0);
INSERT INTO `Analytics` VALUES(107, 107, 0, '25318090', 'pack52cb9c1a082e2-png', 'image52cb9c9570016.png', '2014-02-28 23:32:57', 0);
INSERT INTO `Analytics` VALUES(108, 108, 0, '2024225', 'pack52cb9a1cb6a02-png', 'image52cb9ac2a0755.png', '2014-03-02 10:03:51', 0);
INSERT INTO `Analytics` VALUES(109, 109, 0, '596015253', 'pack52cb9497857a4-png', 'image52cb955de8fdf.png', '2014-03-04 10:19:17', 0);
INSERT INTO `Analytics` VALUES(110, 110, 0, '596015253', 'pack52cb9497857a4-png', 'image52cb955b18f93.png', '2014-03-04 10:24:43', 0);
INSERT INTO `Analytics` VALUES(111, 111, 0, '2024225', 'pack52cb9a1cb6a02-png', 'image52cb9ac2a4924.png', '2014-03-06 21:00:46', 0);
INSERT INTO `Analytics` VALUES(112, 112, 0, '2024225', 'pack52cb9c1a082e2-png', 'image52cb9c968d113.png', '2014-03-07 16:27:45', 0);
INSERT INTO `Analytics` VALUES(113, 113, 0, '2024225', 'pack52cb9c1a082e2-png', 'image52cb9c970f4cc.png', '2014-03-07 19:00:01', 0);
INSERT INTO `Analytics` VALUES(114, 114, 0, '25318090', 'pack52cb96cfedd60-png', 'image52cb9780ef91d.png', '2014-03-08 00:14:16', 0);
INSERT INTO `Analytics` VALUES(115, 115, 0, '2024225', 'pack52cba1aa94acf-png', 'image52cba284c2f1f.png', '2014-03-08 13:31:33', 0);
INSERT INTO `Analytics` VALUES(116, 116, 0, '2021720', 'pack52cb9e1fa2d35-png', 'image52cb9ed35b78c.png', '2014-03-08 22:28:02', 0);
INSERT INTO `Analytics` VALUES(117, 117, 0, '25318090', 'pack52cba1aa94acf-png', 'image52cba2820042b.png', '2014-03-10 19:04:06', 0);
INSERT INTO `Analytics` VALUES(118, 118, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb9782e9bbf.png', '2014-03-10 22:36:50', 0);
INSERT INTO `Analytics` VALUES(119, 119, 0, '25318090', 'pack52ee9c92dc225-png', 'image52ee9d0975b2b.png', '2014-03-13 00:26:13', 0);
INSERT INTO `Analytics` VALUES(120, 120, 0, '100007959189831', 'pack52cb9c1a082e2-png', 'image52cb9c9570016.png', '2014-03-13 15:15:32', 0);
INSERT INTO `Analytics` VALUES(121, 121, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fd0397.png', '2014-03-13 21:30:07', 0);
INSERT INTO `Analytics` VALUES(122, 122, 0, '100001911886454', 'pack52cb96cfedd60-png', 'image52cb977fd0397.png', '2014-03-13 21:43:36', 0);
INSERT INTO `Analytics` VALUES(123, 123, 0, '2024225', 'pack52cb9b2609801-png', 'image52cb9b95729dd.png', '2014-03-14 09:32:06', 0);
INSERT INTO `Analytics` VALUES(124, 124, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb977fedf79.png', '2014-03-14 17:21:11', 0);
INSERT INTO `Analytics` VALUES(125, 125, 0, '2024225', 'pack52cb96cfedd60-png', 'image52cb9782e5b7a.png', '2014-03-15 02:02:59', 0);
INSERT INTO `Analytics` VALUES(126, 126, 0, '25318090', 'pack52cb9a1cb6a02-png', 'image52cb9ac2a64e2.png', '2014-03-17 13:44:27', 0);
INSERT INTO `Analytics` VALUES(127, 127, 0, '25318090', 'pack52cb9a1cb6a02-png', 'image52cb9ac1e760a.png', '2014-03-17 13:46:25', 0);
INSERT INTO `Analytics` VALUES(128, 128, 0, '25318090', 'pack52cb9c1a082e2-png', 'image52cb9c950c81b.png', '2014-03-20 09:48:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ArtistInfo`
--

CREATE TABLE `ArtistInfo` (
  `artistid` int(11) NOT NULL auto_increment,
  `title` varchar(255) character set utf8 NOT NULL,
  `description` varchar(1024) character set utf8 NOT NULL,
  PRIMARY KEY  (`artistid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ArtistInfo`
--

INSERT INTO `ArtistInfo` VALUES(2, 'Gabriel Mourelle', 'Buenos Aires, Argentina -- http://platform.qochi.com/platform/assets/Gabriel.png');
INSERT INTO `ArtistInfo` VALUES(3, 'Safi', 'Seattle, Washington -- http://platform.qochi.com/platform/assets/assad.png');
INSERT INTO `ArtistInfo` VALUES(4, 'Rubens Cantuni', 'Milan, Italy -- http://platform.qochi.com/platform/assets/Rubens.png');
INSERT INTO `ArtistInfo` VALUES(5, 'Leo Espinosa', 'Salt Lake City, UT -- http://platform.qochi.com/platform/assets/Leo.png');
INSERT INTO `ArtistInfo` VALUES(6, 'Soumyadip', 'Hyderabad, India -- http://platform.qochi.com/platform/assets/Soumyadip.png');
INSERT INTO `ArtistInfo` VALUES(8, 'David Vordtriede', 'St. Louis, Missouri -- http://platform.qochi.com/platform/assets/David.png');

-- --------------------------------------------------------

--
-- Table structure for table `Feed`
--

CREATE TABLE `Feed` (
  `id` int(11) NOT NULL auto_increment,
  `packid` varchar(500) default NULL,
  `imageid` varchar(500) default NULL,
  `text` varchar(1000) default NULL,
  `userid` varchar(500) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_Feed_1_idx` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `Feed`
--

INSERT INTO `Feed` VALUES(1, 'pack523cc5cc9900a-jpg', 'image523cc7c463624.jpg', 'A quick brown fox jumps over the lazy dog. A quick brown fox jumps over the lazy dog. A quick brown fox jumps over the lazy dog.', 'adminqa');
INSERT INTO `Feed` VALUES(2, 'pack523cc5cc9900a-jpg', 'mage523cc7c463624.jpg', 'Creating First Feed Item', 'salman_paracha');
INSERT INTO `Feed` VALUES(3, 'pack523cc5cc9900a-jpg', 'image523cc7c463624.jpg', 'Create Second Notoji', 'salman_paracha');
INSERT INTO `Feed` VALUES(4, 'pack523cc5cc9900a-jpg', 'mage523cc7c463624.jpg', 'amrit', 'salman_paracha');
INSERT INTO `Feed` VALUES(5, 'pack52cb9497857a4-png', 'image52cb955ac9e89.png', 'Hello Hello Hello', 'amrit');
INSERT INTO `Feed` VALUES(6, 'pack52cb9497857a4-png', 'image52cb955ac9e89.png', 'Hello Hello Hello', 'amrit');
INSERT INTO `Feed` VALUES(7, 'pack52cb9497857a4-png', 'image52cb955ac9e89.png', 'Hello Hello Hello', 'amrit');
INSERT INTO `Feed` VALUES(8, 'pack52cb9497857a4-png', 'image52cb955deecc0.png', 'Hello everybody', '1');
INSERT INTO `Feed` VALUES(9, 'pack52cb96cfedd60-png', 'image52cb9780ef91d.png', 'Tahidfghfh', '1');
INSERT INTO `Feed` VALUES(10, 'pack52cb96cfedd60-png', 'image52cb97809f145.png', 'Mukku g', '1');
INSERT INTO `Feed` VALUES(11, 'pack52cb99325823b-png', 'image52cb99b00f178.png', 'No way', '1');
INSERT INTO `Feed` VALUES(12, 'pack52cb9497857a4-png', 'image52cb955ca3388.png', 'Hjfjfy h,', '1');
INSERT INTO `Feed` VALUES(13, 'pack52cb99325823b-png', 'image52cb99af67a67.png', 'BBC', '1');
INSERT INTO `Feed` VALUES(14, 'pack52cb9497857a4-png', 'image52cb955bccb3e.png', 'This is for new id', '1');
INSERT INTO `Feed` VALUES(15, 'pack52cb9497857a4-png', 'image52cb955e32acc.png', 'Fdghdfgdhfhgdsfdf', '100006533104895');
INSERT INTO `Feed` VALUES(16, 'pack52cb96cfedd60-png', 'image52cb978324826.png', 'Lala ji kya hal hai', '100006533104895');
INSERT INTO `Feed` VALUES(17, 'pack52cb9a1cb6a02-png', 'image52cb9ac3770c6.png', 'Ramesh notoji', '100006533104895');
INSERT INTO `Feed` VALUES(18, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Mukesh chacha', '100006533104895');
INSERT INTO `Feed` VALUES(19, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Mukesh chacha', '100006533104895');
INSERT INTO `Feed` VALUES(20, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Tap here to create your status\n	message', '100006533104895');
INSERT INTO `Feed` VALUES(21, 'pack52cb9b2609801-png', 'image52cb9b945d736.png', 'Hello', '100006533104895');
INSERT INTO `Feed` VALUES(22, 'pack52cb9b2609801-png', 'image52cb9b96e6763.png', 'Hey wats up', '100006533104895');
INSERT INTO `Feed` VALUES(23, 'pack52cb9b2609801-png', 'image52cb9b94554d0.png', 'Hxhx bb', '100006533104895');
INSERT INTO `Feed` VALUES(24, 'pack52cb9497857a4-png', 'image52cb955b18f93.png', 'Its very cool..', '100002353362874');
INSERT INTO `Feed` VALUES(25, 'pack52cb9497857a4-png', 'image52cb955b18f93.png', 'Gjhgjhgjh', '100002353362874');
INSERT INTO `Feed` VALUES(26, 'pack52cb99325823b-png', 'image52cb99af7003c.png', 'Hey let\\''s party', '710412425');
INSERT INTO `Feed` VALUES(27, 'pack52cb99325823b-png', 'image52cb99af7003c.png', 'I\\''ll make you an offer you can\\''t refuse.', '25318090');
INSERT INTO `Feed` VALUES(28, 'pack52cb99325823b-png', 'image52cb99aed608f.png', 'No kicking', '710412425');
INSERT INTO `Feed` VALUES(29, 'pack52cb99325823b-png', 'image52cb99af67a67.png', 'Burghers chfvbh', '710412425');
INSERT INTO `Feed` VALUES(30, 'pack52cb9b2609801-png', 'image52cb9b945d736.png', 'Kgcgg gghh', '710412425');
INSERT INTO `Feed` VALUES(31, 'pack52cb99325823b-png', 'image52cb99b0a591f.png', 'Hiiiiiiiiiiiii', '710412425');
INSERT INTO `Feed` VALUES(32, 'pack52cb99325823b-png', 'image52cb99b0a591f.png', 'Hiiiiiiiiiiiiiiiii', '710412425');
INSERT INTO `Feed` VALUES(33, 'pack52cb9b2609801-png', 'image52cb9b9614b82.png', 'Ghbjhjk', '710412425');
INSERT INTO `Feed` VALUES(34, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Live life king size', '100002353362874');
INSERT INTO `Feed` VALUES(35, 'pack52cb96cfedd60-png', 'image52cb97839cc91.png', 'Hey bony', '100002353362874');
INSERT INTO `Feed` VALUES(36, 'pack52cb99325823b-png', 'image52cb99b00f178.png', 'You no messy with me ...', '25318090');
INSERT INTO `Feed` VALUES(37, 'pack52cb99325823b-png', 'image52cb99b00f178.png', 'You no messy with me ...', '25318090');
INSERT INTO `Feed` VALUES(38, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'What\\''s going on?', '879185566');
INSERT INTO `Feed` VALUES(39, 'pack52cb96cfedd60-png', 'image52cb9781c3db9.png', 'Tap here to create your status\n	message', '879185566');
INSERT INTO `Feed` VALUES(40, 'pack52cb96cfedd60-png', 'image52cb9780d7fc2.png', 'Tap here to create your status\n	message', '879185566');
INSERT INTO `Feed` VALUES(41, 'pack52cb96cfedd60-png', 'image52cb977fc6122.png', 'Testomatic', '25318090');
INSERT INTO `Feed` VALUES(42, 'pack52cb99325823b-png', 'image52cb99b025603.png', 'Love is like music that want to listen to till my last breathe!', '879185566');
INSERT INTO `Feed` VALUES(43, 'pack52cb99325823b-png', 'image52cb99b0a591f.png', 'Salman Paracha ki billi bolay meow', '25318090');
INSERT INTO `Feed` VALUES(44, 'pack52cb9f265647e-png', 'image52cb9fbda9659.png', 'Hiiiiii emoji testing', '100006533104895');
INSERT INTO `Feed` VALUES(45, 'pack52cb96cfedd60-png', 'image52cb9782e9c2c.png', 'Hmm. I see the light at the end of the tunnel', '2024225');
INSERT INTO `Feed` VALUES(46, 'pack52cb9c1a082e2-png', 'image52cb9c9570016.png', 'Heyyyy. I\\''m Otis and I like to go spelunking', '2024225');
INSERT INTO `Feed` VALUES(47, 'pack52cba1aa94acf-png', 'image52cba282ad87c.png', 'Time for some karaoke!', '25318090');
INSERT INTO `Feed` VALUES(48, 'pack52cb96cfedd60-png', 'image52cb9781d3789.png', 'This is what I feel as an AMZN investor at the moment. Bezos!', '2024225');
INSERT INTO `Feed` VALUES(49, 'pack52cb96cfedd60-png', 'image52cb977fed9ae.png', 'Lovely little cartucho', '2024225');
INSERT INTO `Feed` VALUES(50, 'pack52cb96cfedd60-png', 'image52cb9782e9c2c.png', 'I\\''ll think about it.', '25318090');
INSERT INTO `Feed` VALUES(51, 'pack52cb99325823b-png', 'image52cb99afdc6a0.png', 'Hipster', '25318090');
INSERT INTO `Feed` VALUES(52, 'pack52cb96cfedd60-png', 'image52cb97809f145.png', 'Hgjhgshdg', '100002353362874');
INSERT INTO `Feed` VALUES(53, 'pack52cb9497857a4-png', 'image52cb955cd8610.png', 'I have a brilliant idea.', '25318090');
INSERT INTO `Feed` VALUES(54, 'pack52cb9497857a4-png', 'image52cb955b03dcc.png', 'Nidhiakjdhjdhchjjjvbgkdyvghcfjchggdgddghjhfsfhxghcghfhbcggdfghffgcffhhh chhvcg chhvcg hhhhhh ghhhh', '100006533104895');
INSERT INTO `Feed` VALUES(55, 'pack52cb9a1cb6a02-png', 'image52cb9ac1e760a.png', 'Hi this is anotoji', '100001911886454');
INSERT INTO `Feed` VALUES(56, 'pack52ee9c92dc225-png', 'image52ee9d069e2e8.png', 'don\\''t waste your time', '100001911886454');
INSERT INTO `Feed` VALUES(57, 'pack52cb9b2609801-png', 'image52cb9b952dee4.png', 'Ummmm Bangkok cafe', '2024225');
INSERT INTO `Feed` VALUES(58, 'pack52cb9497857a4-png', 'image52cb955d46abb.png', 'Vvbhgjjjlljjh.   Ghh h g g b b', '100006533104895');
INSERT INTO `Feed` VALUES(59, 'pack52cb96cfedd60-png', 'image52cb9782e5b7a.png', 'Having wings with Nomi.', '2024225');
INSERT INTO `Feed` VALUES(60, 'pack52cb96cfedd60-png', 'image52cb977fd0397.png', 'Tap here to create your status\n	message', '2024225');
INSERT INTO `Feed` VALUES(61, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'hjhgjhgsjdfgjhgsdhfghsdhsdgjhsgdjhgsjhgjhdfghsgdhgsjhdfgjhsgdjhsgjhgsdjhgsjhdgjhsgjhfdgjhsgjhdfsgdshjfgjhsgjhgjhgshjgdshjsgjhgsjhgjhdgsjghjd', '100001911886454');
INSERT INTO `Feed` VALUES(62, 'pack52cb9c1a082e2-png', 'image52cb9c957d55b.png', 'A great step forward. I think I finally see a light at the end of the tunnel', '2024225');
INSERT INTO `Feed` VALUES(63, 'pack52cba1aa94acf-png', 'image52cba28228e29.png', 'Woh. It\\''s raining cats and dogs.   Hoping for this nasty weather to let up', '2024225');
INSERT INTO `Feed` VALUES(64, 'pack52ee9c92dc225-png', 'image52ee9d066354e.png', 'kkjhfkjhkjsdhfkjhsdkjhfkjsdhfkjhsdkjfhkjdshfjkhdskjhkdjfhgkjdshgjkhdkjghkdsjhkjdshgjhdskjhdskjhksjhkjdfshkjsdhkhkjdshkjdshgkjd.  46465456465', '100001911886454');
INSERT INTO `Feed` VALUES(65, 'pack52ee9c92dc225-png', 'image52ee9d066354e.png', 'Iluyhiluyioui', '100001911886454');
INSERT INTO `Feed` VALUES(66, 'pack52ee9c92dc225-png', 'image52ee9d0a67125.png', '6rturturturtb tyrty', '100001911886454');
INSERT INTO `Feed` VALUES(67, 'pack52ee9c92dc225-png', 'image52ee9d069e2e8.png', 'Yutuytyutyutyu', '100001911886454');
INSERT INTO `Feed` VALUES(68, 'pack52ee9c92dc225-png', 'image52ee9d066a0a4.png', 'Dfygdrrdhfh', '100001911886454');
INSERT INTO `Feed` VALUES(69, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'hjhkjshdfkjhkjshfkjshdfkjhskjdfhkjhdfkjshfdkjhsdfkjhskdjfhkjsfhkjshfkjhkjhskdjfhkjsdhkjhskjdhfkjshkjshfkjhsfkjhkjshjkshkjjshfkjhkjdfhkjs5555', '100001911886454');
INSERT INTO `Feed` VALUES(70, 'pack52cb96cfedd60-png', 'image52cb977fc6122.png', 'jgjhgdjfgdsjfgjshgfjsdgjfgsjgfjhgsjhfdgjhgsdfjhgsjdfhgjsgdfjhsdgfjgsjhgfjdhsgfjhsgfjgshgfjhsgfjhgsdhfgjhsgdfjsgdfjhsgdfjhgsjdfgjsdhgfjhdsgfj', '100001911886454');
INSERT INTO `Feed` VALUES(71, 'pack52cb96cfedd60-png', 'image52cb977fed9ae.png', 'Ujikhjkjhkhjkhjkhj', '100001911886454');
INSERT INTO `Feed` VALUES(72, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Nm,bm,bvmnvbmnvbmnbnm', '100001911886454');
INSERT INTO `Feed` VALUES(73, 'pack52cba1aa94acf-png', 'image52cba284133c0.png', 'I hope you feel better, and that all your wishes and dreams come true. Keep your chin up. I am sure things will turn upwards soon inshallah', '2024225');
INSERT INTO `Feed` VALUES(74, 'pack52cb9a1cb6a02-png', 'image52cb9ac2a0755.png', 'Tim cook aur Carl ichan Nomi ki Gand marnay Ka plan bana rahai hain. Prediction? Apple down $100. Nomi Tayyar hai tu?', '2024225');
INSERT INTO `Feed` VALUES(75, 'pack52cba1aa94acf-png', 'image52cba282c6b83.png', 'This is Ben bernanke. After he fucked Nomi over for $50k last year.', '2024225');
INSERT INTO `Feed` VALUES(76, 'pack52ee9c92dc225-png', 'image52ee9d0809232.png', 'Birds flyin\\'' high ... you know how I feel.', '25318090');
INSERT INTO `Feed` VALUES(77, 'pack52cb96cfedd60-png', 'image52cb978244f79.png', 'Isn\\''t it getting late in Austin? What you doin still chatting on Facebook. Tomorrow is a work day homie', '2024225');
INSERT INTO `Feed` VALUES(78, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(79, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(80, 'pack52cb96cfedd60-png', 'image52cb978244f79.png', 'Tap on the image to select a sticker. Tap here to type your message', '2024225');
INSERT INTO `Feed` VALUES(81, 'pack52cb96cfedd60-png', 'image52cb977fc6122.png', 'Eating lunch with Rohit, and scheming plans to change the world. Good times for sure', '2024225');
INSERT INTO `Feed` VALUES(82, 'pack52cb9497857a4-png', 'image52cb955b03dcc.png', 'Gfhgfghfhgf', '100001911886454');
INSERT INTO `Feed` VALUES(83, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(84, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(85, 'pack52cb96cfedd60-png', 'image52cb97809f145.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(86, 'pack52cb99325823b-png', 'image52cb99aed608f.png', 'Sad.', '25318090');
INSERT INTO `Feed` VALUES(87, 'pack52cba1aa94acf-png', 'image52cba2839db5e.png', 'THank god it\\''s a fridaaaayyy. Time to get ready for the party animal who arrives from SFO. Anas munir, night owl Mai holahu on hai', '2024225');
INSERT INTO `Feed` VALUES(88, 'pack52cb9b2609801-png', 'image52cb9b96cab2b.png', 'Inshallah. My heart tells me that things will turn out in dad\\''s favor. I have a positive feeling about it all. You guys will be fine.', '2024225');
INSERT INTO `Feed` VALUES(89, 'pack52cb9497857a4-png', 'image52cb955b18f93.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(90, 'pack52cb9497857a4-png', 'image52cb955b18f93.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(91, 'pack52cb99325823b-png', 'image52cb99aed608f.png', 'Whaaaaa', '25318090');
INSERT INTO `Feed` VALUES(92, 'pack52cb96cfedd60-png', 'image52cb977fd36ed.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(93, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(94, 'pack52cb96cfedd60-png', 'image52cb977fcab14.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(95, 'pack52ee9c92dc225-png', 'image52ee9d09eb322.png', 'Making stuff happen. It\\''s been a struggle these past couple of weeks, but things are coming together.', '2024225');
INSERT INTO `Feed` VALUES(96, 'pack52ee9c92dc225-png', 'image52ee9d066354e.png', 'Grrrrr ...', '25318090');
INSERT INTO `Feed` VALUES(97, 'pack52cb9b2609801-png', 'image52cb9b960e974.png', 'Nazla zukaam', '25318090');
INSERT INTO `Feed` VALUES(98, 'pack52cb9a1cb6a02-png', 'image52cb9ac3770c6.png', 'Peace. Love and happiness. Don\\''t fret the \\"besties\\" in Seattle. Maria can\\''t replace you. But if you don\\''t come to visit, things can change..', '2024225');
INSERT INTO `Feed` VALUES(99, 'pack52cb9497857a4-png', 'image52cb955bccb3e.png', 'Had a great time munching and chatting. What are we doing tomorrow?', '2021720');
INSERT INTO `Feed` VALUES(100, 'pack52cb9497857a4-png', 'image52cb955ac9e89.png', 'eryerwyriuywiueyriuwyiuerywiuyeriuywieuryiwuyeriuwyirueywiueyriwuyeriuywiueryiwueyriuwyeriuyweiruyiuyiweuyriuyweriuyiweuryiuweyriuyiwueyriuy', '100001911886454');
INSERT INTO `Feed` VALUES(101, 'pack52ee9c92dc225-png', 'image52ee9d0863980.png', 'Mudassar stop trying to impress us with your business trips. We know you are sitting in your backyard slacking. You hippie', '2024225');
INSERT INTO `Feed` VALUES(102, 'pack52cb9c1a082e2-png', 'image52cb9c950c81b.png', 'Hhjhsdkjfhkjshdfkjhskjdfhksjhfdkjshdfkjhskjhfdkjshfkjhskjdfhkjshdfkjshdkfjhskjdhfkjshdfkjhsdkfjhskjdhfkjsdhfkjshdfkjhsdkfjhskdjfhksjdhfkj687', '100001911886454');
INSERT INTO `Feed` VALUES(103, 'pack52ee9c92dc225-png', 'image52ee9d0684862.png', 'Anas Munir, DDLJ dekhnay kay baad ...', '25318090');
INSERT INTO `Feed` VALUES(104, 'pack52cb9c1a082e2-png', 'image52cb9c9673f5a.png', 'It\\''s just one of those days again. Late night netflixing is going to cost me today. On the bright side, I\\''m done with house of cards.', '2024225');
INSERT INTO `Feed` VALUES(105, 'pack52cba1aa94acf-png', 'image52cba28380040.png', 'Hey David. This is Salman Paracha. Testing out Kai in Notoji. Hopefully you will have a build to play with soon.', '2024225');
INSERT INTO `Feed` VALUES(106, 'pack52cb9b2609801-png', 'image52cb9b9614b82.png', 'Umair looking dashing at cactus cafe. But Shifaat looks like he just sold drugs on the street. Get some clothes man', '2024225');
INSERT INTO `Feed` VALUES(107, 'pack52cb9c1a082e2-png', 'image52cb9c9570016.png', 'Text', '25318090');
INSERT INTO `Feed` VALUES(108, 'pack52cb9a1cb6a02-png', 'image52cb9ac2a0755.png', 'Laala nai aaj India ki sahi bajai. O poranay paapi teray saray ghuna mauf. Boom! Boom! Afridi!!!', '2024225');
INSERT INTO `Feed` VALUES(109, 'pack52cb9497857a4-png', 'image52cb955de8fdf.png', 'Assssshhhhh!!!!!', '596015253');
INSERT INTO `Feed` VALUES(110, 'pack52cb9497857a4-png', 'image52cb955b18f93.png', 'Hola', '596015253');
INSERT INTO `Feed` VALUES(111, 'pack52cb9a1cb6a02-png', 'image52cb9ac2a4924.png', 'If the house wives of Seattle keep on promoting their hangouts, it will cause social unrest. Social circles will be in chaos.', '2024225');
INSERT INTO `Feed` VALUES(112, 'pack52cb9c1a082e2-png', 'image52cb9c968d113.png', 'This will be me after Saturday night. I sense an onslaught coming my way. Zamana bauhat kharab hai bach Kai aye haseenon', '2024225');
INSERT INTO `Feed` VALUES(113, 'pack52cb9c1a082e2-png', 'image52cb9c970f4cc.png', 'To all my peeps in Florida. Xoxo. I get reminded every month why I love you all. I mean everyday of course.', '2024225');
INSERT INTO `Feed` VALUES(114, 'pack52cb96cfedd60-png', 'image52cb9780ef91d.png', 'No one puts Baby in a corner.', '25318090');
INSERT INTO `Feed` VALUES(116, 'pack52cb9e1fa2d35-png', 'image52cb9ed35b78c.png', 'Karaoke night. Poker night. Zinger night. It\\''s time for the hang ama to start. And beware of Salman. He has his guard up', '2021720');
INSERT INTO `Feed` VALUES(117, 'pack52cba1aa94acf-png', 'image52cba2820042b.png', 'Tap on the image to select a sticker. Tap here to type your message', '25318090');
INSERT INTO `Feed` VALUES(118, 'pack52cb96cfedd60-png', 'image52cb9782e9bbf.png', 'I am determined to roll back the pounds. I am determined to toll back the pounds. I am......', '2024225');
INSERT INTO `Feed` VALUES(119, 'pack52ee9c92dc225-png', 'image52ee9d0975b2b.png', 'Ishq-e-ishq-e-tere-ishq-e', '25318090');
INSERT INTO `Feed` VALUES(120, 'pack52cb9c1a082e2-png', 'image52cb9c9570016.png', 'Let\\''s test this experience on Facebook and see what we see. I really hope to resolve this issue soon. It\\''s really bugging me', '100007959189831');
INSERT INTO `Feed` VALUES(121, 'pack52cb96cfedd60-png', 'image52cb977fd0397.png', 'Tap on the image to select a sticker. Tap here to type your message', '100001911886454');
INSERT INTO `Feed` VALUES(122, 'pack52cb96cfedd60-png', 'image52cb977fd0397.png', 'Sdfsfdsfsdf', '100001911886454');
INSERT INTO `Feed` VALUES(123, 'pack52cb9b2609801-png', 'image52cb9b95729dd.png', 'A bit exhausted and drained out. It\\''s time to recharge my batteries. Looking forward to whistler in June.  Canada here I come', '2024225');
INSERT INTO `Feed` VALUES(124, 'pack52cb96cfedd60-png', 'image52cb977fedf79.png', 'Na ja Rahul. Khair koi tension naheen. Milain gay break Kai baad. Khayal rakh and kick some ass when you graduate', '2024225');
INSERT INTO `Feed` VALUES(125, 'pack52cb96cfedd60-png', 'image52cb9782e5b7a.png', 'Burger hut Ka burger on hai. Nabeel apna wallet khol. Aaj Teri treat hai', '2024225');
INSERT INTO `Feed` VALUES(126, 'pack52cb9a1cb6a02-png', 'image52cb9ac2a64e2.png', 'Sleepy ...', '25318090');
INSERT INTO `Feed` VALUES(127, 'pack52cb9a1cb6a02-png', 'image52cb9ac1e760a.png', 'Tap on the image to select a sticker. Tap here to type your message', '25318090');
INSERT INTO `Feed` VALUES(128, 'pack52cb9c1a082e2-png', 'image52cb9c950c81b.png', 'Ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp', '25318090');

-- --------------------------------------------------------

--
-- Table structure for table `ImageMetaData`
--

CREATE TABLE `ImageMetaData` (
  `id` varchar(500) NOT NULL,
  `packid` varchar(45) default NULL,
  `title` varchar(256) default NULL,
  `description` varchar(1024) default NULL,
  `tags` varchar(1024) default NULL,
  `userid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `id_idx` (`packid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ImageMetaData`
--

INSERT INTO `ImageMetaData` VALUES('image52cb9ed57d4a5.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed57c283.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed57b189.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed502b41.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed51266a.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed48d662.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed4ec4c5.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed491844.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed484d64.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed47fdb1.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed46927b.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed4653cd.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed3ec18e.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed3e4f30.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed3d9b7a.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed3d12f7.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed3cee1d.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed3c4556.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed35b78c.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed35591f.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed34ac6a.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed3437f1.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed338e3f.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed32e926.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c988b84d.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c950c81b.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b96e6763.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b96cab2b.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b9650483.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b963f6e8.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b9627021.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b9624781.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b9614b82.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b960e974.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b95729dd.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b955633c.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b954b6e4.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b9546cba.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b95408b7.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b952dee4.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b946440b.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b945d736.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b94582ad.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b9457209.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac3770c6.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b943fc8b.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9b94554d0.png', 'pack52cb9b2609801-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c98711f8.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c9842b5c.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c98132b7.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c980c381.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c97a7017.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c979fcc3.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c9787d93.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c977cae9.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c9722c8f.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c970f4cc.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c968d113.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c968a4fd.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c9673f5a.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c96600bf.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c9654b42.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c964fb96.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c95f2c9e.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c9589d3c.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c9587004.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c957d55b.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c9570016.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9c954e148.png', 'pack52cb9c1a082e2-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac36765c.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac32fdad.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac32e5ce.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac32cf1e.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac32a1cc.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac311d4b.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac303f66.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac2b03c2.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac2a64e2.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac2a5578.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac2a4924.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac2a0755.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac286565.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac20330a.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac201e14.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac1f0571.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac1efb3c.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac1e7941.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ac1e760a.png', 'pack52cb9a1cb6a02-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb97839cc91.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb978324826.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9782e9bbf.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9782e9c2c.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9782e8afa.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9782e77bd.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9782e5b7a.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb978244f79.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb978225309.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9781d3789.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9781c3db9.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9781b9d96.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb97819d2ed.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb97815c72b.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9780ef91d.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9780d7fc2.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9780be810.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb97809f145.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb978071b7c.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb977fedf79.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb977fed9ae.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb977fd36ed.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb977fd0397.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb977fcab14.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb977fc6122.png', 'pack52cb96cfedd60-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955ee0349.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955edf3a1.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955ecde3b.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955e32acc.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955e321ef.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955deecc0.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955def2c3.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955ded5b6.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955de8fdf.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955d7894a.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955d50392.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955d46abb.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955cee853.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955cd8610.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955ca3388.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955c7b52d.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955c5bc2f.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955c1c0a2.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955bce730.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955bccb3e.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955b89995.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955b44e67.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955b18f93.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955b03dcc.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb955ac9e89.png', 'pack52cb9497857a4-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa3ef9a901d.jpg', 'pack52aa3ec60400a-jpg', 'testre', 'sdsada', ',sdsd', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b98639c1.jpg', 'pack52aa3ec60400a-jpg', 'D-Day', 'fdf', ',dfd', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b98adde9.jpg', 'pack52aa3ec60400a-jpg', 'dfdf', 'dfdfd', ',dfdf', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b99244c9.jpg', 'pack52aa3ec60400a-jpg', 'dfdf', 'fdsfsd', ',dfdfds', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b993992d.jpg', 'pack52aa3ec60400a-jpg', 'dfdfd', 'fsdf', ',fdfd', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b999ca57.jpg', 'pack52aa3ec60400a-jpg', 'dff', 'dfd', ',dfd', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b9a03283.jpg', 'pack52aa3ec60400a-jpg', 'dfd5454', 'dfsdfsdf', ',erwe453', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b9a3ee84.jpg', 'pack52aa3ec60400a-jpg', 'fdfd', 'dsfsdf', ',fdsfsdf', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b9b8eb24.jpg', 'pack52aa3ec60400a-jpg', 'dfdsf', 'gdfgd', ',dfrtrt', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4b9b90b3b.jpg', 'pack52aa3ec60400a-jpg', 'dfsd', 'dfgdfgd', ',4w', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4c262d2b5.jpg', 'pack52aa3ec60400a-jpg', 'dsfsdfs', 'dfdsfsdfsdfs', ',dfffsD', 1);
INSERT INTO `ImageMetaData` VALUES('image52aa4c29a0bbe.png', 'pack52aa3ec60400a-jpg', 'Large', 'hghjggk', ',mlmkll', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed57c396.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed57b1ef.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed5807a9.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cb9ed5e930d.png', 'pack52cb9e1fa2d35-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0a67125.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0a16224.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d09eb322.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d09c0cc4.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d099c301.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0975b2b.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0926555.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0896720.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0863980.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d085d3a9.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0809232.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d07d02ad.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d07c52d2.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d069e2e8.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0684862.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d067066d.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d066a0a4.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d066354e.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52ee9d0655f8d.png', 'pack52ee9c92dc225-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba2820042b.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba2820eb87.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba28214812.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba28216abd.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba282176f2.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba28228e29.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba282ad87c.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba282af2c5.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba282b041b.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba282b80ca.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba282c6b83.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba282ce67a.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba28346e59.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba283784c0.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba2837836e.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba28380040.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba28384237.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba2839db5e.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba284133c0.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba2841c62a.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba2841ed5d.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba284351fd.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba2843c329.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba2845627d.png', 'pack52cba1aa94acf-png', '', '', '', 1);
INSERT INTO `ImageMetaData` VALUES('image52cba284c2f1f.png', 'pack52cba1aa94acf-png', '', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `PackMetaData`
--

CREATE TABLE `PackMetaData` (
  `id` varchar(500) NOT NULL,
  `title` varchar(256) default NULL,
  `description` varchar(1024) default NULL,
  `tags` varchar(1024) default NULL,
  `userid` int(11) default NULL,
  `artistid` int(11) NOT NULL,
  `dominantcolor` varchar(45) default NULL,
  `attribcolor` varchar(45) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PackMetaData`
--

INSERT INTO `PackMetaData` VALUES('pack52cb9497857a4-png', 'Sally', 'A quirkly gril scout that loves the outdoors and her long winding hair.', ',sally,girl', 1, 2, '', '');
INSERT INTO `PackMetaData` VALUES('pack52cb96cfedd60-png', 'Cartucho', 'Born and raised in a small town near New Mexico, Cartucho runs a doggie dating service in the big apple.', '', 1, 2, '', '');
INSERT INTO `PackMetaData` VALUES('pack52cb9a1cb6a02-png', 'Killer', 'A wrestler bunny that loves to crush hearts and bones.', ',killer rubens', 1, 4, '', '');
INSERT INTO `PackMetaData` VALUES('pack52cb9b2609801-png', 'Yatta', 'Don\\''t be fooled by her cute looks. She means business.', ',yatta rubens', 1, 4, '', '');
INSERT INTO `PackMetaData` VALUES('pack52cb9c1a082e2-png', 'Otis & Rae', 'Two charming and cheekie little creatures whose humor and pluck guide them through adventures.', '', 1, 5, '', '');
INSERT INTO `PackMetaData` VALUES('pack52cb9e1fa2d35-png', 'Dodo', 'Spurs mischief and laughter wherever he goes.', ',dodo soumya', 1, 6, '', '');
INSERT INTO `PackMetaData` VALUES('pack52cba1aa94acf-png', 'Kai', 'A Tiki god who watches over the sacred islands of Hawaii. When not on watch, he sneaks out to grab a few waves', '', 1, 8, '', '');
INSERT INTO `PackMetaData` VALUES('pack52ee9c92dc225-png', 'CircuitX', 'Why a Robot? Why not a horse, or a beetle, or a bald eagle? I\\''m saying this more as, like, existentialism, you know? Who am I? And how can a Robot ever be happy without, you\\''ll forgive the expression, a chicken in its teeth?', ',circuitX', 1, 3, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `Role`
--

CREATE TABLE `Role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(128) default NULL,
  `apis` varchar(1024) default NULL,
  `admintype` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Role`
--

INSERT INTO `Role` VALUES(1, 'SuperAdmin', '', 2);
INSERT INTO `Role` VALUES(3, 'Admin', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Subscriptions`
--

CREATE TABLE `Subscriptions` (
  `id` int(11) NOT NULL auto_increment,
  `unique_user_id` varchar(256) NOT NULL,
  `packid` varchar(500) default NULL,
  `timestamp` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `Subscriptions`
--

INSERT INTO `Subscriptions` VALUES(3, 'salman_paracha', 'pack52cb9c1a082e2-png', '2014-01-12 00:44:01');
INSERT INTO `Subscriptions` VALUES(32, '2024225', 'pack52cb9b2609801-png', '2014-02-25 09:47:17');
INSERT INTO `Subscriptions` VALUES(31, '100001911886454', 'pack52cba1aa94acf-png', '2014-02-25 05:08:01');
INSERT INTO `Subscriptions` VALUES(30, '100001911886454', 'pack52cb9c1a082e2-png', '2014-02-25 05:01:01');
INSERT INTO `Subscriptions` VALUES(29, '100001911886454', 'pack52cb9497857a4-png', '2014-02-25 04:55:15');
INSERT INTO `Subscriptions` VALUES(25, 'salman_paracha', 'pack52cb9b2609801-png', '2014-01-12 19:22:33');
INSERT INTO `Subscriptions` VALUES(28, '100001911886454', 'pack52cb9a1cb6a02-png', '2014-02-25 04:38:45');
INSERT INTO `Subscriptions` VALUES(27, '100001911886454', 'pack52cb96cfedd60-png', '2014-02-25 04:25:24');
INSERT INTO `Subscriptions` VALUES(26, '100001911886454', 'pack52ee9c92dc225-png', '2014-02-25 03:23:09');
INSERT INTO `Subscriptions` VALUES(33, '2024225', 'pack52cb9497857a4-png', '2014-02-25 09:49:18');
INSERT INTO `Subscriptions` VALUES(34, '2024225', 'pack52cb9e1fa2d35-png', '2014-02-25 10:09:19');
INSERT INTO `Subscriptions` VALUES(35, '2024225', 'pack52cb9c1a082e2-png', '2014-02-25 17:08:26');
INSERT INTO `Subscriptions` VALUES(36, '2024225', 'pack52cb9a1cb6a02-png', '2014-02-25 18:56:24');
INSERT INTO `Subscriptions` VALUES(37, '2024225', 'pack52cb96cfedd60-png', '2014-02-26 08:48:29');
INSERT INTO `Subscriptions` VALUES(38, '2024225', 'pack52ee9c92dc225-png', '2014-02-26 08:56:58');
INSERT INTO `Subscriptions` VALUES(39, '25318090', 'pack52ee9c92dc225-png', '2014-02-26 09:09:02');
INSERT INTO `Subscriptions` VALUES(40, '25318090', 'pack52cb9c1a082e2-png', '2014-02-26 09:11:34');
INSERT INTO `Subscriptions` VALUES(41, '25318090', 'pack52cb9b2609801-png', '2014-02-26 09:13:06');
INSERT INTO `Subscriptions` VALUES(42, '2021720', 'pack52cb9497857a4-png', '2014-02-26 09:14:34');
INSERT INTO `Subscriptions` VALUES(43, '2021720', 'pack52cba1aa94acf-png', '2014-02-26 09:15:02');
INSERT INTO `Subscriptions` VALUES(44, '25318090', 'pack52cb9e1fa2d35-png', '2014-02-26 09:18:33');
INSERT INTO `Subscriptions` VALUES(45, '2024225', 'pack52cba1aa94acf-png', '2014-02-26 19:49:34');
INSERT INTO `Subscriptions` VALUES(46, '2021720', 'pack52ee9c92dc225-png', '2014-02-26 20:15:05');
INSERT INTO `Subscriptions` VALUES(47, '2021720', 'pack52cb9c1a082e2-png', '2014-02-27 08:50:53');
INSERT INTO `Subscriptions` VALUES(48, '100006533104895', 'pack52cb9497857a4-png', '2014-02-28 09:19:26');
INSERT INTO `Subscriptions` VALUES(49, '25318090', 'pack52cba1aa94acf-png', '2014-02-28 23:35:38');
INSERT INTO `Subscriptions` VALUES(50, '100006533104895', 'pack52cb96cfedd60-png', '2014-03-04 06:57:27');
INSERT INTO `Subscriptions` VALUES(51, '596015253', 'pack52cb9497857a4-png', '2014-03-04 10:16:15');
INSERT INTO `Subscriptions` VALUES(52, '25318090', 'pack52cb96cfedd60-png', '2014-03-08 00:13:40');
INSERT INTO `Subscriptions` VALUES(53, '2021720', 'pack52cb9e1fa2d35-png', '2014-03-08 22:26:52');
INSERT INTO `Subscriptions` VALUES(54, '100007959189831', 'pack52cb9c1a082e2-png', '2014-03-13 15:14:28');
INSERT INTO `Subscriptions` VALUES(55, '25318090', 'pack52cb9a1cb6a02-png', '2014-03-17 13:43:31');
INSERT INTO `Subscriptions` VALUES(56, '2021720', 'pack52cb9b2609801-png', '2014-03-20 18:10:24');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `fullname` varchar(128) default NULL,
  `roleid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `id_idx` (`roleid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` VALUES(1, 'adminqa', 'JDYkcm91bmRzPTUwMDAwJFg5WGpDSGtMNHBXNUtlY2gkTlZUdU9jMC8weHRackpLU01UYkRSQTEzTW1HN0F3aFkwdFhUejVqTzIwblVyam4wQjJsQ0hPQjRlZThMcnRzbFRhWHBYNksvd0xrQ2diTmZLbE12UjA=', 'Administrator', 1);
INSERT INTO `Users` VALUES(2, 'salman', 'JDYkcm91bmRzPTUwMDAwJG9yU0V0S1dYSm12S3IvcXckcG9XeERpNWQ0Wi5Na0t2NUNiWFlqNklIQnZ0dzVsZW5MMmQ3NnBtUFhDR0pnWXpJejd6VHpQcWNGOFNKZk15Z0txcTZleHVaLjM0LllpMHo5MEIuVS8=', 'Salman Paracha', 3);
INSERT INTO `Users` VALUES(3, 'notoji', 'JDYkcm91bmRzPTUwMDAwJC96UmFGZWVUTUJlbTllaFYkWmhnYkhYdHlNc2JmYmRuNVp1ZHpmVFBEVHBwcmV6WmtxOUNRb1JzMlpEYjlaUjhUNVlnUXZ6V0dWMjlramc0dEMvRGlIRUJ5bnFMU1V1L0ZEbmM2Vi4=', 'Notoji', 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Users`
--
ALTER TABLE `Users`
  ADD CONSTRAINT `id` FOREIGN KEY (`roleid`) REFERENCES `Role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;