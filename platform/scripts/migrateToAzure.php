<?php

include_once __DIR__ . '/../wrappers/AzureStorage.php';
include_once __DIR__ . '/../wrappers/ImageStorage.php';

function migrateToAzure()
{
	$blobsStorageClient = new AzureStorage();
	$filesStorageClient = new ImageStorage();

	$containers = $filesStorageClient->getContainersList();

	echo "***************************************************<br/>\n";
	echo "* File storage to Azure Storage Migration Utility *<br/>\n";
	echo "***************************************************<br/>\n";

	foreach($containers as $key => $container)
	{
		$containerName = substr(strrchr($container,'/'),1);
		echo "Creating container: " . $containerName . "<br/>\n";

		try
		{
			$blobsStorageClient->createContainer($containerName);
		}
		catch(Microsoft_WindowsAzure_Exception $e)
		{
			echo "<br/>\n" . $e->getMessage() . "<br/>\n";
		}

		echo "Transfering blobs into the container: " . $containerName . "<br/>\n";

		$blobs = $filesStorageClient->getBlobList($containerName);

		foreach($blobs as $bkey => $blobName)
		{
			$file = $container . "/" . $blobName;
			echo "Uploading file " . $blobName . " ... <br/>\n";

			try
			{
				$blobsStorageClient->uploadBlob($containerName, $blobName, $file);
			}
			catch(Microsoft_WindowsAzure_Exception $e)
			{
				echo "<br/>\n" . $e->getMessage() . "<br/>\n";
			}
		}
	}
}

function cleanAzure()
{

	$blobsStorageClient = new AzureStorage();

	$containers = $blobsStorageClient->getContainersList();

	echo "***************************************************<br/>\n";
	echo "* File storage to Azure Storage Migration Utility *<br/>\n";
	echo "***************************************************<br/>\n";

	foreach($containers as $key => $containerName)
	{
		echo "Deleting container " . $containerName . "<br/>\n";
		try
		{
			$blobsStorageClient->deleteContainer($containerName);
		}
		catch(Microsoft_WindowsAzure_Exception $e)
		{
			echo "<br/>\n" . $e->getMessage() . "<br/>\n";
		}
	}
}

//cleanAzure();
migrateToAzure();




?>