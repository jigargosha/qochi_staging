<?php 
/*if($_SERVER['HTTPS']!="on")  
{     
	$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];     
	header("Location:$redirect");  
}*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" /> 
    <title>Image Storage Retrieval Manipulation Service :: Login</title>

    <meta name="apple-mobile-web-app-capable" content="no" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="viewport" content="width=device-width,initial-scale=0.69,user-scalable=yes,maximum-scale=1.00" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/forms.css" />
    <link rel="stylesheet" type="text/css" href="css/forms-btn.css" />
    <link rel="stylesheet" type="text/css" href="css/menu.css" />
    <link rel="stylesheet" type="text/css" href="css/style_text.css" />
    <link rel="stylesheet" type="text/css" href="css/datatables.css" />
    <link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
    <link rel="stylesheet" type="text/css" href="css/pirebox.css" />
    <link rel="stylesheet" type="text/css" href="css/modalwindow.css" />
    <link rel="stylesheet" type="text/css" href="css/statics.css" />
    <link rel="stylesheet" type="text/css" href="css/tabs-toggle.css" />
    <link rel="stylesheet" type="text/css" href="css/system-message.css" />
    <link rel="stylesheet" type="text/css" href="css/tooltip.css" />
    <link rel="stylesheet" type="text/css" href="css/wizard.css" />
    <link rel="stylesheet" type="text/css" href="css/wysiwyg.css" />
    <link rel="stylesheet" type="text/css" href="css/wysiwyg.modal.css" />
    <link rel="stylesheet" type="text/css" href="css/wysiwyg-editor.css" />
    <link rel="stylesheet" type="text/css" href="css/handheld.css" />
<!--    <style type="text/css">
	    @import url("css/style.css");
	    @import url("css/forms.css");
	    @import url("css/forms-btn.css");
	    @import url("css/menu.css");
	    @import url('css/style_text.css');
	    @import url("css/datatables.css");
	    @import url("css/fullcalendar.css");
	    @import url("css/pirebox.css");
	    @import url("css/modalwindow.css");
	    @import url("css/statics.css");
	    @import url("css/tabs-toggle.css");
	    @import url("css/system-message.css");
	    @import url("css/tooltip.css");
	    @import url("css/wizard.css");
	    @import url("css/wysiwyg.css");
	    @import url("css/wysiwyg.modal.css");
	    @import url("css/wysiwyg-editor.css");
	    @import url("css/handheld.css");
    </style>-->
    
    <!--[if lte IE 8]>
	    <script type="text/javascript" src="js/excanvas.min.js"></script>
    <![endif]-->
    
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.backgroundPosition.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.1.8.17.js"></script>
    <script type="text/javascript" src="js/jquery.ui.select.js"></script>
    <script type="text/javascript" src="js/jquery.ui.spinner.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/supersubs.js"></script>
    <script type="text/javascript" src="js/jquery.datatables.js"></script>
    <script type="text/javascript" src="js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="js/jquery.smartwizard-2.0.min.js"></script>
    <script type="text/javascript" src="js/pirobox.extended.min.js"></script>
    <script type="text/javascript" src="js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="js/jquery.elastic.source.js"></script>
    <script type="text/javascript" src="js/jquery.jBreadCrumb.1.1.js"></script>
    <script type="text/javascript" src="js/jquery.customInput.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery.metadata.js"></script>
    <script type="text/javascript" src="js/jquery.filestyle.mini.js"></script>
    <script type="text/javascript" src="js/jquery.filter.input.js"></script>
    <script type="text/javascript" src="js/jquery.flot.js"></script>
    <script type="text/javascript" src="js/jquery.flot.pie.min.js"></script>
    <script type="text/javascript" src="js/jquery.flot.resize.min.js"></script>
    <script type="text/javascript" src="js/jquery.graphtable-0.2.js"></script>
    <script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
    <script type="text/javascript" src="js/wysiwyg.image.js"></script>
    <script type="text/javascript" src="js/wysiwyg.link.js"></script>
    <script type="text/javascript" src="js/wysiwyg.table.js"></script>
    <script type="text/javascript" src="js/wysiwyg.rmFormat.js"></script>
    <script type="text/javascript" src="js/costum.js"></script>
    
    <script language="javascript" src="js/jquery.cookie.js"></script>
    <script language="javascript" src="js/utils.js"></script>
    <script language="javascript">

	/***********************************************************************************
	*	Login User: Checks every possible scenario and handles sessions properly
        *       Although redirecting on the client side to the dashboard but unless the user 
	*       is properly logged in, the user will be redirected back to login page.
	************************************************************************************/

	$(function(){
	    $("#submit").click(function()
	    {

		$("#username").val($.trim($("#username").val()));
		$("#password").val($.trim($("#password").val()));

		$("#message").html("");
		$("#message").hide();

		if($("#username").val() == "")
		{
		    $("#message").html("<span><b>Error</b>: Please enter your user name</span>");
		    $("#message").show();
		    return;
		}

		if($("#password").val() == "")
		{
		    $("#message").html("<span><b>Error</b>: Please enter your password</span>");
		    $("#message").show();
		    return;
		}

		$.cookie("username", $.trim($("#username").val()));
		$.cookie("password", $.trim($("#password").val()));

		$('body').loading();

		$.ajax({
		    type: "GET",
		    url: $("#loginForm").attr("action"),
		    data: $("#loginForm").serialize(), 
		    success: function(result)
		    {
			$('body').loadingHide();
			result = JSON.parse(result);
			if(!result.status)
			{
			    $("#message").html("<span><b>Error</b>: " + result.ErrorMessage + "</span>");
			    $("#message").show();
			    $.removeCookie("username");
			    $.removeCookie("password");
			}
			else
			    $(window).attr("location", "dashboard.php");
		    },
		});
	    });
	});
    </script>
</head>

<body>

<div id="wrapper" class="login">
    <div class="box">
	<div class="title">
	    Please login
	    <span class="hide"></span>
	</div>
	<div class="content">
	    <div class="message inner blue" id="message" name="message" style="display:none">
	    </div>
	    <form action="server.php" id="loginForm" name="loginForm">
		<input type="hidden" id="api" name="api" value="loginUser" />
		<div class="row">
		    <label>Username</label>
		    <div class="right"><input type="text" value="" id="username" name="username" /></div>
		</div>
		<div class="row">
		    <label>Password</label>
		    <div class="right"><input type="password" value="" id="password" name="password" /></div>
		</div>
		<div class="row">
		    <div class="right">
			<button type="button" id="submit" name="submit"><span>Submit</span></button>
		    </div>
		</div>
	    </form>
	</div>
    </div>
</div>

</body>

</html> 