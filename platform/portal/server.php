<?php
session_start();

/*if($_SERVER['HTTPS']!="on")  
{     
	$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];     
	header("Location:$redirect");  
}*/

include_once("model/Model.php");

/*********************************************************************************************
*
*  server.php
*  BI Layer: Business Logic Layer for the administration
*
*********************************************************************************************/

class AdminServer
{

    const DEFAULT_ROUNDS = 50000;

    /**************************************************************************
    *	To store the POST/GET variables 
    ***************************************************************************/

    private $arguments;
    private $model;

    /**************************************************************************
    *	AdminServer Construction
    ***************************************************************************/

    function __construct()
    {
	$this->setArguments();
	$this->model = new Model();
    }

    /**************************************************************************
    *	Core Server APIs
    ***************************************************************************/

    public function registerUser()
    {
	$username = trim($this->arguments['username']);
	$password = trim($this->arguments['password']);
	$fullname = trim($this->arguments['fullname']);
	$roleid = trim($this->arguments['roleid']);
	$userid = trim($this->arguments['userid']);

	if(!isset($_SESSION["loggeduser"]))
	    return $this->createResponse(false, "User not logged in or session is expired");

	if($username === "")
	    return $this->createResponse(false, "Please provide a user name");

	if($password === "")
	    return $this->createResponse(false, "Please provide your password");

	$role = $this->model->getRoleById($roleid);

	if(!$role)
	{
	    return $this->createResponse(false, "Role does not exist");
	}

	$admintype = trim($role[3]);
	$loggedAdminType = intval($this->getLoggedUserProperty("admintype"));

	if($admintype !== "0" && $admintype !== "1" && $admintype !== "2")
	    return $this->createResponse(false, "Some invalid value is provided");

	$admintype = intval($admintype);

	if($loggedAdminType <= 1 && $admintype >= 1)
	    return $this->createResponse(false, "You do not have rights to create a new role with full control");

	if($userid !== "")
	{
	    $hash_code = base64_encode($this->hash($password));
	    return $this->model->updateUser($userid, $username, $password, $fullname, $roleid, $hash_code);
	}
	else
	{
	    $password = base64_encode($this->hash($password));
	    return $this->model->createUser($username, $password, $fullname, $roleid);
	}
    }
    public function removeUser()
    {
	$ids = trim($this->arguments['ids']);
	$this->model->removeUsers($ids);
	return $this->createResponse(true, "");
    }
    public function loginUser()
    {
	$username = trim($this->arguments['username']);
	$password = trim($this->arguments['password']);

	$user = $this->model->getUserByName($username);

	if(!$user)
	    return $this->createResponse(false, "User does not exist!");

	$stored_hash = base64_decode(trim($user[2]));

	if(!$this->check($password, $stored_hash))
	    return $this->createResponse(false, "Provided password is wrong");
				
	$_SESSION["loggeduser"] = array("id"=>$user[0], "username"=>$user[1], "password" => $user[2], "fullname"=>$user[3], "roleid"=>$user[4], "rolename"=>$user[5], "apis"=>$user[6], "admintype"=>$user[7]);

	return $this->createResponse(true, "");
    }
    public function logoutUser()
    {
	session_destroy();
    }
    public function getLoggedUserInfo()
    {
	if(isset($_SESSION["loggeduser"]))
	    return $this->createResponse(true, $_SESSION["loggeduser"]);
	else
	    return $this->createResponse(false, "You are not logged in, please login!");
    }
    public function saveUserSettings()
    {
	$userid = trim($this->arguments['userid']);
	$username = trim($this->arguments['username']);
	$password = trim($this->arguments['password']);
	$fullname = trim($this->arguments['fullname']);
	$roleid   = trim($this->getLoggedUserProperty("roleid"));

	if(!isset($_SESSION["loggeduser"]))
	    return $this->createResponse(false, "User not logged in or session is expired");

	if($username === "")
	    return $this->createResponse(false, "Please provide a user name");

	if($password === "")
	    return $this->createResponse(false, "Please provide your password");

	$hash_code = base64_encode($this->hash($password));
	return $this->model->updateUser($userid, $username, $password, $fullname, $roleid, $hash_code);
    }
    public function createRole()
    {
	$roleid= trim($this->arguments['roleid']);
	$rolename= trim($this->arguments['rolename']);
	$authorizedapis= trim($this->arguments['authorizedapis']);
	$admintype= trim($this->arguments['admintype']);

	if($admintype !== "0" && $admintype !== "1" && $admintype !== "2")
	    return $this->createResponse(false, "Some invalid value is provided");

	$loggedAdminType = intval($this->getLoggedUserProperty("admintype"));
	$admintype = intval($admintype);

	if($loggedAdminType <= 1 && $admintype >= 1)
	    return $this->createResponse(false, "You do not have rights to create a new role with full control");

	if($roleid !== "")
	{
	    return $this->model->updateRole($roleid, $rolename, $authorizedapis, $admintype);
	}
	else
	{
	    return $this->model->createRole($roleid, $rolename, $authorizedapis, $admintype);
	}
    }
    public function removeRole()
    {
	$ids = trim($this->arguments['ids']);
	$this->model->removeRoles($ids);
	return $this->createResponse(true, "");
    }

    /**************************************************************************
    *	Utility Functions
    ***************************************************************************/

    public function getPackOptions()      
    {
	$userid = trim($this->getLoggedUserProperty("id"));
	$admintype = intval($this->getLoggedUserProperty("admintype"));

	$packs = $this->model->getPacks($userid, $admintype);
      
	$packoptions = "";

	foreach($packs as $key=>$pack)
	{
	    $packoptions .= "<option value='".$pack[0]."'>".trim($pack[0]). " :: " .trim($pack[1])."</option>";
	}
	return $packoptions;
    }
	
	public function getArtistOptions()      
    {
	
	$artists = $this->model->getArtists();
	$artistoptions = "";
	foreach($artists as $key=>$artist)
	{
	    $artistoptions .= "<option value='".$artist[0]."'>".trim($artist[1])."</option>";
	}
	return $artistoptions;
    }
	
    public function createResponse($status, $value)
    {
	return json_encode(array("status" => $status, (($status)?"Data":"ErrorMessage")=>$value));
    }
    public function checkAdminType($type)
    {
	return (intval($this->getLoggedUserProperty("admintype")) == $type);
    }
    public function checkUserLoggedin()
    {
	if(!isset($_SESSION["loggeduser"]))
	    Header("Location: login.php");
    }
    public function getLoggedUserProperty($name)
    {
	return (isset($_SESSION["loggeduser"][$name]))?$_SESSION["loggeduser"][$name]:"";
    }
    public function getUserRows()
    {
	$admintype = intval($this->getLoggedUserProperty("admintype"));
	$users = $this->model->getUsers($admintype);
	$roles = $this->model->getRoles($admintype);

	$header = '<thead> 
		      <tr>
			  <th></th>
			  <th>Id</th>
			  <th>Username</th>
			  <th>FullName</th>
			  <th>Role</th>
			  <th></th>
		      </tr>
		  </thead>';

	$rows =	  '<tbody>';

	foreach($users as $k => $user)
	{
	    $roleoption = "";
	    foreach($roles as $key => $role)
	    {
		if(intval($role[0]) == intval($user["rid"]))
		{
		    $roleoption = trim($role[1]);
		}
	    }
	    $rows .=  '<tr>
			  <td><input type="checkbox" class="selectedusers" value="'.$user["uid"].'"/></td>
			  <td>'.$user["uid"].'</td>
			  <td>'.$user["username"].'</td>
			  <td>'.$user["fullname"].'</td>
			  <td>'.$roleoption.'</td>
			  <td><a href="#" onclick=\'window.btnEditUser("'.$user["uid"].'", "'.$user["username"].'", "'.$user["password"].'", "'.$user["fullname"].'", "'.$user["rid"].'")\'><img src="img/edituser.png" width="16px" /></a></td>
		      </tr>';
	}
	$rows .= '</tbody>';

	return $header . $rows;
    }
	
	public function getApiStats(){
	$api_stats = $this->model->getappstats();
	$header = '<thead> 
		      <tr>
			  <th width="150px;">API</th>
			  <th width="50px;">Avg/Hr</th>
			  <th width="52px;">Max/Hr</th>
			  <th>Max/Hr Day</th>
			  <th width="58px;">Avg/Day</th>
			  <th width="60px;">Max/Day</th>
			  <th>Max Day</th>
		      </tr>
		  	</thead>';
	$rows =	  '<tbody>';
	if(!empty($api_stats)){
		foreach($api_stats as $k => $stats)
		{
			$rows .=  '<tr>
				  <td style="max-width: 10px;word-wrap: break-word;">'.$stats['api_name'].'</td>
				  <td>'.$stats["average_within_hour"].'</td>
				  <td>'.$stats["maximum_within_hour"].'</td>
				  <td>'.$stats["maximum_date_hour"].'</td>
				  <td>'.$stats["average_within_day"].'</td>
				  <td>'.$stats["maximum_within_day"].'</td>
				  <td>'.$stats["maximum_month_day"].'</td>
				  </tr>';
		}
	}
	$rows .= '</tbody>';

	return $header . $rows;
    }
	
    public function getRoleOptions()
    {
	$admintype = intval($this->getLoggedUserProperty("admintype"));
	$roles = $this->model->getRoles($admintype);

	$roleoptions = "";
	foreach($roles as $key => $role)
	{
	    $roleoptions .= "<option value='".$role[0]."'>".$role[1]."</option>";
	}
	return $roleoptions;
    }
    public function getRoleRows()
    {
	$loggedadmintype = intval($this->getLoggedUserProperty("admintype"));
	$roles = $this->model->getRoles($loggedadmintype);

	$header = '<thead> 
		      <tr>
			  <th></th>
			  <th>Name</th>
			  <th>Authorized APIs</th>
			  <th>Control</th>
			  <th></th>
		      </tr>
		  </thead>';

	$rows =	  '<tbody>';

	foreach($roles as $key => $role)
	{
	    $roleid = trim($role[0]);
	    $admintype = trim($role[3]);
	    $authorizedapis = trim($role[2]);    
	    $authorizedapisoptions = $this->getOptionsFromCvs($authorizedapis);
	    $rolename = trim($role[1]);
	    
	    $rows .=  '<tr>
			  <td><input type="checkbox" class="selectedroles" value="'.$roleid.'" /></td>
			  <td>'.$rolename.'</td>
			  <td><select style="width:100%">' . $authorizedapisoptions . '</select></td>
			  <td>'.(($admintype>0)?"Full":"Limited").'</td>
			  <td><a href="#" onclick=\'window.btnEditRole("'.$roleid.'","'.$rolename.'","'.$authorizedapis.'",'.$admintype.')\'><img src="img/editrole.png" width="16px" /></a></td>
		      </tr>';
	}
	$rows .= '</tbody>';

	return $header . $rows;
    }
    public function getDownloadDetailRows()
    {
	$admintype = intval($this->getLoggedUserProperty("admintype"));

	$header = '<thead> 
		      <tr>
			  <th>Access UserID</th>
			  <th>Image</th>
			  <th>Timestamp</th>
		      </tr>
		  </thead>';

	$rows =	  '<tbody>';

	$rows .=  '<tr>
		      <td>'."Salman Naseer".'</td>
		      <td>'."Image3522g234".'</td>
		      <td>'."2013/11/01 12:01:42 GMT".'</td>
		  </tr>';

	$rows .= '</tbody>';

	return $header . $rows;
    }
    public function getOptionsFromCvs($cvs)
    {
	$cvs = explode(",", $cvs);
	$options = "";
	foreach($cvs as $key => $value)
	{
	    $options .= "<option value='".$value."'>".$value."</option>";
	}
	return $options;
    }
    public function processRequest()
    {
	if(!$this->arguments)
	    return;

	if(!isset($this->arguments["api"]))
	{
	    echo $this->createResponse(false, "Bad request!");
	    return;
	}

	$api = $this->arguments["api"];

	if($api !== "loginUser" && !isset($_SESSION["loggeduser"]))
	    header("Location: login.php");

	$response = call_user_func(array($this, $api));

	echo $response;
    }
    public function setArguments() 
    {
	$method = $_SERVER['REQUEST_METHOD'];
	switch ($method) 
	{
	    case 'GET':
	    case 'HEAD':
	      $this->arguments = $_GET;
	      break;
	    case 'POST':
	      $this->arguments = $_POST;
	      break;
	    case 'PUT':
	    case 'DELETE':
	      parse_str(file_get_contents('php://input'), $this->arguments);
	      break;
	    default:
	      $this->arguments = NULL;
	}
    }
    public function hash($password) 
    {
    
	if (version_compare(PHP_VERSION, '5.3.2') < 0)
        throw new Exception('SHA requires PHP 5.3.2 or above');

	$salt = '';
	for ($j = 0; $j < 16; $j++) {
	    $salt .= substr("./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", mt_rand(0, 63), 1);
	}

	$salt = '$6$rounds=' . self::DEFAULT_ROUNDS . '$' . $salt . '$';

        return crypt($password, $salt);
    }
    public function check($password, $stored_hash) 
    {
        if (version_compare(PHP_VERSION, '5.3.2') < 0)
            throw new Exception('SHA requires PHP 5.3.2 or above');
        

        return crypt($password, $stored_hash) == $stored_hash;
    }
}
$server = new AdminServer();
$server->processRequest();
?>