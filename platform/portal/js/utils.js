$(function(){

    $.fn.loading = function() 
    {
	var loadingElement = document.createElement('div');
	loadingElement.id = 'loading';
	loadingElement.className = 'loading';
	loadingElement.innerHTML = '<img src="img/processing.gif" style="border: 3px solid #000000" />';

	// apply styles
	loadingElement.style.position = 'fixed';
	loadingElement.style.width = '100%';
	loadingElement.style.textAlign = 'center';
	loadingElement.style.zIndex = '10000';
	loadingElement.style.display = 'block';

	// attach it to DOM
	$(this).append(loadingElement);

	// position element
	$("#loading").position({
	    my: "center middle",
	    at: "center middle",
	    of: window
	});
    }
    
    $.fn.loadingHide = function()
    {
	$("#loading").remove();
    }
});