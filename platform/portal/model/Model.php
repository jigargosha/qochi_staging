<?php

include_once __DIR__ . "/../../config/config.php";
include_once __DIR__ . "/../../wrappers/Database.php";

/*********************************************************************************************
*
*  Model.php
*  Database Layer: It is used for every database interaction with respect to roles and users
*
*********************************************************************************************/

class Model
{
    /**************************************************************************
    *	Database reference (Singleton) 
    ***************************************************************************/

    private $db;

    /**************************************************************************
    *	Rounds for password encryption
    ***************************************************************************/

    const DEFAULT_ROUNDS = 50000;

    /**************************************************************************
    *	AdminServer construction
    ***************************************************************************/

    public function __construct() 
    {	
	$this->db = new database(DB_TYPE,DB_SERVER,DB_NAME,DB_USERNAME,DB_PASS,DB_PORT);
    }
    /**************************************************************************
    *	User administration in database
    ***************************************************************************/

    public function createUser($username, $password, $fullname, $roleid)
    {	
	if(!$this->getUserByName($username))
	{
	    $this->db->query("INSERT INTO Users(username,password,fullname,roleid) VALUES('$username','$password','$fullname','$roleid')");			
	    return $this->createResponse(true, "");
	}
	return $this->createResponse(false, "User already exists, please take another username");
    }
    public function getUserByName($username)
    {
	$this->db->query("SELECT u.id as uid,u.username,u.password,u.fullname, r.id as rid, r.name, r.apis, r.admintype from Users u, Role r WHERE u.roleid=r.id AND u.username='$username'");
	$user = $this->db->get_row();
	return $user;
    }
    public function getUserById($userid)
    {
	$this->db->query("SELECT u.id as uid,u.username,u.password,u.fullname, r.id as rid, r.name, r.apis, r.admintype from Users u, Role r WHERE u.roleid=r.id AND u.id=$userid");
	$user = $this->db->get_row();
	return $user;
    }
    public function getUsers($admintype)
    {
	$this->db->query("SELECT u.id as uid,u.username,u.password,u.fullname, r.id as rid, r.name, r.apis, r.admintype from Users u, Role r WHERE u.roleid=r.id AND r.admintype < $admintype");
	$dbusers = $this->db->fetchAll();
	
	$users = array();
	foreach($dbusers as $key => $dbuser)
	{
	    $users[] = array("uid"=>$dbuser[0], "username"=>$dbuser[1], "password" => $dbuser[2], "fullname"=>$dbuser[3], "rid"=>$dbuser[4], "rolename"=>$dbuser[5], "apis"=>$dbuser[6], "admintype"=>$dbuser[7]);
	}
	return $users;
    }
	public function getappstats()
    {
	$this->db->query("SELECT * FROM app_analytics");
	$data=$this->db->fetchAll();
	$app_stats = array();
	
	foreach($data as $key => $dbstat)
	{
		 if(isset($dbstat[6]) && $dbstat[6]!=""){
			 $date_hr=date('m/d/Y H:i:s', $dbstat[6]);
		 }
		 else{
		 	 $date_hr="";
		 }
		if(isset($dbstat[11]) && $dbstat[11]!=""){
			$month_day = date('m/d/Y H:i:s', $dbstat[11]);
		}
		else{
			$month_day = "";
		}
	    $app_stats[] = array("api_name"=>$dbstat[1], "average_within_hour"=>$dbstat[3], "maximum_within_hour" => $dbstat[4], "maximum_date_hour"=>$date_hr, "average_within_day"=>$dbstat[8], "maximum_within_day"=>$dbstat[9], "maximum_month_day"=>$month_day);
	}	
	return $app_stats;
    }
    public function updateUser($userid, $username, $password, $fullname, $roleid, $hash_code)
    {	
	$userbyid = $this->getUserById($userid);
	$userbyname = $this->getUserByName($username);

	if($userbyid)
	{
	    $password = ($password === trim($userbyid[2]))?$password:$hash_code;

	    // This is to check if it is the same user (that is being updated) whose username is used or not.
	    if(!$userbyname || ($userbyname[0] == $userbyid[0]))
	    {

		$this->db->query("UPDATE Users SET username='$username', password='$password', fullname='$fullname', roleid='$roleid' WHERE id='$userid'");			
		return $this->createResponse(true, "");
	    }
	    else
		return $this->createResponse(false, "User with name '$username' already exists, please take another username");
	}
	return $this->createResponse(false, "User does not exist");
    }
    public function removeUsers($ids)
    {
	$this->db->query("DELETE FROM Users WHERE id IN ($ids)");   
    }

    /**************************************************************************
    *	Role administration in database
    ***************************************************************************/

    public function createRole($roleid, $rolename, $authorizedapis, $admintype)
    {	
	if(!$this->getRoleByName($rolename))
	{
	    $this->db->query("INSERT INTO Role(name, apis, admintype) VALUES('$rolename','$authorizedapis',$admintype)");			
	    return $this->createResponse(true, "");
	}
	return $this->createResponse(false, "Role already exists, please take another role name");
    }
    public function updateRole($roleid, $rolename, $authorizedapis, $admintype)
    {	
	$rolebyid = $this->getRoleById(trim($roleid));
	$rolebyname = $this->getRoleByName(trim($rolename));

	if($rolebyid)
	{
	    // This is to check if it is the same role (that is being updated) whose rolename is used or not.
	    if(!$rolebyname || ($rolebyname[0] == $rolebyid[0]))
	    {
		$this->db->query("UPDATE Role SET name='$rolename', apis='$authorizedapis', admintype=$admintype WHERE id=$roleid");			
		return $this->createResponse(true, "");
	    }
	    else
		return $this->createResponse(false, "Role with name '$rolename' already exists, please take another role name");
	}
	return $this->createResponse(false, "Role does not exist");
    }
    public function removeRoles($ids)
    {
	$this->db->query("DELETE FROM Role WHERE id IN ($ids)");
    }
    public function getRoles($admintype)
    {
	$this->db->query("SELECT id, name, apis, admintype from Role where admintype < $admintype");
	return $this->db->fetchAll();
    }
    public function getRoleByName($rolename)
    {
	$this->db->query("SELECT id, name, apis, admintype from Role where name='$rolename'");
	$role = $this->db->get_row();
	return $role;
    }
    public function getRoleById($roleid)
    {
	$this->db->query("SELECT id, name, apis, admintype from Role where id=$roleid");
	$role = $this->db->get_row();
	return $role;
    }
    public function createResponse($status, $value)
    {
	return json_encode(array("status" => $status, (($status)?"Data":"ErrorMessage")=>$value));
    } 

    /**************************************************************************
    *	Related to Image Pack
    ***************************************************************************/

    public function getPacks($userid, $admintype)
    {
	$this->db->query("SELECT * FROM PackMetaData" . (($admintype==0)?" WHERE userid='$userid'":""));
	return $this->db->fetchAll();
    } 
	
	public function getArtists()
	{
		$this->db->query("SELECT artistid,title FROM ArtistInfo WHERE 1=1");
		return $this->db->fetchAll();
	}
	
	public function createUpdateApiStats($api_name)
	{
		$this->db->query("SELECT id FROM app_analytics WHERE api='$api_name'");
		$data=$this->db->fetchAll();
	}
	
} 