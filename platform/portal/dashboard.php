<?php

/*if($_SERVER['HTTPS']!="on")  
{     
	$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];     
	header("Location:$redirect");  
}
*/
include_once "server.php";
$server->checkUserLoggedin();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Image Storage Retrieval Manipulation Service :: Dashboard</title>

<meta name="apple-mobile-web-app-capable" content="no" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="viewport"
	content="width=device-width,initial-scale=0.69,user-scalable=yes,maximum-scale=1.00" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/forms.css" />
<link rel="stylesheet" type="text/css" href="css/forms-btn.css" />
<link rel="stylesheet" type="text/css" href="css/menu.css" />
<link rel="stylesheet" type="text/css" href="css/style_text.css" />
<link rel="stylesheet" type="text/css" href="css/datatables.css" />
<link rel="stylesheet" type="text/css" href="css/fullcalendar.css" />
<link rel="stylesheet" type="text/css" href="css/pirebox.css" />
<link rel="stylesheet" type="text/css" href="css/modalwindow.css" />
<link rel="stylesheet" type="text/css" href="css/statics.css" />
<link rel="stylesheet" type="text/css" href="css/tabs-toggle.css" />
<link rel="stylesheet" type="text/css" href="css/system-message.css" />
<link rel="stylesheet" type="text/css" href="css/tooltip.css" />
<link rel="stylesheet" type="text/css" href="css/wizard.css" />
<link rel="stylesheet" type="text/css" href="css/wysiwyg.css" />
<link rel="stylesheet" type="text/css" href="css/wysiwyg.modal.css" />
<link rel="stylesheet" type="text/css" href="css/wysiwyg-editor.css" />
<link rel="stylesheet" type="text/css" href="css/handheld.css" />
<!--<style type="text/css">
@import
	url("css/style.css")
	;

@import
	url("css/forms.css")
	;

@import
	url("css/forms-btn.css")
	;

@import
	url("css/menu.css");

@import
	url("css/style_text.css")
	;

@import
	url("css/datatables.css")
	;

@import
	url("css/fullcalendar.css")
	;

@import
	url("css/pirebox.css")
	;

@import
	url("css/modalwindow.css")
	;

@import
	url("css/statics.css")
	;

@import
	url("css/tabs-toggle.css")
	;

@import
	url("css/system-message.css")
	;

@import
	url("css/tooltip.css")
	;

@import
	url("css/wizard.css")
	;

@import
	url("css/wysiwyg.css")
	;

@import
	url("css/wysiwyg.modal.css")
	;

@import
	url("css/wysiwyg-editor.css")
	;

@import
	url("css/handheld.css")
	;
</style>-->

<!--[if lte IE 8]>
	    <script type="text/javascript" src="js/excanvas.min.js"></script>
    <![endif]-->
<link rel="stylesheet" media="screen" type="text/css"
	href="js/colorpicker/css/colorpicker.css" />
<script type="text/javascript" src="js/colorpicker/js/jquery.js"></script>
<script type="text/javascript" src="js/colorpicker/js/colorpicker.js"></script>
<script type="text/javascript" src="js/colorpicker/js/eye.js"></script>
<script type="text/javascript" src="js/colorpicker/js/utils.js"></script>
<script type="text/javascript"
	src="js/colorpicker/js/layout.js?ver=1.0.2"></script>

<script type="text/javascript"
	src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript"
	src="js/jquery.backgroundPosition.js"></script>
<script type="text/javascript"
	src="js/jquery.placeholder.min.js"></script>
<script type="text/javascript"
	src="js/jquery.ui.1.8.17.js"></script>
<!--<script type="text/javascript" src="js/jquery.ui.select.js"></script>-->
<script type="text/javascript"
	src="js/jquery.ui.spinner.js"></script>
<script type="text/javascript"
	src="js/superfish.js"></script>
<script type="text/javascript"
	src="js/supersubs.js"></script>
<script type="text/javascript"
	src="js/jquery.datatables.js"></script>
<script type="text/javascript"
	src="js/fullcalendar.min.js"></script>
<script type="text/javascript"
	src="js/jquery.smartwizard-2.0.min.js"></script>
<script type="text/javascript"
	src="js/pirobox.extended.min.js"></script>
<script type="text/javascript"
	src="js/jquery.tipsy.js"></script>
<script type="text/javascript"
	src="js/jquery.elastic.source.js"></script>
<script type="text/javascript"
	src="js/jquery.jBreadCrumb.1.1.js"></script>
<script type="text/javascript"
	src="js/jquery.customInput.js"></script>
<script type="text/javascript"
	src="js/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="js/jquery.metadata.js"></script>
<script type="text/javascript"
	src="js/jquery.filestyle.mini.js"></script>
<script type="text/javascript"
	src="js/jquery.filter.input.js"></script>
<script type="text/javascript"
	src="js/jquery.flot.js"></script>
<script type="text/javascript"
	src="js/jquery.flot.pie.min.js"></script>
<script type="text/javascript"
	src="js/jquery.flot.resize.min.js"></script>
<script type="text/javascript"
	src="js/jquery.graphtable-0.2.js"></script>
<script type="text/javascript"
	src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript"
	src="js/wysiwyg.image.js"></script>
<script type="text/javascript"
	src="js/wysiwyg.link.js"></script>
<script type="text/javascript"
	src="js/wysiwyg.table.js"></script>
<script type="text/javascript"
	src="js/wysiwyg.rmFormat.js"></script>
<script type="text/javascript"
	src="js/costum.js"></script>
<script type="text/javascript" src="js/colorpicker/js/colorpicker.js"></script>
<script language="javascript" src="js/jquery.cookie.js"></script>
<script language="javascript" src="js/utils.js"></script>
<script language="javascript">
    $(function(){

	window.ServiceURL =  "https://platform.qochi.com/platform/service/ISRMService.php";
	
    window.packtype = "";
	window.packop   = "";

	$.fn.hideSections = function()
	{
	    $("#sectionUser").hide();
	    $("#sectionImages").hide();
	    $("#sectionRoles").hide();
	    $("#sectionDownloads").hide();
	    $("#sectionApis").hide();
	}

	$("#lnkSectionUser").click(function()
	{
	    $('body').hideSections();
	    $("#sectionUser").show();
	    $(this).parent().siblings().removeClass("current");
	    $(this).parent().addClass("current");
	});
	$("#lnkSectionImages").click(function()
	{
	    $('body').hideSections();
	    $("#sectionImages").show();
	    $(this).parent().siblings().removeClass("current");
	    $(this).parent().addClass("current");
	});
	$("#lnkSectionRoles").click(function()
	{
	    $('body').hideSections();
	    $("#sectionRoles").show();
	    $(this).parent().siblings().removeClass("current");
	    $(this).parent().addClass("current");
	});
	$("#lnkSectionDownloads").click(function()
	{
	    $('body').hideSections();
	    $("#sectionDownloads").show();
	    $(this).parent().siblings().removeClass("current");
	    $(this).parent().addClass("current");
	});
	$("#lnkSectionApis").click(function()
	{
	    $('body').hideSections();
	    $("#sectionApis").show();
	    $(this).parent().siblings().removeClass("current");
	    $(this).parent().addClass("current");
	});

       /**************************************************************************
	*	Related to Images and Image Packs
	***************************************************************************/

	window.postUpload = function(form, successhandler)
	{
	    $.ajax({
		url: window.ServiceURL, 
		type: "POST",
		success: successhandler,
		error: function(msg)
		{
		    $('body').loadingHide();
		},
		data: new FormData(form),
		cache: false,
		contentType: false,
		processData: false
	    });
	}

	window.renderImageUploadForm = function()
	{
	    var form = '' + 
		'<form class="formImage">'+
		'<input type="hidden" name="api" value="addImageInPack" />'+
		'<input type="hidden" class="imageusername" name="username" value="" />'+
		'<input type="hidden" class="imagepassword" name="password" value="" />'+
		'<table width="100%" style="border:2px solid #ffffff; background-color: #FFFF99">'+
		'<tbody>'+
		'	<tr>'+
		'	<td><input type="hidden" class="packid" name="packid" value=""/></td>'+
		'	<td style="text-align:right;"><a href="#" onclick="if(confirm(\'Are you sure you want to remove image ?\'))$(this).closest( \'form\' ).remove();" title="Remove Image"><img src="img/cross.png" width="10px"/></a></td>'+
		'	</tr>'+
		'	<tr>'+
		'	<td><label>Title (Optional)</label></td>'+
		'	<td><input type="text" class="imagetitle" name="title" style="width:100%" /></td>'+
		'	</tr>'+
		'	<tr>'+
		'	<td><label style="width:100%">Tags</label></td>	'+
		'	<td><input type="text" class="imagetags" name="tags" style="width:100%" /></td>'+
		'	</tr>'+
		'	<tr>'+
		'	<td><label style="width:100%">Description (Optional)</label></td>'+
		'	<td><textarea class="imagedescription" name="description" style="width:100%"></textarea></td>'+
		'	</tr>'+
		'	<tr>'+
		'	<td><label>Image:</label></td>'+
		'	<td>'+
		'		<input type="file" name="file" class="image"/>'+
		'	</td>'+
		'	</tr>'+
		'</tbody>'+
		'</table>'+
		'</form>';
	    return form;
	}

	$("#imagesList").html(window.renderImageUploadForm());

	window.val = function(cls, value)
	{
	    $("."+cls).each(function(){$(this).val(value)});
	}
	$("#imageListUpload").click(function(){

	    window.val("imageusername", $.cookie("username"));
	    window.val("imagepassword", $.cookie("password"));
	    var succeed = true;
	    $(".image").each(function()
	    {
	        if($.trim($(this).val()) == "")
	        {
		    alert("Please select image file(s) ");
		    $(this).focus();
		    succeed = false;
		    return false;
	        }
	    });

	    var packid = $("#imagepackid").val();

	    $(".packid").each(function(){$(this).val(packid)});

	    if(!succeed)
		return false;

	    $('body').loading();
	    var count = 0;
	    $(".formImage").each(function(){
	        count++;
		window.postUpload(this, function(result)
		{
			$('body').loadingHide();
			result = JSON.parse(result);
			
			if(!result.status)
			{
			    alert(result.ErrorMessage);
			}
			else
			{
			    count--;
			    if(count<=0)
			    {
			        window.val("imagetitle", "");
			        window.val("imagetags", "");
			        window.val("imagedescription", "");
					jQuery('input[type=file]').wrap('<form></form>').parent().trigger('reset').children().unwrap('<form></form>');
					alert("Image(s) uploaded in the pack");
			    }
			}
		});
	    });
	    return false;
	});
	
	$('#artistid').change(function() {
		 if($(this).val()=="") {
			$("#artisttitle").removeAttr("readonly"); 
    		$("#artistdescription").removeAttr("readonly"); 
			$("#artisttitle").focus();
		 }
		 else{
			$("#artisttitle").val("");
			$("#artistdescription").val("");
			$("#artisttitle").attr("readonly", "readonly"); 
    		$("#artistdescription").attr("readonly", "readonly");
		 }
	  });

	$("#formImagePack").submit(function(){

	    $("#packusername").val($.cookie("username"));
	    $("#packpassword").val($.cookie("password"));

		if($.trim($("#artisttitle").val()) == "" && $("#artistid").val() == "") {
	  	    alert("Please write artist title or select any artist title");
			return false;
		}
		if($.trim($("#thumbnail").val()) == ""){
			alert("Please select a thumbnail image file");
			return false;
	    }
		
		
		
	    $('body').loading();
	    window.postUpload(this, function(result)
	    {
		result = JSON.parse(result);
		$('body').loadingHide();

		if(!result.status)
		{
		    alert(result.ErrorMessage);
		}
		else
		{
		    //$("#packtitle").val("");
		    //$("#packtags").val("");
		    //$("#packdescription").val("");
            $('#formImagePack').each(function(){
				this.reset();
			});
			
			if(($('#artisttitle').attr('readonly') !== undefined) && ($('#artistdescription').attr('readonly') !== undefined)) {
				$("#artisttitle").removeAttr("readonly"); 
				$("#artistdescription").removeAttr("readonly");
		    }
		    var data = result.Data;
		    $("#imagepackid").append($('<option>', 
		    {
			value: data.packid,
			text: data.packid + " :: " + data.title
		    }));
		    alert("Image pack created");
		}
	    });

	    return false;
	});

	var searchQuery = "";
	
	window.showImage = function(url)
	{
	    window.open(url);
	}

	window.removeImage = function(packid, imageid)
	{
	    var username = $.trim($.cookie("username"));
	    var password = $.trim($.cookie("password"));

	    if(confirm("Are you sure you want to remove the image ?"))
	    {
		$('body').loading();
		$.ajax({
		    type: "POST",
		    url: window.ServiceURL,
		    data: {username: username, password: password, packid: packid, imageid: imageid, api: "removeImage"}, 
		    success: function(result)
		    {
			$('body').loadingHide();
			result = JSON.parse(result);
			if(!result.status)
			{
			    alert(result.ErrorMessage);
			}
			else
			{
			    window.getImages(packid)
			}
		    }
		});
	    }
	}

	window.showImages = function(data)
	{
	    var results = "";
	    var colors = ["#F5DFDF","#ffffffe"];
	    var c = 0;
	    var api = "";

	    switch(window.packop)
	    {
		case "searchImagePacks":
		case "retrieveAllImagePacks":
		    api = "retrieveImage";
		break;
		case "searchMyImagePacks":
		case "retrieveMyImagePacks":
		    api = "retrieveMyImage";
		break;
	    }
	    $.each(data, function(i, item)  
	    {
		var username 	= $.trim($.cookie("username"));
		var password 	= $.trim($.cookie("password"));
		var url 	= window.ServiceURL + /*encodeURIComponent*/("?api=" + api + "&username=" + username + "&password=" + password + "&packid=" + item.packid + "&imageid=" + item.imageid + "&unique_user_id="+username);
		var title 	= item.title;
		var description = item.description;
		var tags 	= item.tags;
		var color = colors[c];
		results += "<tr bgColor='"+color+"' id='" + item.imageid + "'><td width='110px'><a href='#' onclick='window.showImage(\"" + url + "\")'><img src='"+url+"' width='110px' height='110px' title='Show images' /></a></td><td valign='top' style='word-break:break-all;'><p align='right'><a href='#' onclick='window.removeImage(\"" + item.packid + "\",\"" + item.imageid + "\")' title='Remove image'><img src='img/cross.png' width='10px'/></a></p><p><strong>"+title+"</strong></p>"+description+"<p></p><p><strong>Tags:</strong> "+tags+"</p></td></tr>";
		c = (c+1)%2;
            });
	    results = (results == "")?"<p align='center'>No images found</p>":results;
	    $("#imagesresults").html(results);
	    $("#imagesdialog").dialog({width: 600,height: 500}).show();
	}

	window.getImages = function(packid)
	{
	    var username = $.trim($.cookie("username"));
	    var password = $.trim($.cookie("password"));
	    var api="";

	    switch(window.packop)
	    {
		case "searchImagePacks":
		    api = "searchImages";
		break;
		case "searchMyImagePacks":
		    api = "searchMyImages";
		break;
		case "retrieveAllImagePacks":
		    api = "retrieveImages";
		break;
		case "retrieveMyImagePacks":
		    api = "retrieveMyImages";
		break;
	    }

	    $.ajax(
	    {
		url: window.ServiceURL, 
		type: "GET",
		success: function(result)
		{
		    result = JSON.parse(result);
		    $('body').loadingHide();

		    if(!result.status)
		    {
			alert(result.ErrorMessage);
		    }
		    else
		    {
			var data = result.Data;
			window.showImages(data);
		    }
		},
		error: function()
		{
		    $('body').loadingHide();
		},
		data: {username: username, password: password, api: api, packid: packid, tags: searchQuery}
	    });
	}
	$("#attribColorSelector").ColorPicker({
		color: '#0000ff',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$("#attrib").css('backgroundColor', '#' + hex);
			$("#attribColorSelector").val('#' + hex);
		}
	});
	$("#dominantColorSelector").ColorPicker({
		color: '#0000ff',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$("#dominant").css('backgroundColor', '#' + hex);
			$("#dominantColorSelector").val('#' + hex);
		}
	});
	window.getPacks = function(tags, api, successhandler)
	{
	    var username = $.trim($.cookie("username"));
	    var password = $.trim($.cookie("password"));

	    $.ajax(
	    {
		url: window.ServiceURL, 
		type: "GET",
		success: successhandler,
		error: function()
		{
		    $('body').loadingHide();
		},
		data: {username: username, password: password, api: api, tags: tags}
	    });
	}
	
	window.removePack = function(packid)
	{
	    var username = $.trim($.cookie("username"));
	    var password = $.trim($.cookie("password"));

	    if(confirm("Are you sure you want to remove the image pack?"))
	    {
		$('body').loading();
		$.ajax(
		{
		    type: "POST",
		    url: window.ServiceURL,
		    data: {username: username, password: password, packid: packid, api: "removeImagePack"},
		    success: function(result)
		    {
			$('body').loadingHide();
			result = JSON.parse(result);
			if(!result.status)
			{
			    alert(result.ErrorMessage);
			}
			else
			{
			    $("#" + packid).remove();
			    $("#imagepackid option[value='" + packid + "']").remove();
			}
		    }
		});
	    }
	}

	window.createPackRepresentation = function(packid)
	{
	    var username = $.trim($.cookie("username"));
	    var password = $.trim($.cookie("password"));
	    $('body').loading();
	    $.ajax(
	    {
		type: "POST",
		url: window.ServiceURL,
		data: {username: username, password: password, packid: packid, api: "createPackRepresentation"},
		success: function(result)
		{
		    $('body').loadingHide();
		    result = JSON.parse(result);
		    if(!result.status)
		    {
			alert(result.ErrorMessage);
		    }
		    else
		    {
			var data = result.Data;
			var packRepresentationDialog = $("#packRepresentationDialog").dialog({width: 650,height: 650});
			packRepresentationDialog.show();

			var representation = data.representation;
			var url = window.ServiceURL + ("?username=" + username + "&password=" + password + "&packid=" + packid + "&imageid=" + representation + "&api=retrieveImage");
			$("#representation").attr("src", url);
		    }
		}
	    });
	}

	window.showPacks = function(data)
	{
	    var results = "";
	    var colors = ["#F5DFDF","#ffffffe"];
	    var c = 0;
	    var api = "";

	    switch(window.packop)
	    {
		case "searchImagePacks":
		case "retrieveAllImagePacks":
		    api = "retrieveImage";
		break;
		case "searchMyImagePacks":
		case "retrieveMyImagePacks":
		    api = "retrieveMyImage";
		break;
	    }
	    $.each(data, function(i, item) 
	    {
		var username 	= $.trim($.cookie("username"));
		var password 	= $.trim($.cookie("password"));
		var url 	= window.ServiceURL + /*encodeURIComponent*/("?api=" + api + "&username=" + username + "&password=" + password + "&packid=" + item.packid + "&imageid=" + item.thumbnail + "&unique_user_id="+username);
		var title 	= item.title;
		var description = item.description;
		var tags 	= item.tags;
		var color = colors[c];
		results += "<tr bgColor='"+color+"' id='" + item.packid + "'><td width='110px'><a href='#' onclick='window.getImages(\"" + item.packid + "\")'><img src='"+url+"' width='110px' height='110px' title='Show images' /></a></td><td valign='top' style='word-break:break-all;'><p align='right'><a href='#' onclick='window.createPackRepresentation(\"" + item.packid + "\")' title='Create Pack Representation'><img src='img/folder.png' width='10px'/></a>&nbsp;<a href='#' onclick='window.removePack(\"" + item.packid + "\")' title='Remove pack'><img src='img/cross.png' width='10px'/></a></p><p><strong>"+title+"</strong></p>"+description+"<p></p><p><strong>Tags:</strong> "+tags+"</p></td></tr>";
		c = (c+1)%2;
            });
	    results = (results == "")?"<p align='center'>No image packs found</p>":results;
	    $("#packsresults").html(results);
	}

	window.getPacksHandler = function(result)
	{
	    result = JSON.parse(result);
	    $('body').loadingHide();

	    if(!result.status)
	    {
		alert(result.ErrorMessage);
	    }
	    else
	    {
		var data = result.Data;
		window.showPacks(data);
	    }
	}

	$("#btnSearch").click(function()
	{

	    window.packtype = $.trim($("#packtype").val());
	    window.packop   = (window.packtype=="All Packs")?"searchImagePacks":"searchMyImagePacks";

	    $('body').loading();
	    var tags = $.trim($("#search").val());
	    searchQuery = tags;
	    window.getPacks(tags, window.packop, window.getPacksHandler);
	});

	$("#btnGetPacks").click(function()
	{
	    window.packtype = $.trim($("#packtype").val());
	    window.packop   = (window.packtype=="All Packs")?"retrieveAllImagePacks":"retrieveMyImagePacks";

	    $('body').loading();
	    searchQuery = "";
	    window.getPacks("", window.packop, window.getPacksHandler);
	});

       /**************************************************************************
	*	Related to User Settings: Rendered for every user
	***************************************************************************/

	$("#btnUserSettingsSave").click(function()
	{
	    var userid		= $.trim($("#settingsuserid").val());
	    var username 	= $.trim($("#settingsusername").val());
	    var password 	= $.trim($("#settingspassword").val());
	    var fullname 	= $.trim($("#settingsfullname").val());

	    if(username == "")
	    {
		alert("Please enter your user name");
		return;
	    }

	    if(password == "")
	    {
		alert("Please enter your password");
		return;
	    }

	    var pwd = password;
	    var tokens = pwd.split(" ");

	    if(tokens.length > 1)
	    {
		alert("Password should not contain empty spaces.");
		return;
	    }

	    tokens = username.split(" ");
	    if(tokens.length > 1)
	    {
		alert("Username should not contain empty spaces.");
		return;
	    }

	    $('body').loading();

	    $.ajax({
		type: "POST",
		url: "server.php",
		data: {userid: userid, username: username, password: password, fullname: fullname, api: "saveUserSettings"}, 
		success: function(result)
		{
		    $('body').loadingHide();
		    result = JSON.parse(result);
		    if(!result.status)
		    {
			alert(result.ErrorMessage);
		    }
		    else
		    {
			window.logoutUser();
		    }
		}
	    });
	});

	$("#btnUserSettingsCancel").click(function()
	{
	    var userSettingsDialog = $("#userSettingsDialog").dialog({width: 600,height: 230});
	    userSettingsDialog.dialog('close');
	});

	window.changeSettings = function()
	{
	    $('body').loading();
	    $.ajax(
	    {
		type: "POST",
		url: "server.php",
		data: {api: "getLoggedUserInfo"}, 
		success: function(result)
		{
		    $('body').loadingHide();
		    result = JSON.parse(result);
		    if(!result.status)
		    {
			alert(result.ErrorMessage);
		    }
		    else
		    {
			var data = result.Data;
			var userSettingsDialog = $("#userSettingsDialog").dialog({width: 600,height: 230});
			userSettingsDialog.show();

			$("#settingsuserid").val(data.id);
			$("#settingsusername").val(data.username);
			$("#settingspassword").val(data.password);
			$("#settingsfullname").val(data.fullname);
		    }
		}
	    });
	}

	window.logoutUser = function()
	{
	    $.ajax({
		type: "POST",
		url: "server.php",
		data: {api: "logoutUser"}, 
		success: function(result)
		{
		    $(window).attr("location", "login.php");
		}
	    });
	}

	<?php if($server->checkAdminType(ADMINMASTER) || $server->checkAdminType(SUPERADMIN)){?>

       /**************************************************************************
        *       Related to Users: Rendered only for SuperAdmin and AdminMaster
        ***************************************************************************/

        $("#btnAddUser").click(function()
        {
            var userCRUDDialog = $("#userCRUDDialog").dialog({width: 620,height: 300});
            $("#userid").val("");
            $("#username").val("");
            $("#password").val("");
            $("#fullname").val("");
            userCRUDDialog.show();
        });

        $("#btnDeleteUser").click(function()
        {
            
            var ids = "";
            $(".selectedusers:checked").each(function() 
            {
                ids += (ids == "")?($.trim(this.value)):("," + $.trim(this.value));
            });

            if(ids == "")
            {
                alert("Please select some users");
                return;
            }

            if(confirm("Are you sure you want to remove the selected users?"))
            {
                $('body').loading();
                $.ajax({
                    type: "POST",
                    url: "server.php",
                    data: {ids: ids, api: "removeUser"}, 
                    success: function(result)
                    {
                        $('body').loadingHide();
                        result = JSON.parse(result);
                        if(!result.status)
                        {
                            alert(result.ErrorMessage);
                            return;
                        }
                        window.updateUserRows();
                    }
                });
            }
        });

        window.btnEditUser = function(userid, username, password, fullname, roleid)
        {
            var userCRUDDialog = $("#userCRUDDialog").dialog({width: 620,height: 300});

            userCRUDDialog.show();

            $("#userid").val(userid);
            $("#username").val(username);
            $("#password").val(password);
            $("#fullname").val(fullname);
            $("#roles").val(roleid);
        }

        window.updateUserRows = function()
        {
  
            $('body').loading();
            $.ajax({
                type: "POST",
                url: "server.php",
                data: {api: "getUserRows"}, 
                success: function(result)
                {
                    $('body').loadingHide();
                    $("#tableUsers").html(result);
                }
            });
        }

        $("#btnUserSave").click(function()
        {
            var userid          = $.trim($("#userid").val());
            var username        = $.trim($("#username").val());
            var password        = $.trim($("#password").val());
            var fullname        = $.trim($("#fullname").val());
            var roleid          = $.trim($("#roles option:selected").val());
            var admintype       = $("#fullcontrol").is(":checked") ? 1 : 0;

            if(username == "")
            {
                alert("Please enter your user name");
                return;
            }

            if(password == "")
            {
                alert("Please enter your password");
                return;
            }

            var pwd = $.trim($("#password").val());
            var tokens = pwd.split(" ");

            if(tokens.length > 1)
            {
                alert("Password should not contain empty spaces.");
                return;
            }

            tokens = username.split(" ");
            if(tokens.length > 1)
            {
                alert("Username should not contain empty spaces.");
                return;
            }

            $('body').loading();

            $.ajax({
                type: "POST",
                url: "server.php",
                data: {userid: userid, username: username, password: password, fullname: fullname, roleid: roleid, admintype: admintype, api: "registerUser"}, 
                success: function(result)
                {
                   
                    $('body').loadingHide();
                    result = JSON.parse(result);
                    if(!result.status)
                    {
                        alert(result.ErrorMessage);
                    }
                    else
                    {
                        var userCRUDDialog = $("#userCRUDDialog").dialog({width: 620,height: 300});
                        userCRUDDialog.dialog('close');
                        window.updateUserRows();
                    }
                }
            });
        });

        $("#btnUserCancel").click(function()
        {
            var userCRUDDialog = $("#userCRUDDialog").dialog({width: 620,height: 300});
            userCRUDDialog.dialog('close');
        });


       /**************************************************************************
        *       Related to Roles: Rendered only for SuperAdmin and AdminMaster
        ***************************************************************************/

        $("#btnAddRole").click(function()
        {
            var roleCRUDDialog = $("#roleCRUDDialog").dialog({width: 620,height: 400});
            roleCRUDDialog.show();

            $("#rolename").val("");
            $("#roleid").val("");
            $("#authorizedapis").empty();
            $("#rolefullcontrol").attr("checked", false);
        });

        $("#btnDeleteRole").click(function()
        {
            var ids = "";
            $(".selectedroles:checked").each(function() 
            {
                ids += (ids == "")?($.trim(this.value)):("," + $.trim(this.value));
            });

            if(ids == "")
            {
                alert("Please select some roles");
                return;
            }

            if(confirm("Are you sure you want to remove the selected roles?"))
            {
                $('body').loading();
                $.ajax({
                    type: "POST",
                    url: "server.php",
                    data: {ids: ids, api: "removeRole"}, 
                    success: function(result)
                    {
                        $('body').loadingHide();
                        result = JSON.parse(result);
                        if(!result.status)
                        {
                            alert(result.ErrorMessage);
                            return;
                        }
                        window.updateRoleRows();
                    }
                });
            }
        });
        window.addSelectedAPI = function()
        {
            var value = $.trim($("#apis option:selected").val());

            if($("#authorizedapis option[value=" + value + "]").length == 0)
            {
                $("#authorizedapis").append($('<option>', {
                    value: value,
                    text: value
                }));
                $("#apis option:selected").next('option').attr('selected', 'selected');
                $("#authorizedapis option:selected").next('option').attr('selected', 'selected');
            }
        }

        window.removeSelectedAPI = function()
        {
            $("#authorizedapis option:selected").remove();
        }

        window.btnEditRole = function(roleid,rolename,apis,fullcontrol)
        {
            var roleCRUDDialog = $("#roleCRUDDialog").dialog({width: 620,height: 400});
            roleCRUDDialog.show();
 
            $("#authorizedapis").empty();
            $("#roleid").val(roleid);
            $("#rolename").val(rolename);
            $("#rolefullcontrol").attr("checked",(fullcontrol>0));

            var tokens = apis.split(",");
            var i = 0;

            for(i=0; i<tokens.length; i++)
            {
                var value = $.trim(tokens[i]);
                $("#authorizedapis").append($('<option>', {
                    value: value,
                    text: value
                }));
            }
        }
        window.updateRoleRows = function()
        {
            $('body').loading();
            $.ajax({
                type: "POST",
                url: "server.php",
                data: {api: "getRoleRows"}, 
                success: function(result)
                {
                    $('body').loadingHide();
                    $("#tableRoles").html(result);
                }
            });
            $.ajax({
                type: "POST",
                url: "server.php",
                data: {api: "getRoleOptions"}, 
                success: function(result)
                {
                    $('body').loadingHide();
                    $("#roles").html(result);
                }
            });
        }
        $("#btnRoleSave").click(function()
        {
            var roleid          = $.trim($("#roleid").val());
            var rolename        = $.trim($("#rolename").val());
            var authorizedapis  = $.trim($("#authorizedapis option:selected").val());
            var admintype       = $("#rolefullcontrol").is(":checked") ? 1 : 0;

            var authorizedapis = "";

            $("#authorizedapis option").each(function() 
            {
                authorizedapis += (authorizedapis == "")?($.trim(this.value)):("," + $.trim(this.value));
            });

            if(rolename == "")
            {
                alert("Please enter the role name");
                return;
            }
            $('body').loading();

            $.ajax({
                type: "POST",
                url: "server.php",
                data: {roleid: roleid, rolename: rolename, authorizedapis: authorizedapis, admintype: admintype, api: "createRole"}, 
                success: function(result)
                {
                    $('body').loadingHide();
                    result = JSON.parse(result);
                    if(!result.status)
                    {
                        alert(result.ErrorMessage);
                    }
                    else
                    {
                        var roleCRUDDialog = $("#roleCRUDDialog").dialog({width: 620,height: 400});
                        roleCRUDDialog.dialog('close');
                        window.updateRoleRows();
                        window.updateUserRows();
                    }
                }
            });
        });

        $("#btnRoleCancel").click(function()
        {
            var roleCRUDDialog = $("#roleCRUDDialog").dialog({width: 620,height: 400});
            roleCRUDDialog.dialog('close');
        });

        <? } ?>
    });
    </script>
</head>

<body>

<div id="wrapper">
<div id="container">
<div class="hide-btn top tip-s" original-title="Close sidebar"></div>
<div class="hide-btn center tip-s" original-title="Close sidebar"></div>
<div class="hide-btn bottom tip-s" original-title="Close sidebar"></div>

<div id="top">
<div style="padding-left: 10px; padding-bottom: 5px"><img src="img/logo.jpeg" width="110px" /></div>
<div id="labels">
<ul>
	<li><a href="#" class="user"><span class="bar">Welcome <?=$server->getLoggedUserProperty("fullname")?></span></a></li>
	<li><a href="#" class="settings" title="Settings" onclick="window.changeSettings()"></a></li>
	<li><a href="#" class="logout" title="Logout" onclick="window.logoutUser()"></a></li>
</ul>
</div>
<div id="menu">
<ul>
	<li class="current"><a href="dashboard.php">Dashboard</a></li>
</ul>
</div>
</div>

<div id="left">
<div class="box submenu">
<div class="content">
<ul>
	<li <?php if($server->checkAdminType(NONADMIN)){ ?> class="current" <?php } ?>><a href="#" id="lnkSectionImages" name="lnkSectionImages">Upload Image Packs</a></li>
    <?php if($server->checkAdminType(ADMINMASTER) || $server->checkAdminType(SUPERADMIN)){?>
	<li class="current"><a href="#" id="lnkSectionUser" name="lnkSectionUser">User Management</a></li>
	<li><a href="#" id="lnkSectionRoles" name="lnkSectionRoles">Define Roles</a></li>
	<li><a href="#" id="lnkSectionDownloads" name="lnkSectionDownloads">Image Downloads</a></li>
	<li><a href="#" id="lnkSectionApis" name="lnkSectionApis">APIs</a></li>
	<?php } ?>
</ul>
</div>
</div>
</div>

<div id="right">

<div id="breadcrumbs">
<ul>
	<li></li>
	<li><a href="#">Home</a></li>
	<li><a href="#">Login</a></li>
	<li><a href="#">Dashboard</a></li>
</ul>
</div>
	<?php if($server->checkAdminType(ADMINMASTER) || $server->checkAdminType(SUPERADMIN)){?>
<div class="section" id="sectionUser" name="sectionUser">
<div class="box">
<div class="title">Users <span class="hide"></span></div>
<div style="padding-left: 15px; padding-top: 10px"><a href="#"
	style="padding-right: 5px" id="btnAddUser" name="btnAddUser"><img
	src="img/adduser.png" width="35px" /></a> <a href="#"
	style="padding-right: 5px" id="btnDeleteUser" name="btnDeleteUser"><img
	src="img/deleteuser.png" width="35px" /></a></div>
<div class="content">
<table cellspacing="0" cellpadding="0" border="0" class="all"
	id="tableUsers" name="tableUsers">
	<?php echo $server->getUserRows()?>
</table>
</div>
</div>
</div>
<div id="userCRUDDialog" name="userCRUDDialog" class="dialog"
	style="display: none" title="User Details"><input type="hidden"
	id="userid" name="userid" />
<table width="100%" style="border: 2px solid #ffffff">
	<tbody>
		<tr>
			<td><label for="username">Username</label></td>
			<td><input type="text" id="username" name="username"
				style="width: 100%" /></td>
		</tr>
		<tr>
			<td><label for="password" style="width: 100%">Password</label></td>
			<td><input type="password" id="password" name="password"
				style="width: 100%" /></td>
		</tr>
		<tr>
			<td><label for="fullname" style="width: 100%">Fullname</label></td>
			<td><input type="text" id="fullname" name="fullname"
				style="width: 100%" /></td>
		</tr>
		<tr>
			<td><label style="width: 100%">Role</label></td>
			<td><select id="roles" name="roles" data-theme="none">
			<?php echo $server->getRoleOptions() ?>
			</select></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="button" id="btnUserSave" name="btnUserSave"
				value="Save" /> <input type="button" id="btnUserCancel"
				name="btnUserCancel" value="Cancel" /></td>
		</tr>
	</tbody>
</table>
</div>
			<?php } ?>
<div id="userSettingsDialog" name="userSettingsDialog" class="dialog"
	style="display: none" title="Change Your Settings"><input type="hidden"
	id="settingsuserid" name="settingsuserid" />
<table width="100%" style="border: 2px solid #ffffff">
	<tbody>
		<tr>
			<td><label for="username">Username</label></td>
			<td><input type="text" id="settingsusername" name="settingsusername"
				style="width: 100%" /></td>
		</tr>
		<tr>
			<td><label for="password" style="width: 100%">Password</label></td>
			<td><input type="password" id="settingspassword"
				name="settingspassword" style="width: 100%" /></td>
		</tr>
		<tr>
			<td><label for="fullname" style="width: 100%">Fullname</label></td>
			<td><input type="text" id="settingsfullname" name="settingsfullname"
				style="width: 100%" /></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="button" id="btnUserSettingsSave"
				name="btnUserSettingsSave" value="Save" /> <input type="button"
				id="btnUserSettingsCancel" name="btnUserSettingsCancel"
				value="Cancel" /></td>
		</tr>
	</tbody>
</table>
</div>
<div class="section" id="sectionImages" name="sectionImages"
<?php if($server->checkAdminType(ADMINMASTER) || $server->checkAdminType(SUPERADMIN)){?>
	style="display: none" <?php } ?>>
<div class="box">
<div class="title">Images & Packs <span class="hide"></span></div>
<div class="content">
<div class="box togglemenu">
<div class="content">
<ul>
	<li class="title"><b>Create Image Pack</b></li>
	<li class="content">
	<form id="formImagePack" name="formImagePack"><input type="hidden"
		id="api" name="api"
		value="createImagePackAndCreateAristIfNotExistsAndAssociateArtistToImagePack" />
	<input type="hidden" id="packusername" name="username" value="" /> <input
		type="hidden" id="packpassword" name="password" value="" />
	<table width="100%"
		style="border: 2px solid #ffffff; background-color: #FFFF99">
		<tbody>
			<tr>
				<td><label for="packtitle">Title (Optional)</label></td>
				<td><input type="text" id="packtitle" name="title"
					style="width: 100%" /></td>
			</tr>
			<tr>
				<td><label for="packtags" style="width: 100%">Tags</label></td>
				<td><input type="text" id="packtags" name="tags" style="width: 100%" /></td>
			</tr>
			<tr>
				<td><label for="packdescription" style="width: 100%">Description
				(Optional)</label></td>
				<td><textarea id="packdescription" name="description"
					style="width: 100%"></textarea></td>
			</tr>
			<tr>
				<td><label for="artisttitle" style="width: 100%">Artist Title</label></td>
				<td><input type="text" placeholder="Enter an artist name"
					id="artisttitle" name="artisttitle" style="width: 35%" />
				&nbsp;&nbsp;Or &nbsp;&nbsp;<select id="artistid" name="artistid"
					style="width: 55%">
					<option value="">Select Any Artist</option>
					<?php echo $server->getArtistOptions(); ?>
				</select></td>
			</tr>
			<tr>
				<td><label for="artistdescription" style="width: 100%">Artist
				Description<br />
				(Optional)</label></td>
				<td><textarea id="artistdescription" name="artistdescription"
					style="width: 100%"></textarea></td>
			</tr>
			<tr>
				<td><label></label></td>
				<td style="vertical-align: top">
				<table>
					<tr>
						<td><label for="dominantColorSelector"> Dominant&nbsp;Color: </label></td>
						<td><input id="dominantColorSelector" name="dominantColor"
							style="width: 40px; height: 14px; font-size: 10px; text-align: right" /></td>
						<td>
						<div style="width: 16px; height: 16px" id="dominant" />
						</td>
						<td><label for="attribColorSelector"> Attrib&nbsp;Color: </label></td>
						<td><input id="attribColorSelector" name="attribColor"
							style="width: 40px; height: 14px; font-size: 10px; text-align: right" /></td>
						<td>
						<div style="width: 16px; height: 16px" id="attrib" />
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td style=""><label for="file">Thumbnail:</label></td>
				<td>
				<p><input type="file" name="file" id="thumbnail" /> <input
					type="button" id="packimg" value="Browse" /></p>
				<p align="right"><input type="submit" value="Upload" /></p>
				</td>
			</tr>
		</tbody>
	</table>
	</form>
	</li>
	<li class="title"><b>Upload Images in Image Pack</b></li>
	<li class="content">
	<table width="100%"
		style="border: 2px solid #ffffff; background-color: #FFFF66">
		<tr>
			<td width="30%"><label style="width: 100%">Pack</label></td>
			<td width="70%"><select id="imagepackid" name="packid"
				style="width: 100%">
				<?php echo $server->getPackOptions() ?>
			</select></td>
		</tr>
	</table>
	<div id="imagesList" width="100%"></div>
	<p align="right"><input type="button" value="Remove All Images"
		onclick="if(confirm('Are you sure you want to remove all images ?')){$('#imagesList').html('');$(this).attr('disabled', 'disabled');}"
		id="removeAllImages" /> <input type="button" value="Add Image"
		onclick="$('#imagesList').append(window.renderImageUploadForm()); $('#removeAllImages').removeAttr('disabled')" />
	<input type="submit" value="Upload" id="imageListUpload" /></p>
	</li>
	<li class="title"><b>Search Image Packs</b></li>
	<li class="content">
	<table style="border: 2px solid #ffffff">
		<tbody>
			<tr>
				<td align="center"><select id="packtype" name="packtype">
					<option value="My Packs">My Packs</option>
					<option value="All Packs">All Packs</option>
				</select> <input type="text" id="search" name="search"
					style="width: 45%" /> <input type="button" id="btnSearch"
					name="btnSearch" value="Search" /> <input type="button"
					id="btnGetPacks" name="btnGetPacks" value="Get Packs" /></td>
			</tr>
			<tr>
				<td>
				<table style="border: 2px solid #ffffff" width="100%">
					<tbody id="packsresults" name="packsresults">
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
	</li>
</ul>
</div>
</div>
</div>
</div>
</div>
<div class="content" class="dialog" id="imagesdialog"
	name="imagesdialog">
<table style="border: 2px solid #ffffff" width="100%">
	<tbody id="imagesresults" name="imagesresults">
	</tbody>
</table>
</div>
				<?php if($server->checkAdminType(ADMINMASTER) || $server->checkAdminType(SUPERADMIN)){?>
<div class="section" id="sectionRoles" name="sectionRoles"
	style="display: none">
<div class="box">
<div class="title">Roles and API Authorization <span class="hide"></span>
</div>
<div style="padding-left: 15px; padding-top: 10px"><a href="#"
	style="padding-right: 5px" id="btnAddRole" name="btnAddRole"><img
	src="img/addrole.jpg" width="35px" /></a> <a href="#"
	style="padding-right: 5px" id="btnDeleteRole" name="btnDeleteRole"><img
	src="img/removerole.jpg" width="35px" /></a></div>
<div class="content">
<table cellspacing="0" cellpadding="0" border="0" class="all"
	id="tableRoles" name="tableRoles">
	<?php echo $server->getRoleRows()?>
</table>
</div>
</div>
</div>
<div class="content" id="roleCRUDDialog" name="roleCRUDDialog"
	class="dialog" style="display: none" title="Role Details"><input
	type="hidden" id="roleid" name="roleid" />
<table width="100%" style="border: 2px solid #ffffff">
	<tbody>
		<tr>
			<td width="40%"><label for="rolename">Role Name</label></td>
			<td width="60%"><input type="text" id="rolename" name="rolename"
				style="width: 100%" /></td>
		</tr>
		<tr>
			<td valign="top" width="40%"><labelstyle="width:100%">Select
			Authorized APIs</label></td>
			<td valign="top" width="60%">
			<table width="100%">
				<tr>
					<td><select id="apis" name="apis" size="10" style="width: 150px">
					<?php echo $server->getOptionsFromCvs(ALLAPIs); ?>
					</select></td>
					<td align="center" valign="top">
					<p align="center"><input type="button"
						onclick="window.addSelectedAPI()" value="Add >" /></p>
					<p align="center"><input type="button"
						onclick="window.removeSelectedAPI()" value="< Remove" /></p>
					</td>
					<td><select id="authorizedapis" name="authorizedapis" size="10"
						style="width: 150px"></select></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="40%"><?php if($server->checkAdminType(SUPERADMIN)){?> <input
				type="checkbox" id="rolefullcontrol" name="rolefullcontrol" />Full
			Control <?php } ?></td>
			<td width="60%"><input type="button" id="btnRoleSave"
				name="btnRoleSave" value="Save" /> <input type="button"
				id="btnRoleCancel" name="btnRoleCancel" value="Cancel" /></td>
		</tr>
	</tbody>
</table>
</div>
<div class="section" id="sectionDownloads" name="sectionDownloads"
	style="display: none">
<div class="box">
<div class="title">Image Downloads <span class="hide"></span></div>
<div class="content">
<table cellspacing="0" cellpadding="0" border="0" class="all">
<?php echo $server->getDownloadDetailRows()?>
</table>
</div>
</div>
</div>
<div class="section" id="sectionApis" name="sectionApis"
	style="display: none">
<div class="box">
<div class="title">APIs Section <span class="hide"></span></div>
<table cellspacing="0" cellpadding="0" border="0" class="all"
	id="tableUsers" name="tableUsers">
	<?php echo $server->getApiStats()?>
</table>
</div>
</div>
<?php } ?></div>
<div id="packRepresentationDialog" name="packRepresentationDialog"
	style="display: none;" title="Pack Representation"><img
	id="representation" name="representation" width="600px" /></div>
<div id="footer">
<div class="split">&#169; Copyright Originsoft Pvt Ltd.</div>
<div class="split right">Powered by Originsoft</div>
</div>
</div>
</div>

</body>

</html>
