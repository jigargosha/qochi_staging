<?php

/*if($_SERVER['HTTPS']!="on")  
{     
	$redirect= "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];     
	header("Location:$redirect");  
}*/

include_once "../wrappers/RestService.php";
include_once "../wrappers/ImageStorage.php";
include_once "../wrappers/ImageProcessor.php";
include_once "model/Model.php";

class ISRMService extends RestService
{

	private $stCloudClient;
	private $imageProcessor;
	private $model;
	private $connect;

	/**********************************************************************************
	 *
	 *	Storage Cloud Client: It can be either Cloudinary or Azure.
	 *  Note: We have implemented an wrapper interface for both storage clouds,
	 *  If we want to switch the cloud later we will simply implement it and change
	 *  the class name below.
	 *
	 **********************************************************************************/

	public function __construct()
	{
		$this->setStorageCloud();
		$this->model = new Model();
		$this->connect=$this->model->getCon();
		$this->imageProcessor = new ImageProcessor();
	}

	public function setStorageCloud()
	{
		$this->stCloudClient = new ImageStorage();
	}

	/**********************************************************************************
	 *
	 *	Image CRUD and Manipulation API (Core API)
	 *
	 **********************************************************************************/

	public function createImagePack($thumbnail)
	{
		$ext = substr(strrchr($thumbnail,'.'),1);
		$thumbimage = "thumbnail." . $ext;
		$packid = "pack" . uniqid() . "-" . $ext;
		$this->stCloudClient->createContainer($packid);
		$this->stCloudClient->uploadBlob($packid, $thumbimage, $thumbnail);
		return $packid;
	}

	public function addImageInPack($packid, $image)
	{
		$ext = substr(strrchr($image,'.'),1);
		$imageid = "image" . uniqid() . "." . $ext;
		$this->stCloudClient->uploadBlob($packid, $imageid, $image);
		return $imageid;
	}

	public function removeImagePack($packid)
	{
		$deleted = $this->stCloudClient->deleteContainer($packid);

		if($deleted)
		{
			$this->model->removeImagePack($packid);
		}
		return $deleted;
	}

	public function removeImage($packid, $imageid)
	{
		$deleted = $this->stCloudClient->deleteBlob($packid, $imageid);

		if($deleted)
		{
			$this->model->removeImage($imageid);
		}
		return $deleted;
	}

	public function retrieveImagePack($packid)
	{
		return $this->model->retrieveImagePack($packid);
	}

	public function retrieveImage($packid, $imageid)
	{
		return $this->stCloudClient->getBlob($packid, $imageid);
	}
	public function retrieveImages($packid)
	{
		return $this->model->retrieveImages($packid);
	}
	public function retrieveAllImagePacks()
	{
		return $this->model->retrieveAllImagePacks();
	}
	public function retrieveAllArtists()
	{
		return $this->model->retrieveAllArtists();
	}
	public function retrieveImagePacksByArtist($artistid)
	{
		return $this->model->retrieveImagePacksByArtist($artistid);
	}
	public function getDominantColor($packid)
	{

		$pack = $this->model->retrieveImagePack($packid);

		if(count($pack) == 0)
		return "";

		$pack = $pack[0];
		$dominantColor = trim($pack["dominantcolor"]);

		return $dominantColor;
	}
	public function getAttribColor($packid)
	{
		$pack = $this->model->retrieveImagePack($packid);

		if(count($pack) == 0)
		return "";

		$pack = $pack[0];
		$attribColor = trim($pack["attribcolor"]);

		return $attribColor;
	}
	public function retrieveFeedImage($feeditemid)
	{
		$feedItem = $this->model->getFeedItem($feeditemid);

		if(count($feedItem) == 0)
		return "";

		$pack = $this->model->retrieveImagePack($feedItem["packid"]);

		if(count($pack) == 0)
		return "";

		$pack = $pack[0];
		$dominantColor = trim($pack["dominantcolor"]);
		$attribColor = trim($pack["attribcolor"]);

		$dominantColor = ($dominantColor==="")?"#ff0000":$dominantColor;
		$attribColor   = ($attribColor==="")?"#0000ff":$attribColor;

		$image  = __DIR__ . "/../data/" . $feedItem["packid"] . "/" . $feedItem["imageid"];

		if(!file_exists($image))
		return "";

		$border = __DIR__ . "/../assets/" . "Border.png";
		$logo   = __DIR__ . "/../assets/" . "notoji.png";
		$font   = __DIR__ . "/../assets/" . "MaiandraGD.ttf";
		$output = __DIR__ . "/../data/" . "output" . uniqid() . ".png";
		$text   = trim($feedItem["text"]);

		$this->imageProcessor->prepareImage($border, $logo, $image, 430, 100, $text, 130, 500, 50, 22, $dominantColor, $attribColor, $font, $output);

		if($stream = fopen($output, 'r'))
		{
			$image = stream_get_contents($stream);
			return $image;
		}

		return "";
	}
	public function getFeedItem($feeditemid)
	{
		return $this->model->getFeedItem($feeditemid);
	}
	public function getFeedItemsInRange($start,$stop,$order)
	{
		return $this->model->getFeedItemsInRange($start,$stop,$order);
	}
	public function createUpdateApiStats($api_name){
		return $this->model->createUpdateApiStats($api_name);
	}
	public function createPackRepresentation($packid)
	{
		$ids=$this->stCloudClient->getBlobList($packid);
		$images = array();
		$representation = "representation";

		for($i=0;$i<count($ids);$i++)
		{
			if( (strpos($ids[$i], "thumbnail") === false) && (strpos($ids[$i], "representation") === false) )
			$images[] = __DIR__ . "/../data/" . $packid . "/" . $ids[$i];
		}

		$output = __DIR__."/../data/" . $packid . "/" . $representation . ".png";
		if(file_exists($output))
		unlink($output);
		$nrows = 4;
		$ncols = ceil(count($images)/$nrows);

		$this->imageProcessor->createPackRepresentation($images, $nrows, $ncols, 300, 20, $output);

		rename(__DIR__."/../data/" . $packid . "/" . $representation . ".png", __DIR__."/../data/" . $packid . "/" . $representation . "-png");

		return $representation . "-png";
	}
	public function retrieveMyImagePacks($username)
	{
		return $this->model->retrieveMyImagePacks($username);
	}
	public function subscribeToImagePack($username, $password, $unique_user_id, $packid)
	{
		return $this->model->subscribeToImagePack($username, $password, $unique_user_id, $packid);
	}
	public function isImagePackSubscribed($unique_user_id,$packid)
	{
		return $this->model->isImagePackSubscribed($unique_user_id, $packid);
	}
	public function getSubscribedImagePacks($unique_user_id)
	{
		return $this->model->getSubscribedImagePacks($unique_user_id);
	}
	public function getArtistDescription($artistid){
	 return $this->model->getArtistDescription($artistid);
	}
	public function createArtist($artisttitle,$artistdescription){
	 return $this->model->createRetriveArtist($artisttitle,$artistdescription);
	}
	public function createImagePackAndAssociateToArtist($packid, $title, $description, $tags, $username, $artistid, $dominantColor, $attribColor){
		$this->model->createImagePack($packid, $title, $description, $tags, $username,$artistid ,$dominantColor, $attribColor);
		$this->model->associateImagePackToArtist($packid,$artistid);
	}
	public function retrieveMyImage($packid, $imageid, $username)
	{
		$image = $this->model->retrieveMyImage($packid, $imageid, $username);

		if($image != null || ( ($image == null) && strpos($imageid, "thumbnail") !== false) )
		{
			return $this->retrieveImage($packid, $imageid);
		}
		return $image;
	}

	public function retrieveMyImages($packid, $username)
	{
		return $this->model->retrieveMyImages($packid, $username);
	}

	public function searchImagePacks($tags)
	{
		return $this->model->searchImagePacks($tags);
	}

	public function searchImages($tags, $packid)
	{
		return $this->model->searchImages($tags, $packid);
	}

	public function searchMyImagePacks($tags, $username)
	{
		return $this->model->searchImagePacks($tags, $username);
	}

	public function searchMyImages($packid, $tags, $username)
	{
		return $this->model->searchImages($packid, $tags, $username);
	}

	public function createFeedItem($packid, $imageid, $text, $unique_user_id)
	{
		return $this->model->createFeedItem($packid, $imageid, $text, $unique_user_id);
	}

	public function getFeed($unique_user_id)
	{
		return $this->model->getFeed($unique_user_id);
	}

	/**********************************************************************************
	 *
	 *	Utility Functions
	 *
	 **********************************************************************************/

	public function doesImagePackExist($packid)
	{
		return $this->stCloudClient->getContainer($packid);
	}
	public function authenticate($arguments)
	{
		try
		{
			$this->checkArguments(array("username", "password", "api"), $arguments);
		}
		catch(Exception $ex)
		{
			return $this->createResponse(false, $ex->getMessage());
		}

		$api = trim($arguments["api"]);
		$username = trim($arguments["username"]);
		$password = trim($arguments["password"]);
		return $this->model->authenticate($username, $password, $api);
	}

	public function createResponse($status, $value)
	{
		return json_encode(array("status" => $status, (($status)?"Data":"ErrorMessage")=>$value));
	}

	public function checkArguments($required, $provided)
	{
		foreach($required as $param)
		{
			if(!isset($provided[trim($param)]))
			throw new Exception("Parameter '".$param."' is missing");
		}
	}
	
	/*public function ms_escape_string($data) {
	if ( !isset($data) or empty($data) ) return '';
	if ( is_numeric($data) ) return $data;
	$non_displayables = array(
		'/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
		'/%1[0-9a-f]/',             // url encoded 16-31
		'/[\x00-\x08]/',            // 00-08
		'/\x0b/',                   // 11
		'/\x0c/',                   // 12
		'/[\x0e-\x1f]/'             // 14-31
	);
	foreach ( $non_displayables as $regex )
		$data = preg_replace( $regex, '', $data );
	$data = str_replace("'", "''", $data );
	return $data;
    }*/
	
	function ms_escape_string($value) {
		$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");		
		$replace = array("\\\\","\\0","\\n", "\\r", "''", '""', "\\Z");
		return str_replace($search, $replace, $value);
	}
	
	public function performEscape($arguments){
		foreach ($arguments as $key=>$value){
			$key = str_replace("'", "", $key );
			$key = str_replace('"', "", $key );
			$key = str_replace(' ', "", $key );
			$arguments[$key]=$this->ms_escape_string($value);
		}
		return $arguments;
	}

	/**********************************************************************************
	 *
	 *	REST API
	 *
	 **********************************************************************************/

	public function performGet($url, $arguments, $accept)
	{
		$arguments= $this->performEscape($arguments);
		if(($resp = $this->authenticate($arguments)) !== true)
		{
			echo $resp;
			return;
		}

		$api = trim($arguments["api"]);
		$username = trim($arguments["username"]);
		$password = trim($arguments["password"]);
		$this->createUpdateApiStats($api);
		try
		{
			switch($api)
			{
				case "retrieveImagePack":
					{
						$this->checkArguments(array("packid"), $arguments);
						$packid = trim($arguments["packid"]);
						$results = $this->retrieveImagePack($packid);
						echo $this->createResponse(true, $results);
					}
					break;
				case "retrieveFeedImage":
					{
						$this->checkArguments(array("feeditemid"), $arguments);
						$feeditemid = trim($arguments["feeditemid"]);
						$feedImage = $this->retrieveFeedImage($feeditemid);

						if($feedImage==="")
						{
							echo $this->createResponse(false, "Feed item or associated pack and image is not found" );
							return;
						}
						header('Content-type: image/png');
						echo $feedImage;
					}
					break;
				case "getDominantColor":
					$this->checkArguments(array("packid"), $arguments);
					$packid = trim($arguments["packid"]);
					$color = $this->getDominantColor($packid);

					if($color === "")
					{
						echo $this->createResponse(false, "Pack does not exist");
						return;
					}
					echo $this->createResponse(true, array("dominantColor" => $color));
					break;
				case "getAttribColor":
					$this->checkArguments(array("packid"), $arguments);
					$packid = trim($arguments["packid"]);
					$color = $this->getAttribColor($packid);

					if($color === "")
					{
						echo $this->createResponse(false, "Pack does not exist");
						return;
					}
					echo $this->createResponse(true, array("attribColor" => $color));
					break;
				case "retrieveImage":
					{
						$this->checkArguments(array("packid", "imageid"), $arguments);
						$packid = trim($arguments["packid"]);
						$imageid = trim($arguments["imageid"]);
						$ext = substr(strrchr($imageid,'.'),1);
						header('Content-type: image/' . $ext);
						echo $this->retrieveImage($packid, $imageid);
					}
					break;
				case "retrieveImages":
					{
						$this->checkArguments(array("packid"), $arguments);
						$packid = trim($arguments["packid"]);
						$results = $this->retrieveImages($packid);
						echo $this->createResponse(true, $results);
					}
					break;
				case "retrieveMyImagePacks":
					{
						$results = $this->retrieveMyImagePacks($username);
						echo $this->createResponse(true, $results);
					}
					break;
				case "retrieveMyImage":
					{
						$this->checkArguments(array("packid", "imageid"), $arguments);
						$packid = trim($arguments["packid"]);
						$imageid = trim($arguments["imageid"]);
						$ext = substr(strrchr($imageid,'.'),1);
						header('Content-type: image/' . $ext);
						echo $this->retrieveMyImage($packid, $imageid, $username);
					}
					break;
				case "retrieveMyImages":
					{
						$this->checkArguments(array("packid"), $arguments);
						$packid = trim($arguments["packid"]);
						$results = $this->retrieveMyImages($packid, $username);
						echo $this->createResponse(true, $results);
					}
					break;
				case 'retrieveAllImagePacks':
					{
						$packids = $this->retrieveAllImagePacks();
						echo $this->createResponse(true, $packids);
					}
					break;
				case 'searchImagePacks':
					{
						$this->checkArguments(array("tags"), $arguments);
						$tags = trim($arguments["tags"]);
						$packids = $this->searchImagePacks($tags);
						echo $this->createResponse(true, $packids);
					}
					break;
				case 'searchImages':
					{
						$this->checkArguments(array("packid", "tags"), $arguments);
						$packid = trim($arguments["packid"]);
						$tags = trim($arguments["tags"]);
						$imageids = $this->searchImages($tags, $packid);
						echo $this->createResponse(true, $imageids);
					}
					break;
				case 'searchMyImagePacks':
					{
						$this->checkArguments(array("tags"), $arguments);
						$tags = trim($arguments["tags"]);
						$packids = $this->searchMyImagePacks($tags, $username);
						echo $this->createResponse(true, $packids);
					}
					break;
				case 'searchMyImages':
					{
						$this->checkArguments(array("packid", "tags"), $arguments);
						$packid = trim($arguments["packid"]);
						$tags = trim($arguments["tags"]);
						$imageids = $this->searchMyImages($packid, $tags, $username);
						echo $this->createResponse(true, $imageids);
					}
					break;
				case "getFeed":
					$this->checkArguments(array("unique_user_id"), $arguments);
					$unique_user_id = trim($arguments["unique_user_id"]);
					$feed = $this->getFeed($unique_user_id);

					echo $this->createResponse(true, $feed);
					break;
				case "getFeedItem":
					$this->checkArguments(array("feeditemid"), $arguments);
					$feeditemid = trim($arguments["feeditemid"]);
					echo $this->createResponse(true, $this->getFeedItem($feeditemid));
					break;
				case "getFeedItemsInRange":
					$this->checkArguments(array("start", "stop", "order"), $arguments);
					$start = trim($arguments["start"]);
					$stop = trim($arguments["stop"]);
					$order = trim($arguments["order"]);
					echo $this->createResponse(true, $this->getFeedItemsInRange($start,$stop,$order));					
					break;
				case "getArtistDescription":
					$this->checkArguments(array("artistid"), $arguments);
					$artistid = trim($arguments["artistid"]);
					$desc=$this->getArtistDescription($artistid);
					echo $this->createResponse(true, $desc);
					break;
				case 'retrieveAllArtists':
					{
						$packids = $this->retrieveAllArtists();
						echo $this->createResponse(true, $packids);
					}
					break;
				case 'retrieveImagePacksByArtist':
					{
						$this->checkArguments(array("artistid"), $arguments);
						$artistid = trim($arguments["artistid"]);
						$packids = $this->retrieveImagePacksByArtist($artistid);
						echo $this->createResponse(true, $packids);
					}
					break;
				case 'isImagePackSubscribed':
					{
						$this->checkArguments(array("unique_user_id","packid"), $arguments);
						$unique_user_id = trim($arguments["unique_user_id"]);
						$packid = trim($arguments["packid"]);
						$isSubscribed = $this->isImagePackSubscribed($unique_user_id,$packid);
						echo $this->createResponse(true, $isSubscribed);
					}
					break;
				case 'getSubscribedImagePacks':
					{
						$this->checkArguments(array("unique_user_id"), $arguments);
						$unique_user_id = trim($arguments["unique_user_id"]);
						$packids = $this->getSubscribedImagePacks($unique_user_id);

						echo $this->createResponse(true, $packids);
					}
					break;
			}
		}
		catch(Exception $e)
		{
			echo $this->createResponse(false, $e->getMessage());
			return;
		}
	}
  
	public function performPost($url, $arguments, $accept)
	{
		$arguments= $this->performEscape($arguments);
		
		if(($resp = $this->authenticate($arguments)) !== true)
		{
			echo $resp;
			return;
		}

		$api = trim($arguments["api"]);
		$username = trim($arguments["username"]);
		$password = trim($arguments["password"]);
		$this->createUpdateApiStats($api);
		try
		{
			switch($api)
			{
				case "createImagePackAndCreateAristIfNotExistsAndAssociateArtistToImagePack":
					{
						$file = "";

						if(!isset($_FILES["file"]["tmp_name"]) || trim($_FILES["file"]["tmp_name"]) === "")
						{
							echo $this->createResponse(false, "Please select an image to upload");
							return;
						}

						else if ((!isset($arguments['artisttitle']) || trim($arguments['artisttitle']) === "") &&
						(!isset($arguments['artistid']) || trim($arguments['artistid']) === "")){
							echo $this->createResponse(false, "Please write title for artist or select any artist");
							return;
						}

						$fname = str_replace(array("_"), "", strtolower($_FILES["file"]["name"]));
						$status = copy($_FILES["file"]["tmp_name"],"./". $fname);
						$file = "./".$fname;

						$this->checkArguments(array("title", "description", "tags", "artisttitle", "artistdescription", "dominantColor", "attribColor"), $arguments);
						$thumbnail = $file;
						$packid = $this->createImagePack($thumbnail);
						unlink($file);

						$title = trim($arguments['title']);
						$description = trim($arguments['description']);
						$tags = trim($arguments['tags']);
						$dominantColor = trim($arguments['dominantColor']);
						$attribColor = trim($arguments['attribColor']);
						$artisttitle = trim($arguments['artisttitle']);
						$artistdescription = trim($arguments['artistdescription']);				
						if(isset($arguments['artistid']))
							$artistidsel = $arguments['artistid'];
						else
							$artistidsel = "";
						if($artistidsel)
							$artistid=$artistidsel;
						else
							$artistid=$this->createArtist($artisttitle,$artistdescription);					
						$this->createImagePackAndAssociateToArtist($packid, $title, $description, $tags, $username, $artistid, $dominantColor, $attribColor);
						echo $this->createResponse(true, array("packid" => $packid, "title" => $title, "artistid" =>$artistid));
					}
					break;

				case "createArtist":
					{
						$this->checkArguments(array("artisttitle", "artistdescription"), $arguments);
						$artisttitle = trim($arguments['artisttitle']);
						$artistdescription = trim($arguments['artistdescription']);
						$artistid=$this->createArtist($artisttitle,$artistdescription);
						echo $this->createResponse(true, array("artistid" =>$artistid));
					}
					break;

				case "createImagePackAndAssociateToArtist":
					{

						$file = "";
						if(!isset($_FILES["file"]["tmp_name"]) || trim($_FILES["file"]["tmp_name"]) === "") {
							echo $this->createResponse(false, "Please select an image to upload");
							return;
						}
							
						$fname = str_replace(array("_"), "", strtolower($_FILES["file"]["name"]));
						$status = copy($_FILES["file"]["tmp_name"],"./". $fname);
						$file = "./".$fname;
							
						$this->checkArguments(array("title", "description", "tags", "dominantColor","artistid","attribColor"), $arguments);
							
						$thumbnail = $file;
						$packid = $this->createImagePack($thumbnail);
						unlink($file);

						$title = trim($arguments['title']);
						$description = trim($arguments['description']);
						$tags = trim($arguments['tags']);
						$dominantColor = trim($arguments['dominantColor']);
						$attribColor = trim($arguments['attribColor']);
						$artistid=$arguments['artistid'];
							
						$this->createImagePackAndAssociateToArtist($packid, $title, $description, $tags, $username, $artistid, $dominantColor, $attribColor);
						echo $this->createResponse(true, array("packid" => $packid, "title" => $title, "artistid" =>$artistid));
					}
					break;

				case "addImageInPack":
					{
						$file = "";

						if(!isset($_FILES["file"]["tmp_name"]) || trim($_FILES["file"]["tmp_name"]) === "")
						{
							echo $this->createResponse(false, "Please select an image to upload");
							return;
						}

						$fname = str_replace(array("_"), "", strtolower($_FILES["file"]["name"]));
						$status = copy($_FILES["file"]["tmp_name"],"./". $fname);
						$file = "./".$fname;

						$this->checkArguments(array("packid", "title", "description", "tags"), $arguments);
						$image = $file;
						$packid = trim($arguments["packid"]);

						if(!$this->doesImagePackExist($packid))
						{
							echo $this->createResponse(false, "Image pack does not exist");
							return;
						}

						if(!$this->model->checkPackOwner($packid, $username))
						{
							echo $this->createResponse(false, "This image pack is not owned by you");
							return;
						}

						$imageid = $this->addImageInPack($packid, $image);
						unlink($file);

						$title = trim($arguments['title']);
						$description = trim($arguments['description']);
						$tags = trim($arguments['tags']);

						$this->model->addImageInPack($packid, $imageid, $title, $description, $tags, $username);

						echo $this->createResponse(true, array("imageid" => $imageid, "title" => $title));
					}
					break;
				case "removeImagePack":
					{
						$this->checkArguments(array("packid"), $arguments);
						$packid = trim($arguments["packid"]);


						if(!$this->doesImagePackExist($packid))
						{
							echo $this->createResponse(false, "Image pack does not exist");
							return;
						}

						if(!$this->model->checkPackOwner($packid, $username))
						{
							echo $this->createResponse(false, "This image pack is not owned by you");
							return;
						}

						$status = $this->removeImagePack($packid);
						echo $this->createResponse($status, ($status)?"":"Error deleting pack");
					}
					break;
				case "removeImage":
					{
						$this->checkArguments(array("packid", "imageid"), $arguments);
						$packid = trim($arguments["packid"]);
						$imageid = trim($arguments["imageid"]);

						if(!$this->doesImagePackExist($packid))
						{
							echo $this->createResponse(false, "Image pack does not exist");
							return;
						}

						if(!$this->model->checkImageOwner($imageid, $username))
						{
							echo $this->createResponse(false, "This image is not owned by you");
							return;
						}

						$status = $this->removeImage($packid,$imageid);
						echo $this->createResponse($status, ($status)?"":"Error deleting image");
					}
					break;
				case "createFeedItem":
					{
						$backCompatRequired = True;
						if (count($arguments) == 7 ){
							$this->checkArguments(array("packid", "imageid", "unique_user_id", "text"), $arguments);
						} else if (count($arguments) == 8 ){
							$this->checkArguments(array("packid", "imageid", "unique_user_id", "text", "origin_user_id"), $arguments);
							$backCompatRequired = False;
						}

						$packid = trim($arguments["packid"]);
						$imageid = trim($arguments["imageid"]);
						$unique_user_id = trim($arguments["unique_user_id"]);
						$text = trim($arguments["text"]);

						if ($backCompatRequired == True){
							$feeditem = $this->createFeedItem($packid, $imageid, $text, $unique_user_id);
						} else {
							$origin_user_id = trim($arguments["origin_user_id"]);
							$feeditem = $this->createFeedItem($packid, $imageid, $text, $unique_user_id, $origin_user_id);
						}						

						echo $this->createResponse(true, $feeditem);
						break;
					}
				case "createPackRepresentation":
					{
						$this->checkArguments(array("packid"), $arguments);
						$packid = trim($arguments["packid"]);
						$representation = $this->createPackRepresentation($packid);
						echo $this->createResponse(true, array("representation" => $representation));

						break;
					}
				case "subscribeToImagePack":
					{
						$this->checkArguments(array("packid", "unique_user_id"), $arguments);
						$packid = trim($arguments["packid"]);
						$unique_user_id = trim($arguments["unique_user_id"]);

						if(!$this->doesImagePackExist($packid))
						{
							echo $this->createResponse(false, "Image pack does not exist");
							return;
						}

						$this->subscribeToImagePack($username, $password, $unique_user_id, $packid);

						echo $this->createResponse(true, "");

						break;
					}
			}
		}
		catch(Exception $e)
		{
			echo $this->createResponse(false, $e->getMessage());
			return;
		}
	}
}
$service = new ISRMService();
$service->handleRawRequest($_SERVER, $_GET, $_POST);
?>