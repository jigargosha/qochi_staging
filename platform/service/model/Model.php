<?php
include_once __DIR__ . "/../../config/config.php";
include_once __DIR__ . "/../../wrappers/Database.php";
/******************************************************************************************
*
*  Model.php
*  Database Layer: It is used for every database interaction with respect to the core APIs
*
*******************************************************************************************/
class Model
{
    private $db;

	public function __construct()
	{
		$this->db = new database(DB_TYPE,DB_SERVER,DB_NAME,DB_USERNAME,DB_PASS,DB_PORT);
	}
	public function getCon(){
		$conect=$this->db->con;
		return $conect;
	}
    public function getUserByName($username)
    {
	$this->db->query("SELECT u.id as uid,u.username,u.password,u.fullname, r.id as rid, r.name, r.apis, r.admintype from Users u, Role r WHERE u.roleid=r.id AND u.username='$username'");
	$user = $this->db->get_row();
	return $user;
    }
	
	public function createRetriveArtist($artisttitle,$artistdescription)
	{	
	 $this->db->query("SELECT artistid from ArtistInfo WHERE title='$artisttitle'");
	 $artist = $this->db->get_row();
	 if (isset($artist[0])) {
	 return $artist[0];
	 }
	 $this->db->query("INSERT INTO ArtistInfo(title,description) VALUES('$artisttitle','$artistdescription')");
	 $artistid=$this->db->con->insert_id;
	 return $artistid;
    }
	
	public function getArtistDescription($artistid){
	$this->db->query("SELECT title,description FROM ArtistInfo WHERE artistid='$artistid'");
	$artist=array();
	while($row = $this->db->get_row()) {
		$artist[] = array("title" => trim($row[0]), "description" => trim($row[1]));
	 }
	 return $artist;
	}
	
    public function createImagePack($packid, $title, $description, $tags, $username, $artistid ,$dominantcolor, $attribcolor){
	$userid=null;
	if($username)
	{
	    $user = $this->getUserByName($username);
	    $userid = $user[0];
    }
	$this->db->query("INSERT INTO PackMetaData(id,title,description,tags,userid,artistid,dominantcolor,attribcolor) VALUES('$packid','$title','$description','$tags',$userid,$artistid,'$dominantcolor','$attribcolor')");
    }
	
	public function associateImagePackToArtist($packid,$artistid){
		$this->db->query("UPDATE PackMetaData SET artistid='$artistid' where id='".$packid."'");
	}
	
    public function addImageInPack($packid, $imageid, $title, $description, $tags, $username)
    {	
	$userid=null;
	if($username)
	{
	    $user = $this->getUserByName($username);
	    $userid = $user[0];
	}
	$tags = $this->filterTags($tags);
	$this->db->query("INSERT INTO ImageMetaData(id,packid,title,description,tags,userid) VALUES('$imageid','$packid','$title','$description','$tags',$userid)");
	$this->appendImageTagsToPack($packid, $tags);
    }
    public function retrieveImagePack($packid)
    {
	//$this->db->query("SELECT * FROM PackMetaData WHERE id='$packid'");
	$this->db->query("SELECT PackMetaData.id, PackMetaData.title, PackMetaData.description, PackMetaData.tags, PackMetaData.userid, PackMetaData.artistid, PackMetaData.dominantcolor, PackMetaData.attribcolor, artistinfo.title artistname FROM PackMetaData INNER JOIN artistinfo ON PackMetaData.artistid=artistinfo.artistid WHERE id='$packid'");
	$packs = array();
	while($row = $this->db->get_row())
	{
	    $packid = trim($row[0]);
	    $ext = substr(strrchr($packid,'-'),1);
	    $packs[] = array("packid" => $packid, "thumbnail" => "thumbnail." . $ext, "title" => trim($row[1]), "description" => trim($row[2]), "tags" => $this->filterTags(trim($row[3])), "userid"=>trim($row[4]), "artistid" => trim($row[5]), "dominantcolor" => trim($row[6]), 
		"attribcolor" => trim($row[7]), "artistname" => trim($row[8]));
	}
	if(isset($packs[0]["packid"]))
	{
		$this->db->query("SELECT id FROM ImageMetaData WHERE packid='$packid'");
		$images=array();
		while($row = $this->db->get_row())
		{
			$images[]=$row[0];
		}
		if(!empty($images)){
		 $ids=implode(",",$images);
		 $packs[0]["images"]=$ids;
		}
		else{
		 $packs[0]["images"]="";
		}
	}
	return $packs;
    }
    public function removeImagePack($packid)
    {
	$this->db->query("DELETE FROM PackMetaData WHERE id='$packid'");
	$this->db->query("DELETE FROM ImageMetaData WHERE packid='$packid'");
    }
    public function retrieveAllImagePacks($username = null)
    {
	$userid=null;
	if($username)
	{
	    $user = $this->getUserByName($username);
	    $userid = $user[0];
	}
	//$this->db->query("SELECT * FROM PackMetaData" . ($userid?" WHERE userid=$userid":""));
	$this->db->query("SELECT * FROM PackMetaData INNER JOIN artistinfo ON PackMetaData.artistid=artistinfo.artistid");
	$packs = array();
	while($row = $this->db->get_row())
	{
	    $packid = trim($row[0]);
	    $ext = substr(strrchr($packid,'-'),1);
	    //$packs[] = array("packid" => $packid, "thumbnail" => "thumbnail." . $ext, "title" => trim($row[1]), "description" => trim($row[2]), "tags" => $this->filterTags(trim($row[3])), /*"userid"=>trim($row[4]),*/ "artistid"=>trim($row[5]));
	    $packs[] = array("packid" => $packid, "thumbnail" => "thumbnail." . $ext, "title" => trim($row[1]), "description" => trim($row[2]), "tags" => $this->filterTags(trim($row[3])), /*"userid"=>trim($row[4]),*/ "artistid"=>trim($row[5]), "artistname"=>trim($row[9]), "artistdescription"=>trim($row[10]));
	    	
	}	
	return $packs;
    }
    public function retrieveImagePacksByArtist($artistid,$username = null){
	$userid=null;
	if($username)
	{
	    $user = $this->getUserByName($username);
	    $userid = $user[0];
	}
	$this->db->query("SELECT * FROM PackMetaData" . ($userid?" WHERE artistid=$artistid":""));
	$packs = array();
	while($row = $this->db->get_row()){
		if (trim($row[5])==$artistid){
		    $packid = trim($row[0]);
		    $ext = substr(strrchr($packid,'-'),1);
		    $packs[] = array("packid" => $packid, "thumbnail" => "thumbnail." . $ext, "title" => trim($row[1]), "description" => trim($row[2]), "tags" => $this->filterTags(trim($row[3])), "artistid"=>trim($row[5]));
		}	
	}	
	return $packs;
    }	
	public function retrieveAllArtists(){
    $this->db->query("SELECT * FROM ArtistInfo");
	$artist=array();
	while($row = $this->db->get_row())
	{
		$artist[]=array("artistid" => $row[0],"title"=>$row[1],"description"=>$row[2]);
	}
	return $artist;
	}

    public function retrieveMyImagePacks($userid)
    {
	return $this->retrieveAllImagePacks($userid);
    }
    public function retrieveMyImage($packid, $imageid, $username)
    {
	$userid=null;
	if($username)
	{
	    $user = $this->getUserByName($username);
	    $userid = $user[0];
	}
	$this->db->query("SELECT * FROM ImageMetaData WHERE userid=$userid AND packid='$packid' AND id='$imageid'");
	$row = $this->db->get_row();

	if($row != null)
	    return array("imageid"=>$imageid, "packid"=>trim($row[1]), "title"=>trim($row[2]), "description"=>trim($row[3]), "tags"=>$this->filterTags(trim($row[4])), "userid"=>trim($row[5]));

	return null;
    }

    public function retrieveMyImages($packid, $username)
    {
	$userid=null;
	if($username)
	{
	    $user = $this->getUserByName($username);
	    $userid = $user[0];
	}
	$this->db->query("SELECT * FROM ImageMetaData WHERE userid=$userid AND packid='$packid'");

	$images = array();
	while($row = $this->db->get_row())
	{
	    $imageid = trim($row[0]);
	    $ext = substr(strrchr($imageid,'-'),1);
	    $images[] = array("imageid"=>$imageid, "packid"=>trim($row[1]), "title"=>trim($row[2]), "description"=>trim($row[3]), "tags"=>$this->filterTags(trim($row[4])), "userid"=>trim($row[5]));
	}	
	return $images;
    }
    public function retrieveImages($packid)
    {
	$this->db->query("SELECT * FROM ImageMetaData WHERE packid='$packid'");

	$images = array();
	while($row = $this->db->get_row())
	{
	    $imageid = trim($row[0]);
	    $ext = substr(strrchr($imageid,'-'),1);
	    $images[] = array("imageid"=>$imageid, "packid"=>trim($row[1]), "title"=>trim($row[2]), "description"=>trim($row[3]), "tags"=>$this->filterTags(trim($row[4])), "userid"=>trim($row[5]));
	}	
	return $images;
    }
    public function searchImagePacks($tags, $username=null)
    {
	$userid=null;
	if($username)
	{
	    $user = $this->getUserByName($username);
	    $userid = $user[0];
	}
	$tags_array = explode(",",$tags);	
	$packs = array();			
	for($i=0;$i<count($tags_array);$i++)
	{
	    $tag = $tags_array[$i];
	    $query = "SELECT * FROM PackMetaData WHERE tags LIKE '%$tag%'". ($userid?" AND userid=$userid":"");
	    $this->db->query($query);
	    while($row = $this->db->get_row())
	    {
		$packid = trim($row[0]);
		$ext = substr(strrchr($packid,'-'),1);
		$packs[$packid] = array("packid" => $packid, "thumbnail" => "thumbnail." . $ext, "title" => trim($row[1]), "description" => trim($row[2]), "tags" => $this->filterTags(trim($row[3])), "userid"=>trim($row[4]), "artistid"=>trim($row[5]));
	    }				
	}		
	return $packs;
    }
    public function searchMyImagePacks($tags, $username)
    {
	return $this->searchImagePacks($tags, $username);
    }
    public function searchImages($packid, $tags, $username=null)
    {
	$userid=null;
	if($username)
	{
	    $user = $this->getUserByName($username);
	    $userid = $user[0];
	}
	$tags_array = explode(",",$tags);	
	$images = array();			
	for($i=0;$i<count($tags_array);$i++)
	{
	    $tag = $tags_array[$i];
	    $query = "SELECT * FROM ImageMetaData WHERE tags LIKE '%$tag%' AND packid='$packid'". ($userid?" AND userid=$userid":"");
	    $this->db->query($query);
	    while($row = $this->db->get_row())
	    {
		$imageid = trim($row[0]);
		$packid = trim($row[1]);
		$images[$imageid] = array("imageid" => $imageid, "packid" => $packid, "title" => trim($row[2]), "description" => trim($row[3]), "tags" => $this->filterTags(trim($row[4])), "userid"=>trim($row[5]));
	    }				
	}		
	return $images;
    }
    public function searchMyImages($packid, $tags, $username)
    {
	return $this->searchImages($packid, $tags, $username);
    }
    public function removeImage($imageid)
    {
	$this->db->query("SELECT pack.id, pack.tags, image.tags FROM ImageMetaData as image, PackMetaData as pack WHERE image.id='$imageid' AND pack.id = image.packid");
	$row        = $this->db->get_row();
	$packid     = trim($row[0]);
	$packtags   = trim($row[1]);
	$imagetags  = trim($row[2]);
	$packtags = $this->removeImageTagsFromPack($packtags, $imagetags);
	$this->db->query("UPDATE PackMetaData SET tags='$packtags' WHERE id='$packid'");
	$this->db->query("DELETE FROM ImageMetaData WHERE id='$imageid'");
    }
    public function createFeedItem($packid, $imageid, $text, $unique_user_id, $origin_user_id=null)
    {
	$date=date('Y-m-d H:i:s');
	if ($origin_user_id==null){
		$origin_user_id = $unique_user_id;
	}
	
	if(DB_TYPE=='sqlsrv'){
	 $this->db->query("INSERT INTO Feed(packid, imageid, text, userid, created_date,originuserid) VALUES('$packid','$imageid','$text','$unique_user_id','$date','$origin_user_id')");
	 $feeditemid=$this->getLastId();	
	}
	else{
	 $this->db->query("INSERT INTO Feed(packid, imageid, text, userid, created_date,originuserid) VALUES('$packid','$imageid','$text','$unique_user_id','$date','$origin_user_id')");
	 $feeditemid=mysqli_insert_id($this->db->con);
	}
	
	return array("packid"=>trim($packid), "imageid"=>trim($imageid), "text"=>trim($text), "userid"=>trim($unique_user_id), "timestamp"=>date("Y-m-d H:i:s"), "originuserid"=>trim($origin_user_id));
    }
	public function getLastId() {
		$result = 	$this->db->query("SELECT @@IDENTITY as ids");
		$row = $this->db->get_row();
		return $row[0];
    }
    public function getFeed($unique_user_id)
    {

	$this->db->query("SELECT TOP 20 * FROM Feed WHERE userid='$unique_user_id' ORDER BY id DESC");

	$feed = array();
	while($row = $this->db->get_row())
	{
	    $feed[] = array("feeditemid"=>trim($row[0]), "packid"=>trim($row[1]), "imageid"=>trim($row[2]), "text"=>trim($row[3]), "userid"=>trim($row[4]), "timestamp"=>$row[5]);
	}	
	return $feed;
    }
	 public function getSubscribedImagePacks($unique_user_id)
    {
    	$this->db->query("SELECT * FROM Subscriptions WHERE unique_user_id='$unique_user_id'");    	
    	$packIds = array();
    	while($row = $this->db->get_row())
    	{
    		$packIds[] = array("packId"=>trim($row[2]));
    	}
    	return $packIds;
    }
    public function isImagePackSubscribed($unique_user_id,$packid)
    {
    $this->db->query("SELECT CASE WHEN COUNT(*)>0 THEN 'true' ELSE 'false' END AS VAL FROM Subscriptions WHERE unique_user_id='$unique_user_id' AND packid='$packid'");
    $val = array();
    while($row = $this->db->get_row())
    {
    	$val[] = array("value"=>trim($row[0]));
    }
    return $val;
    }
    public function removeAllFeeds()
    {
	$this->db->query("DELETE FROM Feed");
    }
    public function authenticate($username, $password, $api)
    {		
	$user = $this->getUserByName($username);

	if(!$user)
	    return $this->createResponse(false, "User does not exist!");

	$stored_hash = base64_decode(trim($user[2]));

	if(!$this->check($password, $stored_hash))
	    return $this->createResponse(false, "Provided password is wrong");

	$admintype = trim($user[7]);

	if($admintype === "1" || $admintype === "2")
	    return true;

	$apis = trim($user[6]);
	$pos = $this->wstrpos($apis, $api);

	if($pos === false)
	    return $this->createResponse(false, "You have not permission to access this API");

	return true;
    }
  
    /********************************************************************************************
    *	Some Utilities
    ********************************************************************************************/

    public function check($password, $stored_hash) 
    {
        if (version_compare(PHP_VERSION, '5.3.2') < 0)
            throw new Exception('SHA requires PHP 5.3.2 or above');
        

        return crypt($password, $stored_hash) == $stored_hash;
    }
    public function createResponse($status, $value)
    {
	return json_encode(array("status" => $status, (($status)?"Data":"ErrorMessage")=>$value));
    }
    public function removeImageTagsFromPack($packtags, $imagetags)
    {
	$tokens = explode(",", $packtags);
	$filteredtags = "";

	foreach($tokens as $token)
	{
	    $token = strtolower(trim($token));
	    if($token !== "")
	    {
		if($this->wstrpos(strtolower($imagetags), $token) === false)
		    $filteredtags .= "," . $token;
		else
		    $imagetags = str_replace(array($token), "", strtolower($imagetags));
	    }
	}

	return $filteredtags;
    }
    public function filterTags($tags)
    {
	$tokens = explode(",", $tags);
	$filteredtags = "";

	foreach($tokens as $token)
	{
	    $token = trim($token);
	    if($token !== "")
	    {
		if($this->wstrpos(strtolower($filteredtags), strtolower($token)) === false)
		    $filteredtags .= "," . $token;
	    }
	}

	return $filteredtags;
    }
    public function appendImageTagsToPack($packid, $imagetags)
    {
	$this->db->query("SELECT * FROM PackMetaData WHERE id='$packid'");
	$pack = $this->db->get_row();

	$packtags = trim($pack[3]);
	$packtokens = explode(",", $packtags);
	$imagetokens = explode(",", $imagetags);
	$newtags = "";
    
	foreach($packtokens as $token)
	{
	    $token = trim($token);
	    if($token !== "")
	    {
		$newtags .= ",".$token;
	    }
	}
	
	foreach($imagetokens as $token)
	{
	    $token = trim($token);
	    if($token !== "")
	    {
		$newtags .= ",".$token;
	    }
	}
	$this->db->query("UPDATE PackMetaData SET tags='$newtags' WHERE id='$packid'");
    }
    public function unionImageTagsToPack($packid, $imagetags)
    {
	$this->db->query("SELECT * FROM PackMetaData WHERE id='$packid'");
	$pack = $this->db->get_row();

	$packtags = trim($pack[3]);
	$packtokens = explode(",", $packtags);
	$imagetokens = explode(",", $imagetags);
	$newtags = "";
    
	$i=0;
	foreach($packtokens as $token)
	{
	    $token = trim($token);
	    if($token !== "")
	    {
		if($this->wstrpos(strtolower($newtags), strtolower($token)) === false)
		    $newtags .= ",".$token;
		$i++;
	    }
	}
	$i=0;
	foreach($imagetokens as $token)
	{
	    $token = trim($token);
	    if($token !== "")
	    {
		if($this->wstrpos(strtolower($newtags), strtolower($token)) === false)
		    $newtags .= ",".$token;
		$i++;
	    }
	}
	$this->db->query("UPDATE PackMetaData SET tags='$newtags' WHERE id='$packid'");
    }
    public function wstrpos($string, $pattern)
    {
	$string = strtolower(trim($string));
	$pattern = strtolower(trim($pattern));

	$tokens = explode(",", $string);
	
	foreach($tokens as $token)
	{
	    $token = trim($token);
	    if($pattern === $token)
		return true;
	}
	return false;
    }
    public function subscribeToPack($username, $password, $unique_user_id, $packid)
    {
	$date=date('m/d/Y H:i:s A');
	$this->db->query("IF NOT EXISTS(SELECT * FROM Subscriptions WHERE unique_user_id = '$unique_user_id' AND packid = '$packid' AND timestamp = '$date') INSERT INTO Subscriptions(unique_user_id, packid, timestamp) VALUES('$unique_user_id','$packid','$date')");
	
	return true;
    }
    public function getFeedItem($feeditemid)
    {
	$this->db->query("SELECT * FROM Feed WHERE id=$feeditemid");
	$feed = array();
	$row = $this->db->get_row();
	if($row)
	{
	    $feed = array("feeditemid"=>trim($row[0]), "packid"=>trim($row[1]), "imageid"=>trim($row[2]), "text"=>trim($row[3]), "userid"=>trim($row[4]), "timestamp"=>$row[5]);
	}
	return $feed;
    }
    public function getFeedItemsInRange($start,$stop,$order)
    {
	$this->db->query("SELECT * FROM Feed WHERE id >= $start AND id <= $stop ORDER BY id " . (($order==="forward")?"ASC":"DESC"));
	$feed = array();

	while($row = $this->db->get_row())
	{
	    $feed[] = array("feeditemid"=>trim($row[0]), "packid"=>trim($row[1]), "imageid"=>trim($row[2]), "text"=>trim($row[3]), 
		"userid"=>trim($row[4]), "timestamp"=>$row[5]);
        }
	return $feed;
    }
    public function checkPackOwner($packid, $username)
    {
	$user = $this->getUserByName($username);
	$userid = $user[0];
	$admintype = intval(trim($user[7]));

	if($admintype > 0)
	    return true;

	$this->db->query("SELECT * FROM PackMetaData WHERE id='$packid' AND userid=$userid");
	$row = $this->db->get_row();
	
	return ($row != null);
    }
	public function subscribeToImagePack($username, $password, $unique_user_id, $packid)
	{
		// checking before inserting if the row is already there - ignore if it is
		$this->db->query("IF NOT EXISTS(SELECT unique_user_id FROM subscriptions t2 WHERE t2.unique_user_id = '$unique_user_id' AND t2.packid = '$packid') BEGIN INSERT INTO subscriptions (unique_user_id, packid,timestamp) VALUES('$unique_user_id','$packid',CURRENT_TIMESTAMP) END");
		return true;
	}
    public function checkImageOwner($imageid, $username)
    {
	$user = $this->getUserByName($username);
	$userid = $user[0];
	$admintype = intval(trim($user[7]));
	if($admintype > 0)
	    return true;
	$this->db->query("SELECT * FROM ImageMetaData WHERE id='$imageid' AND userid=$userid");
	$row = $this->db->get_row();
	return ($row != null);
    }
	public function createUpdateApiStats($api_name)
	{
		$this->db->query("SELECT id FROM app_analytics WHERE api='$api_name'");
		$row = $this->db->get_row();
		if($row != null){
			return $this->db->query("UPDATE app_analytics SET calls_within_hour=calls_within_hour+1, calls_within_day=calls_within_day+1 WHERE api='$api_name'");			
		}
		else{
			$this->db->query("DELETE FROM app_analytics WHERE api='$api_name'");
			return $this->db->query("INSERT INTO app_analytics (api,calls_within_hour, calls_within_day,hours_count,count_day) VALUES ('$api_name','1','1','0','0')");		
		}	
	}
	public function UpdateHourlyApiStats()
	{
		$this->db->query("SELECT * FROM app_analytics");
		$data=$this->db->fetchAll();
		if(isset($data[0])){
			foreach ($data as $dat) {
				if($dat[3]==""){
				 $api_count=$dat[2];
				 $time=time();
				 $this->db->query("UPDATE app_analytics SET average_within_hour=$api_count, maximum_within_hour= $api_count, hours_count=1 
				                   , maximum_date_hour=$time WHERE id=".$dat[0]);	
				}
				else{
				 $api_count=$dat[2];
				 $hour_count=$dat[5]+1;
				 $time=time();
				 $average=$api_count/$hour_count;
				 $calls_hour=$dat[2]-($dat[5]*$dat[3]);
					if($calls_hour>$dat[4]){
						$max_hour_calls=$calls_hour;
						$time=time();
						 $this->db->query("UPDATE app_analytics SET average_within_hour=$average, maximum_within_hour= $max_hour_calls, 			
						 					hours_count=$hour_count, maximum_date_hour=$time WHERE id=".$dat[0]);	
					}
					else{
						$this->db->query("UPDATE app_analytics SET average_within_hour=$average,hours_count=$hour_count WHERE id=".$dat[0]);	
					}
				}
			}
				die("Updated");
		}
	}
	public function UpdateDailyApiStats()
	{
		$this->db->query("SELECT * FROM app_analytics");
		$data=$this->db->fetchAll();
		if(isset($data[0])){
			foreach ($data as $dat) {
				if($dat[8]==""){
				 $api_count=$dat[7];
				 $time=time();
				 $this->db->query("UPDATE app_analytics SET average_within_day=$api_count, maximum_within_day= $api_count, count_day=1 
				                   , maximum_month_day=$time WHERE id=".$dat[0]);	
				}
				else{
				 $api_count=$dat[7];
				 $day_count=$dat[10]+1;
				 $time=time();
				 $average=$api_count/$day_count;
				 $calls_day=$dat[7]-($dat[8]*$dat[10]);
					if($calls_day>$dat[9]){
						$max_day_calls=$calls_day;
						$time=time();
						 $this->db->query("UPDATE app_analytics SET average_within_day=$average, maximum_within_day= $max_day_calls, 			
						 					count_day=$day_count, maximum_month_day=$time WHERE id=".$dat[0]);	
					}
					else{
						$this->db->query("UPDATE app_analytics SET average_within_day=$average,count_day=$day_count WHERE id=".$dat[0]);	
					}
				}
			}
				die("Updated");
		}	
	}
} 