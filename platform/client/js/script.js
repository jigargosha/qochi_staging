$(function(){

    window.selectedPack = "";
    window.selectedImage = "";

    // setting the feed area as scrollable (in phone)
    
    $('#feedscreen').slimScroll({
	height: '100%'
    });
    
    // It will resize the content area according to mobile dimensions
    $(window).resize(function() {
	$(".feed_contents").height($(window).height()-80);
    });
    
    $(window).resize();
    
   
    // It will remove the default dragging behaviour of images
    $("img").on("dragstart", function(event) { event.preventDefault(); });

    // Pages swipe
    /*
    $("#listitem").swiperight(function() {
	$.mobile.changePage("#page1");
    });
    */
    
    //
    //	The gallery view, left and right swipe.
    //
    
   
    $("#galleryContainer").on("swipeleft", function (e) {
	window.slideGallery(30,"left");
    }); 

    $("#galleryContainer").on("swiperight", function (e) {
	window.slideGallery(30,"right");
    });
    
    window.slideGallery = function(speed, dir)
    {
	var p = $("#gallery").position();
	if((p.left > 0 && dir == "right") || (p.left + $("#gallery").width()+100 < $("#createNoteDialog").width() && dir == "left"))
	{
	    speed = 0;
	    return;
	}

	if(dir=="right")
	    $("#gallery").stop().animate({left:"+="+speed}, 1);
	else
	    $("#gallery").stop().animate({left:"-="+speed}, 1);

	if(speed != 0)
	{
	    setTimeout(function(){window.slideGallery(speed,dir)},0)
	    if(speed > 0)
		speed--;
	    if(speed < 0)
		speed++; 
	}
    }
    window.showNoteDialog = function()
    {
	$("#galleryContainer").show();
	$("#createNotePopup_sticker_img").attr("src", "images/placeHolderImage.jpg");
	$("#createNotePopup_note_txt").val("Type a note");
	window.selectedPack = ""; 
	window.selectedImage = "";
	$("#gallery").html("");
    }
    
    window.loadUIFeed = function(result)
    {
	var html = "";
	var data = result.Data;
	$.each(data, function(i, item) 
	{
	    html += "<tr>" + 
		    "<td class='feedtext' style='width:150px'><input type='hidden' id='feed_" + i + "_id' value='"+item.feeditemid+"' /><img src='" + window.retrieveImage(item.packid, item.imageid) + "' style='width:150px; height:150px'></td>" +
		    "<td class='feedtext' valign='top'>" + item.text + "</td>" + 
		    "</tr>";
	});
	
	$("#feed_container").html(html);
    }

    window.loadUIImagePacks = function(result)
    {
	var html = "";
	var data = result.Data;
	$.each(data, function(i, item) 
	{
	    html += "<a href='#' onclick='window.imageClickHandler(\"" + item.packid + "\",\"\")'><img onmousedown='return false;' class='image'  src='" + window.retrieveImage(item.packid, item.thumbnail) + "' style='width:80px; height:80px; border: 4px solid #ffffff' /></a>";
	});
	
	$("#gallery").html(html);
	$("img").on("dragstart", function(event) { event.preventDefault(); });
    }
    
    window.loadUIImages = function(result)
    {
	var html = "";
	var data = result.Data;
	$.each(data, function(i, item) 
	{
	    html += "<a href='#' onclick='window.imageClickHandler(\"" + item.packid + "\",\"" + item.imageid + "\")'><img  class='image'  src='" + window.retrieveImage(item.packid, item.imageid) + "' style='width:80px; height:80px; border: 4px solid #ffffff' /></a>";
	});
	
	$("#gallery").html(html);
	$("img").on("dragstart", function(event) { event.preventDefault(); });
    }
    window.selectedImageClickHandler = function()
    {
	window.retrieveAllImagePacks(); 
	window.selectedPack = ""; 
	window.selectedImage = "";
    }
    window.imageClickHandler = function(packid, imageid)
    {
	if(window.selectedPack == "")
	{
	    window.selectedPack = packid;
	    window.retrieveImages(packid);
	}
	else
	{
	    window.selectedImage = imageid;
	    $("#createNotePopup_sticker_img").attr("src", window.retrieveImage(window.selectedPack, window.selectedImage));
	}
    }
    window.createUIFeedItem = function()
    {
	var text = $("#createNotePopup_note_txt").val();
	
	if(window.selectedImage == "")
	{
	    alert("Please select an image for your note");
	    return;
	}
	
	window.createFeedItem(window.selectedPack, window.selectedImage, text);
    }
});	