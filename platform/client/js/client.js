$(function(){
     
    window.ServiceURL = "http://kochi32.azurewebsites.net/src/service/ISRMService.php";
    window.unique_user_id = "adminqa";
    window.username = "adminqa"
    window.password = "test1234";
	
    window.getFeed = function()
    {
	$.ajax({
	    url: window.ServiceURL,
	    type: "GET",
	    success: function(result)
	    {
		result = JSON.parse(result);
		if(!result.status)
		{
		    alert(result.ErrorMessage);
		}
		else
		{
		    window.loadUIFeed(result);
		}
	    },
	    data: {unique_user_id: window.unique_user_id, username: window.username, password: window.password, api: "getFeed"}
	});
    }
    
    window.createFeedItem = function(packid, imageid, text)
    {
	$.ajax({
	    type: "POST",
	    url: window.ServiceURL,
	    data: {unique_user_id: window.unique_user_id, username: window.username, password: window.password, packid: packid, imageid: imageid, text: text, api: "createFeedItem"}, 
	    success: function(result)
	    {
		result = JSON.parse(result);
		if(!result.status)
		{
		    alert(result.ErrorMessage);
		}
		else
		{
		    window.getFeed();
		    $("#createNoteDialog").dialog('close');
		}
	    }
	});
    }
    
    window.retrieveAllImagePacks = function()
    {
	$.ajax({
	    url: window.ServiceURL,
	    type: "GET",
	    success: function(result)
	    {
		result = JSON.parse(result);
		if(!result.status)
		{
		    alert(result.ErrorMessage);
		}
		else
		{
		    window.loadUIImagePacks(result);
		}
	    },
	    data: {username: window.username, password: window.password, api: "retrieveAllImagePacks"}
	});
    }
    
    window.retrieveImages = function(packid)
    {
	$.ajax({
	    url: window.ServiceURL,
	    type: "GET",
	    success: function(result)
	    {
		result = JSON.parse(result);
		if(!result.status)
		{
		    alert(result.ErrorMessage);
		}
		else
		{
		    window.loadUIImages(result);
		}
	    },
	    data: {username: window.username, password: window.password, packid: packid, api: "retrieveImages"}
	});
    }
    
    window.retrieveImage = function(packid, imageid)
    {
        return window.ServiceURL + "?" + "unique_user_id=" + window.unique_user_id + "&username=" + window.username + "&password=" + window.password + "&packid=" + packid + "&imageid=" + imageid + "&api=retrieveImage";
    }
    
    window.getFeed();
});