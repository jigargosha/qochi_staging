<?php

set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'library');

include_once "interfaces/ICloudStorage.php";
require_once 'Microsoft/WindowsAzure/Storage/Blob.php';

define("PROTOCOL", 		"http");
define("STORAGE", 		"qochistorage");
define("PRIMARYKEY", 	"Wt7CHON9Vlrk2EwZbOGPYpIeQCwpJlnseiFFG2hB5lapgQ+5pv/XFTMDKJXjvneQL+nI240Buh6QsndHIASEqQ==");
define("SECONDARYKEY", 	"5rbMQOQ7tyXRNPEeXxsiyHu16Ep20e969gy8MsL60OhuWcIoOxBR8IY+KZjb/uIwJuKz7K8zSQxK9Rb6rn8FPg==");

class AzureStorage extends ICloudStorage
{
	public $blobRestProxy;

	function __construct()
	{
		$this->initStorage();
	}
	public function initStorage()
	{
		$this->blobRestProxy = new Microsoft_WindowsAzure_Storage_Blob('blob.core.windows.net', STORAGE, PRIMARYKEY);
	}
	public function createContainer($container)
	{
		$this->blobRestProxy->createContainer($container);
	}

	public function getContainersList()
	{
		try
		{
			$containers = $this->blobRestProxy->listContainers();

			$ids = array();
			$count = count($containers);
			for($b=0; $b<$count; $b++)
			{
				$ids[] = $containers[$b]->Name;
			}
			return $ids;
		}
		catch(ServiceException $e)
		{
		}
		return array();
	}

	public function getContainer($containerName)
	{
		$containers = $this->getContainersList();

		foreach($containers as $key => $container)
		{
			if(strcmp($containerName, $container) == 0)
			return $container;
		}
		return NULL;
	}

	function deleteContainer($containerName)
	{
		try
		{
			$this->blobRestProxy->deleteContainer($containerName);
			return true;
		}
		catch(ServiceException $e)
		{
		}
		return false;
	}

	public function uploadBlob($container, $blob, $file)
	{
		$this->blobRestProxy->putBlob($container, $blob, $file);
	}

	public function getBlobList($container)
	{
		$blobs = $this->blobRestProxy->listBlobs($container);
		$ids = array();
		$count = count($blobs);
		for($b=0; $b<$count; $b++)
		{
			$ids[] = $blobs[$b]->Name;
		}
		return $ids;
	}

	public function getBlob($container, $blob)
	{
		$image = $this->blobRestProxy->getBlobData($container, $blob);
		return $image;//stream_get_contents(fpassthru($image->getContentStream()));
	}

	function deleteBlob($containerName, $blob_name)
	{
		try
		{
			$this->blobRestProxy->deleteBlob($containerName, $blob_name);
			return true;
		}
		catch(ServiceException $e)
		{
		}
		return false;
	}
}

?>