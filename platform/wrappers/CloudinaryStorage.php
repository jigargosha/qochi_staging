<?php

include_once "interfaces/ICloudStorage.php";

$base = realpath(dirname(__FILE__).'/../');
require_once($base . DIRECTORY_SEPARATOR . 'cloudinary/Cloudinary.php');
require_once($base . DIRECTORY_SEPARATOR . 'cloudinary/Uploader.php');
require_once($base . DIRECTORY_SEPARATOR . 'cloudinary/Api.php');

class CloudinaryStorage extends ICloudStorage
{
    public function __construct() 
    {
        $this->initStorage();
    }
    public function initStorage()
    {
    }
}

?>