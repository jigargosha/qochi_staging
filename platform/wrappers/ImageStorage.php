<?php

set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . '/../../library');

include_once "interfaces/ICloudStorage.php";

define("STORAGEPATH", __DIR__."/../data");

class ImageStorage extends ICloudStorage
{
	public $blobRestProxy;

	function __construct()
	{
		$this->initStorage();
	}
	public function initStorage()
	{
	}
	public function createContainer($container)
	{
		if (!file_exists(STORAGEPATH . "/". $container)) {
			mkdir(STORAGEPATH . "/". $container, 0777, true);
			return true;
		}
		return false;
	}

	public function getContainersList()
	{
		$containers = glob(STORAGEPATH . '/*', GLOB_ONLYDIR);

		$ids = array();
		$count = count($containers);
		for($b=0; $b<$count; $b++)
		{
			$ids[] = $containers[$b];
		}
		return $ids;
	}

	public function getContainer($containerName)
	{
		if(!file_exists(STORAGEPATH . "/". $containerName))
		return NULL;

		return $containerName;
	}

	function deleteContainer($containerName)
	{
		if(file_exists(STORAGEPATH . "/". $containerName))
		{
			system('rm -rf ' . escapeshellarg(STORAGEPATH . "/". $containerName));
			return true;
		}

		return false;
	}

	public function uploadBlob($container, $blob, $file)
	{
		if(file_exists(STORAGEPATH . "/". $container))
		{
			copy($file, STORAGEPATH . "/". $container . "/" . $blob);
			return true;
		}
		return false;
	}

	public function getBlobList($container)
	{
		$ids = array();

		if(file_exists(STORAGEPATH . "/". $container))
		{
			$blobs = scandir(STORAGEPATH . "/". $container);
			$count = count($blobs);
			for($b=0; $b<$count; $b++)
			{
				if($blobs[$b] !== "." && $blobs[$b] !== "..")
				$ids[] = $blobs[$b];
			}
		}
		return $ids;
	}

	public function getBlob($container, $blob)
	{
		if(file_exists(STORAGEPATH . "/". $container))
		{
			if($stream = fopen(STORAGEPATH . "/". $container . "/" . $blob, 'r'))
			{
				$image = stream_get_contents($stream);
				return $image;
			}
		}
		return NULL;
	}

	function deleteBlob($containerName, $blob_name)
	{
		if(file_exists(STORAGEPATH . "/". $containerName . "/" . $blob_name))
		{
			return unlink(STORAGEPATH . "/". $containerName . "/" . $blob_name);
		}
		return false;
	}
}

?>