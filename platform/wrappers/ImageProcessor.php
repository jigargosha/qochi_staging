<?php

define("ICONX", 40);
define("ICONY", 1455);


class ImageProcessor
{

	function imageCopyMergeAlpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct)
	{
		imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h);
	}
	function breakTextCentered($str,$num) 
	{
		$strs = explode(' ',$str);
		$strlenMax = $num;
		$strlenTemp = 0;
		$tempStr = "";
		$ret = "";

		foreach($strs as $item) 
		{
			$strlenTemp += strlen(utf8_decode($item));
			$tempStr .= $item." ";
			if($strlenTemp>$strlenMax) 
			{
				$ret .= $tempStr."\n";
				$strlenTemp =0;
				$tempStr = '';
			}
		}
		if($strlenTemp<=$strlenMax) 
		{
			$ret .= $tempStr;
		}
		return $ret;
	}

	function hex2rgb($hex) 
	{
		$hex = str_replace("#", "", $hex);

		if(strlen($hex) == 3) 
		{
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} 
		else 
		{
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb = array($r, $g, $b);
		return $rgb; 
	}

	function prepareImage($background, $icon, $image, $imagex, $imagey, $text, $textx, $texty, $fontsize, $ncharperline, $dominantColor, $attribColor, $fontfile, $output)
	{
		$icon = imagecreatefrompng($icon);
		$background = imagecreatefrompng($background);
		$image = $this->createImageFromFile($image);

		$text = $this->breakTextCentered($text, $ncharperline);

		$dominantColor	= $this->hex2rgb($dominantColor);
		$attribColor	= $this->hex2rgb($attribColor);

		if($background && imagefilter($background, IMG_FILTER_COLORIZE, $dominantColor[0], $dominantColor[1], $dominantColor[2]) && $icon && $image)
		{
			imagesavealpha($icon, true);	
			imagesavealpha($background, true);
			imagesavealpha($image, true);

			$color = imagecolorallocate($background, $attribColor[0], $attribColor[1], $attribColor[2]);
			imagettftext($background, $fontsize, 0, $textx, $texty, $color, $fontfile, $text);

			$this->imageCopyMergeAlpha($background, $icon, ICONX, ICONY, 0, 0, imagesx($icon), imagesy($icon), 100);
			$this->imageCopyMergeAlpha($background, $image, $imagex, $imagey, 0, 0, imagesx($image), imagesy($image), 100);

			imagepng($background, $output);

			imagedestroy($image);
			imagedestroy($background);
			imagedestroy($icon);
		}
	}

	function createImageFromFile($filename)
	{
		$ext = substr(strrchr($filename,'.'),1);
		switch($ext)
		{
			case "png":
				return imagecreatefrompng($filename);
			case "jpg":
				return imagecreatefromjpeg($filename);
			case "gif":
				return imagecreatefromgif($filename);
		}
	}

	function createPackRepresentation($images, $nrows, $ncols, $imgdim, $cellpad, $output)
	{
		$packrep = imagecreatetruecolor($nrows * $imgdim + ($nrows+1) * $cellpad, $ncols * $imgdim + ($ncols+1) * $cellpad);
		$white = imagecolorallocate($packrep, 255, 255, 102);
		imagefill($packrep, 0, 0, $white);

		imagesavealpha($packrep, true);

		for($row = 0; $row < $nrows; $row++)
		{
			for($col = 0; $col < $ncols; $col++)
			{
				if($ncols * $row + $col < count($images))
				{
					$filename = $images[$ncols * $row + $col];
					$image = $this->createImageFromFile($filename);
					$sm_image = imagecreatetruecolor(300,300);
					imagecopyresampled($sm_image,$image,0,0,0,0,300,300,imagesx($image),imagesy($image));
					imagesavealpha($sm_image, true);
					$this->imageCopyMergeAlpha ($packrep, $sm_image, ($row + 1) * $cellpad + $row * $imgdim, ($col + 1) * $cellpad + $col * $imgdim, 0, 0, imagesx($sm_image), imagesy($sm_image), 100);
				}
			}
		}
		
		imagepng($packrep, $output);
		imagedestroy($packrep);
	}
};
?>