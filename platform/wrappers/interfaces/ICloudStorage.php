<?php

abstract class ICloudStorage
{

    /**********************************************************************************
     *	
     *	Initialize the cloud client
     *
     **********************************************************************************/

    abstract public function initStorage();


    /**********************************************************************************
     *	
     *	Create Container
     *  Params: 
     * 		- $container: Name of the container
     *
     **********************************************************************************/
    
    abstract public function createContainer($container);

    /**********************************************************************************
     *	
     *	Upload Blob
     *  Params: 
     * 		- $container: 	Name of the container
     *		- $blob:      	Name of the blob
     *		- $file:	Absolute Path to the file
     *
     **********************************************************************************/

    abstract public function uploadBlob($container, $blob, $file);

    /**********************************************************************************
     *	
     *	Get Blob List in a Container
     *  Params: 
     * 		- $container: Name of the container
     *
     **********************************************************************************/

    abstract public function getBlobList($container);


    /**********************************************************************************
     *	
     *	Get Blob
     *  Params: 
     * 		- $container: 	Name of the container
     *		- $blob:	Name of the blob
     *
     **********************************************************************************/

    abstract public function getBlob($container, $blob);
}

?>